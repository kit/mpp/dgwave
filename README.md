## Getting Started
This is the dgwave project for dynamic fracture simulations.

### Prerequisites
* This version of M++ uses CMake (https://cmake.org/download/) as building tool.
You can install it with:  
```sudo apt install cmake```

* You need the packages BLAS (Basic Linear Algebra Subroutines 
https://www.netlib.org/blas/) and
LAPACK (Linear Algebra PACKage https://www.netlib.org/lapack/).
You can install it with:  
```sudo apt install libblas-dev liblapack-dev```

* To distribute your computations on parallel processes you need to have Open MPI
(Massage Passing Interface https://www.open-mpi.org/).
You can install it with:  
```sudo apt install openmpi-bin libopenmpi-dev```

### Installing DGWave-M++
* To clone the project with the necessary submodules run:  
```git clone --recurse-submodules https://git.scc.kit.edu/mpp/dgwave.git```

* Alternatively, clone it and update the submodules by hand:  
```git clone https://git.scc.kit.edu/mpp/dgwave.git```  
```cd dgwave```  
```git submodule update --init --recursive```   

* To install DGWave-M++, run in the dgwave root directory:  
```mkdir build```  
```cd build```     
```cmake ..```  
```make -j```

* Alternatively, simply run:
```./init.sh```

### Testing DGWave-M++
* cd build
* mpirun -n 4 M++
* find logfile in log/log
* find vkt in data/vkt
* parameters can be changed in dgwave/conf/arc_2d.conf
