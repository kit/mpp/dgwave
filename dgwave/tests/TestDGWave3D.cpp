#include "TestDGWave.hpp"

#include "DGElasticity.cpp"
#include "TestEnvironment.hpp"


const double TEST_TOLERANCE = 1e-4;

class TestDGWaveMainProgram : public TestWithParam<ConfigMap> {
protected:
  double testValue;

  void SetUp() override {
    Config::Initialize(GetParam());
    Config::Get("TestValue", testValue);
  }

  void TearDown() override {
    Config::Close();
  }
};

INSTANTIATE_TEST_SUITE_P(TestDGWaveMainProgram, TestDGWaveMainProgram,
                         Values(Test3D));

TEST_P(TestDGWaveMainProgram, TestRun) {
  EXPECT_NEAR(MaindG_Elasticity(), testValue, TEST_TOLERANCE);
}

int main(int argc, char **argv) {
  return MppTest(
    MppTestBuilder(argc, argv).
      WithSearchPath("../").
      WithPPM().
      WithoutDefaultConfig().
      WithScreenLogging()
  ).RUN_ALL_MPP_TESTS();
}