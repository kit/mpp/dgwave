#include "m++.hpp"
#include "Parallel.hpp"

#ifdef COMPLEX
#error undef COMPLEX in src/Compiler.h
#endif

double MaindG_Elasticity();

int main(int argc, char **argv) {
    Config::SetConfigFileName("m++.conf");
    Mpp::initialize(&argc, argv);

    bool clear = false;
    Config::Get("ClearData", clear);
    if (clear && PPM->master()) 
      int cleared = system("exec rm -rf data/vtk/*");

    std::string Model;
    Config::Get("Model", Model);

    if (Model == "dGElasticity") MaindG_Elasticity();

    return 0;
}
