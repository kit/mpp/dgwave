#include "DGProblem.hpp"

#include <numbers>

#include "GlobalDefinitions.hpp"
#include "RVector.hpp"
#include "RMatrix.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "M_IOFiles.hpp"
#include "ScalarElement.hpp"


class Elastic_Riemann : public ProblemElasticity {
  double A_osc;
  double k_osc;
  double width;
  double shift;
  double lambda2;
  double mu2;
  double jump;
  double delta;
  double omega;
  double length;
public:
  Elastic_Riemann(std::shared_ptr<const DGDiscretization> _disc)
    : ProblemElasticity(_disc, 2),
      A_osc(0.5), k_osc(10), omega(1),
      width(0.125), shift(2.5), lambda2(lambda),
      mu2(mu), jump(2), delta(0.5), length(0.125) {
    Config::Get("k_osc", k_osc);
    Config::Get("A_osc", A_osc);
    Config::Get("width", width);
    Config::Get("shift", shift);
    Config::Get("jump", jump);
    Config::Get("lambda2", lambda2);
    Config::Get("mu2", mu2);
    Config::Get("delta", delta);
    Config::Get("length", length);
    Config::Get("omega", omega);
  }

  double phi(const Point &x) const {
    double y0 = x[0] - jump + x[1] * x[1] / 11;
    if (y0 < -delta) return 0;
    if (y0 > +delta) return 1;
    double y = 0.5 + 0.5 * y0 / delta;
    return (3 - 2 * y) * y * y;
  }

  VectorField Dphi(const Point &x) const {
    double y0 = x[0] - jump + x[1] * x[1] / 11;
    if (y0 < -delta) return zero;
    if (y0 > +delta) return zero;
    double y = 0.5 + 0.5 * y0 / delta;
    return (6 - 6 * y) * y * VectorField(1.0, 2 * x[1] / 11);
  }

  double Lambda(const Point &x) const {
    if (x[0] > 2)
      if (x[0] < 3)
        return lambda2;
    return lambda;

    double p = phi(x);
    return lambda2 * p + lambda * (1 - p);

    if (x[0] < jump) return lambda;
    else return lambda2;
  }

  double Mu(const Point &x) const {
    if (x[0] > 2)
      if (x[0] < 3)
        return mu2;
    return mu;

    double p = phi(x);
    return mu2 * p + mu * (1 - p);

    if (x[0] < jump) return mu;
    else return mu2;
  }

  bool RHS() const { return false; }

  bool Exact() const { return false; }

  const char *Name() const { return "Elastic_Riemann"; }

  double A0(double s) const {
    if (s * s >= width) return 0;
    return exp(-1 / (width - s * s)) / exp(-1 / width);
  }

  double A(double s) const { return A0(s - shift); }

  double ut(double t, const Point &x, int i) const {
    double y = A(x[0] - c_S * t);
    switch(i) {
      case 2:
        return -mu * y;
      case 4:
        return c_S * y;
      default:
        return 0;
    }
  }
};

class Elastic_PhaseField : public ProblemElasticity {
  double A_osc;
  double k_osc;
  double width;
  double shift;
  double lambda2;
  double mu2;
  double jump;
  double delta;
  double omega;
  double length;
public:
  Elastic_PhaseField(std::shared_ptr<const DGDiscretization> _disc)
    : ProblemElasticity(_disc, 2),
      A_osc(0.5), k_osc(10), omega(1),
      width(0.125), shift(2.5), lambda2(lambda),
      mu2(mu), jump(2), delta(0.5), length(0.125) {
    Config::Get("k_osc", k_osc);
    Config::Get("A_osc", A_osc);
    Config::Get("width", width);
    Config::Get("shift", shift);
    Config::Get("jump", jump);
    Config::Get("lambda2", lambda2);
    Config::Get("mu2", mu2);
    Config::Get("delta", delta);
    Config::Get("length", length);
    Config::Get("omega", omega);
  }

  double phi(const Point &x) const {
    //double y0 = x[0]-jump+x[1]*x[1]/11;
    double y0 = dist(x, Point(2.0, 2.0)) - 1;
    if (y0 < -delta) return 0;
    if (y0 > +delta) return 1;
    double y = 0.5 + 0.5 * y0 / delta;
    return (3 - 2 * y) * y * y;
  }

  VectorField Dphi(const Point &x) const {
    double y0 = dist(x, Point(2.0, 2.0)) - 1;
    if (y0 < -delta) return zero;
    if (y0 > +delta) return zero;
    return (1 / y0) * (x - Point(2.0, 2.0));
    double y = 0.5 + 0.5 * y0 / delta;
    return (6 - 6 * y) * y * VectorField(1.0, 2 * x[1] / 11);
  }

  double Lambda(const Point &x) const {
    double p = phi(x);
    return lambda2 * p + lambda * (1 - p);

    if (x[0] < jump) return lambda;
    else return lambda2;
  }

  double Mu(const Point &x) const {

    Exit("not used");

    double p = phi(x);
    return mu2 * p + mu * (1 - p);

    if (x[0] < jump) return mu;
    else return mu2;
  }

  Tensor _StressStrain(const cell &C, const Point &z, const Tensor &S) const {
    double p = phi(z);
    if (p == 0) {
      double s = 1 / (2.0 * mu);
      double ss = lambda / (2.0 * mu * (2.0 * lambda + 2.0 * mu));
      return s * S - ss * trace(S) * One;
    } else if (p == 1) {
      double s = 1 / (2.0 * mu2);
      double ss = lambda2 / (2.0 * mu2 * (2.0 * lambda2 + 2.0 * mu2));
      return s * S - ss * trace(S) * One;
    }
    double q = 1 - p;
    VectorField N = Dphi(z);
    N /= norm(N);
    VectorField T(N[1], -N[0]);
    Tensor NN = Product(N, N);
    Tensor NT = (Product(N, T) + Product(T, N));
    //Tensor NT = (1/sqrt(2)) * (Product(N,T) + Product(T,N));
    Tensor TT = Product(T, T);

    double Snn = N * (S * N);
    double Snt = T * (S * N);
    double Stt = T * (S * T);

    RVector b(5);
    b[0] = Snn * p;
    b[1] = Snn * q;
    b[2] = Snt * p;
    b[3] = Snt * q;
    b[4] = Stt;
    RMatrix A(5, 5);
    A = 0;
    A[0][0] = p * (2.0 * mu + lambda);
    A[1][1] = q * (2.0 * mu2 + lambda2);
    A[2][2] = p * 2 * mu;
    A[3][3] = q * 2 * mu2;
    A[4][4] = p * (2.0 * mu + lambda) + q * (2.0 * mu2 + lambda2);
    A[0][4] = A[4][0] = p * lambda;
    A[1][4] = A[4][1] = q * lambda2;
    A.Invert();
    RVector y(b);
    y = A * b;


    /*
	SmallVector b2(3);
	b2[0] = Snn;
	b2[1] = Snt;
	b2[2] = Stt;
	SmallMatrix A2(3,3);
	A2 = 0;
	A2[0][0] = 2.0 * mu + lambda;
	A2[1][1] = 2 * mu;
	A2[2][2] = 2.0 * mu + lambda;
	A2[0][2] = A2[2][0] = lambda;
	A2.Invert();
	SmallVector y2(A2,b2);
	double s = 1 / (2.0 * mu2);
	double ss = lambda2 / (2.0 * mu2 * (2.0 * lambda2 + 2.0 * mu2));
	Tensor E1 = s * S - ss * trace(S) * One;
	Tensor E0 = y2[0]*NN + y2[1]*NT + y2[2]*TT;
	Tensor E2 = (p*y[0]+q*y[1])*NN + (p*y[2]+q*y[3])*NT + y[4]*TT;
	mout << OUT(p) << OUT(q) << OUTX(E1) << OUTX(E0) << OUTX(E2);
	*/

    return (p * y[0] + q * y[1]) * NN + (p * y[2] + q * y[3]) * NT + y[4] * TT;
  }

  bool RHS() const { return false; }

  bool Exact() const { return false; }

  const char *Name() const { return "Elastic_PhaseField"; }

  double A0(double s) const {
    if (s * s >= width) return 0;
    return exp(-1 / (width - s * s)) / exp(-1 / width);
  }

  double A(double s) const { return A0(s - shift); }

  double ut(double t, const Point &x, int i) const {
    double c_S = sqrt(mu2 / rho);
    double y = A(x[0] - c_S * t);
    switch(i) {
      case 2:
        return -mu * y;
      case 4:
        return c_S * y;
      default:
        return 0;
    }
  }
};

class Elastic_PhaseFieldFracture : public ProblemElasticity {
  double A_osc;
  double k_osc;
  double width;
  double shift;
  double lambda2;
  double mu2;
  double jump;
  double delta;
  double omega;
  double length;
  double l_c;
  double spalt;
  double eta;
  double p_min;
  int degrad;
  int degrad_mode = 0;
  double angle, c_a, s_a;
public:
  Elastic_PhaseFieldFracture(std::shared_ptr<const DGDiscretization> _disc)
    : ProblemElasticity(_disc, 2),
      A_osc(0.5), k_osc(10), omega(1),
      width(0.125), shift(2.5), lambda2(lambda),
      mu2(mu), jump(2), delta(0.5), length(0.125), l_c(0), eta(0), spalt(0.125),
      degrad(5), p_min(1), angle(0) {
    Config::Get("l_c", l_c);
    Config::Get("p_min", p_min);
    if (l_c > 0) Config::Get("degradation", degrad);
    if (l_c > 0) Config::Get("degradation_mode", degrad_mode);
    Config::Get("eta", eta);
    Config::Get("k_osc", k_osc);
    Config::Get("A_osc", A_osc);
    Config::Get("width", width);
    Config::Get("shift", shift);
    Config::Get("jump", jump);
    Config::Get("spalt", spalt);
    Config::Get("lambda2", lambda2);
    Config::Get("mu2", mu2);
    Config::Get("delta", delta);
    Config::Get("length", length);
    Config::Get("omega", omega);
    Config::Get("angle", angle);
    c_a = cos(angle);
    s_a = sin(angle);
  }

  double g0(double s) const {
    switch(degrad) {
      case 1:
        return s;
      case 2:
        return s * (2 - s);
      case 3:
        return s * s * (3 - 2 * s);
      case 4:
        return s * s * s * (4 - 3 * s);
      case 5:
        return s * s * s * s * (5 - 4 * s);
      case -2:
        return s * s;
      case -1:
        return s;
      default: Exit("not defined");
    }
  }

  double g_refl(double x) const {
    if (l_c == 0) {
      if (x == 0) return eta;
      return 1;
    }
    return g0(1 - exp(-x / l_c)) * (1 - eta) + eta;
  }

  double phi(const Point &z) const {
    Point zz = z - Point(2.0, 2.0);
    zz = Point(c_a * zz[0] + s_a * zz[1], -s_a * zz[0] + c_a * zz[1]) + Point(2.0, 2.0);
    double x = abs(zz[0] - 2.0);
    if (zz[1] > 2.5) x = dist(zz, Point(2.0, 2.5));
    else if (zz[1] < 1.5) x = dist(zz, Point(2.0, 1.5));
    if (x < spalt) return eta;
    return g_refl(x - spalt);
  }

  VectorField Dphi(const Point &x) const {
    //double y0 = x[0]-jump+x[1]*x[1]/11;
    double y0 = dist(x, Point(2.0, 2.0)) - 1;
    if (y0 < -delta) return zero;
    if (y0 > +delta) return zero;
    return (1 / y0) * (x - Point(2.0, 2.0));
    double y = 0.5 + 0.5 * y0 / delta;
    return (6 - 6 * y) * y * VectorField(1.0, 2 * x[1] / 11);
  }

  double Lambda(const Point &x) const {
    double p = phi(x);
    return lambda * p;

    return lambda2 * p + lambda * (1 - p);

    if (x[0] < jump) return lambda;
    else return lambda2;
  }

  double Mu(const Point &x) const {
    double p = phi(x);
    return mu * p;

    return mu2 * p + mu * (1 - p);

    if (x[0] < jump) return mu;
    else return mu2;
  }

  double Rho(const Point &x) const {
    double p = phi(x);
    return rho * p;
  }

  Tensor _StressStrain(const cell &C, const Point &z, const Tensor &S) const {
    double p = phi(z);
    if (p == 0) {
      double s = 1 / (2.0 * mu);
      double ss = lambda / (2.0 * mu * (2.0 * lambda + 2.0 * mu));
      return s * S - ss * trace(S) * One;
    } else if (p == 1) {
      double s = 1 / (2.0 * mu2);
      double ss = lambda2 / (2.0 * mu2 * (2.0 * lambda2 + 2.0 * mu2));
      return s * S - ss * trace(S) * One;
    }
    double q = 1 - p;
    VectorField N = Dphi(z);
    N /= norm(N);
    VectorField T(N[1], -N[0]);
    Tensor NN = Product(N, N);
    Tensor NT = (Product(N, T) + Product(T, N));
    //Tensor NT = (1/sqrt(2)) * (Product(N,T) + Product(T,N));
    Tensor TT = Product(T, T);

    double Snn = N * (S * N);
    double Snt = T * (S * N);
    double Stt = T * (S * T);

    RVector b(5);
    b[0] = Snn * p;
    b[1] = Snn * q;
    b[2] = Snt * p;
    b[3] = Snt * q;
    b[4] = Stt;
    RMatrix A(5, 5);
    A = 0;
    A[0][0] = p * (2.0 * mu + lambda);
    A[1][1] = q * (2.0 * mu2 + lambda2);
    A[2][2] = p * 2 * mu;
    A[3][3] = q * 2 * mu2;
    A[4][4] = p * (2.0 * mu + lambda) + q * (2.0 * mu2 + lambda2);
    A[0][4] = A[4][0] = p * lambda;
    A[1][4] = A[4][1] = q * lambda2;
    A.Invert();
    RVector y(b);
    y = A * b;


    /*
	SmallVector b2(3);
	b2[0] = Snn;
	b2[1] = Snt;
	b2[2] = Stt;
	SmallMatrix A2(3,3);
	A2 = 0;
	A2[0][0] = 2.0 * mu + lambda;
	A2[1][1] = 2 * mu;
	A2[2][2] = 2.0 * mu + lambda;
	A2[0][2] = A2[2][0] = lambda;
	A2.invert();
	SmallVector y2(A2,b2);
	double s = 1 / (2.0 * mu2);
	double ss = lambda2 / (2.0 * mu2 * (2.0 * lambda2 + 2.0 * mu2));
	Tensor E1 = s * S - ss * trace(S) * One;
	Tensor E0 = y2[0]*NN + y2[1]*NT + y2[2]*TT;
	Tensor E2 = (p*y[0]+q*y[1])*NN + (p*y[2]+q*y[3])*NT + y[4]*TT;
	mout << OUT(p) << OUT(q) << OUTX(E1) << OUTX(E0) << OUTX(E2);
	*/

    return (p * y[0] + q * y[1]) * NN + (p * y[2] + q * y[3]) * NT + y[4] * TT;
  }

  bool RHS() const { return false; }

  bool Exact() const { return false; }

  const char *Name() const { return "Elastic_PhaseFieldFracture"; }

  double A0(double s) const {
    if (abs(s) >= width)
      return 0;
    if (abs(s) <= 0.5 * width)
      return sin(std::numbers::pi * s / width);
    return sin(std::numbers::pi * s / width) *
           exp((abs(s) - 0.5 * width) * (abs(s) - 0.5 * width) /
               (s * s - width * width));
  }

  double A(double s) const { return A0(s - shift); }

  double ut(double t, const Point &x, int i) const {
    if (abs(x[1] - 2) > 1) return 0;
    double c_S = sqrt(mu2 / rho);
    double y = A(x[0] - c_S * t);
    switch(i) {
      case 2:
        return -mu * y;
      case 4:
        return c_S * y;
      default:
        return 0;
    }
  }
};

class Elastic_PhaseFieldNeumann_old : public ProblemElasticity {
  double A_osc;
  double k_osc;
  double width;
  double width2;
  double shift;
  double lambda2;
  double mu2;
  double jump;
  double delta;
  double omega;
  double length;
  double l_c;
  double spalt;
  double eta;
  double p_min;
  int degrad;
  double angle, c_a, s_a;
public:
  Elastic_PhaseFieldNeumann_old(std::shared_ptr<const DGDiscretization> _disc)
    : ProblemElasticity(_disc, 2),
      A_osc(0.5), k_osc(10), omega(1),
      width(0.125), width2(0.01), shift(2.5), lambda2(lambda),
      mu2(mu), jump(2), delta(0.5), length(0.125), l_c(0), eta(0), spalt(0.125),
      degrad(5), p_min(1), angle(0) {
    Config::Get("l_c", l_c);
    Config::Get("p_min", p_min);
    if (l_c > 0) Config::Get("degradation", degrad);
    Config::Get("eta", eta);
    Config::Get("k_osc", k_osc);
    Config::Get("A_osc", A_osc);
    Config::Get("width", width);
    width2 = width * width;
    Config::Get("shift", shift);
    Config::Get("jump", jump);
    Config::Get("spalt", spalt);
    Config::Get("lambda2", lambda2);
    Config::Get("mu2", mu2);
    Config::Get("delta", delta);
    Config::Get("length", length);
    Config::Get("omega", omega);
    Config::Get("angle", angle);
    c_a = cos(angle);
    s_a = sin(angle);
  }

  double g0(double s) const {
    switch(degrad) {
      case 1:
        return s;
      case 2:
        return s * (2 - s);
      case 3:
        return s * s * (3 - 2 * s);
      case 4:
        return s * s * s * (4 - 3 * s);
      case 5:
        return s * s * s * s * (5 - 4 * s);
      case -2:
        return s * s;
      case -1:
        return s;
      default: Exit("not defined");
    }
  }

  double g_refl(double x) const {
    if (l_c == 0) {
      if (x == 0) return eta;
      return 1;
    }
    return g0(1 - exp(-x / l_c)) * (1 - eta) + eta;
  }

  double phi(const Point &z) const {
    Point zz = z - Point(2.0, 2.0);
    zz = Point(c_a * zz[0] + s_a * zz[1], -s_a * zz[0] + c_a * zz[1]) + Point(2.0, 2.0);
    double x = abs(zz[0] - 2.0);
    if (zz[1] > 2.5) x = dist(zz, Point(2.0, 2.5));
    else if (zz[1] < 1.5) x = dist(zz, Point(2.0, 1.5));
    if (x < spalt) return eta;
    return g_refl(x - spalt);
  }

  VectorField Dphi(const Point &x) const {
    //double y0 = x[0]-jump+x[1]*x[1]/11;
    double y0 = dist(x, Point(2.0, 2.0)) - 1;
    if (y0 < -delta) return zero;
    if (y0 > +delta) return zero;
    return (1 / y0) * (x - Point(2.0, 2.0));
    double y = 0.5 + 0.5 * y0 / delta;
    return (6 - 6 * y) * y * VectorField(1.0, 2 * x[1] / 11);
  }

  double Lambda(const Point &x) const {
    return lambda;
    if (x[0] < jump) return lambda;
    double p = phi(x);
    return lambda * p;

    return lambda2 * p + lambda * (1 - p);
    if (x[0] < jump) return lambda;
    else return lambda2;
  }

  double Mu(const Point &x) const {
    return mu;
    if (x[0] < jump) return mu;
    double p = phi(x);
    return mu * p;

    return mu2 * p + mu * (1 - p);
    if (x[0] < jump) return mu;
    else return mu2;
  }

  double Rho(const Point &x) const {
    return rho;
    double p = phi(x);
    return rho * p;
  }

  Tensor _StressStrain(const cell &C, const Point &z, const Tensor &S) const {
    double p = phi(z);
    if (p == 0) {
      double s = 1 / (2.0 * mu);
      double ss = lambda / (2.0 * mu * (2.0 * lambda + 2.0 * mu));
      return s * S - ss * trace(S) * One;
    } else if (p == 1) {
      double s = 1 / (2.0 * mu2);
      double ss = lambda2 / (2.0 * mu2 * (2.0 * lambda2 + 2.0 * mu2));
      return s * S - ss * trace(S) * One;
    }
    double q = 1 - p;
    VectorField N = Dphi(z);
    N /= norm(N);
    VectorField T(N[1], -N[0]);
    Tensor NN = Product(N, N);
    Tensor NT = (Product(N, T) + Product(T, N));
    //Tensor NT = (1/sqrt(2)) * (Product(N,T) + Product(T,N));
    Tensor TT = Product(T, T);

    double Snn = N * (S * N);
    double Snt = T * (S * N);
    double Stt = T * (S * T);

    RVector b(5);
    b[0] = Snn * p;
    b[1] = Snn * q;
    b[2] = Snt * p;
    b[3] = Snt * q;
    b[4] = Stt;
    RMatrix A(5, 5);
    A = 0;
    A[0][0] = p * (2.0 * mu + lambda);
    A[1][1] = q * (2.0 * mu2 + lambda2);
    A[2][2] = p * 2 * mu;
    A[3][3] = q * 2 * mu2;
    A[4][4] = p * (2.0 * mu + lambda) + q * (2.0 * mu2 + lambda2);
    A[0][4] = A[4][0] = p * lambda;
    A[1][4] = A[4][1] = q * lambda2;
    A.Invert();
    RVector y(b);
    y = A * b;

    /*
	SmallVector b2(3);
	b2[0] = Snn;
	b2[1] = Snt;
	b2[2] = Stt;
	SmallMatrix A2(3,3);
	A2 = 0;
	A2[0][0] = 2.0 * mu + lambda;
	A2[1][1] = 2 * mu;
	A2[2][2] = 2.0 * mu + lambda;
	A2[0][2] = A2[2][0] = lambda;
	A2.invert();
	SmallVector y2(A2,b2);
	double s = 1 / (2.0 * mu2);
	double ss = lambda2 / (2.0 * mu2 * (2.0 * lambda2 + 2.0 * mu2));
	Tensor E1 = s * S - ss * trace(S) * One;
	Tensor E0 = y2[0]*NN + y2[1]*NT + y2[2]*TT;
	Tensor E2 = (p*y[0]+q*y[1])*NN + (p*y[2]+q*y[3])*NT + y[4]*TT;
	mout << OUT(p) << OUT(q) << OUTX(E1) << OUTX(E0) << OUTX(E2);
	*/

    return (p * y[0] + q * y[1]) * NN + (p * y[2] + q * y[3]) * NT + y[4] * TT;
  }

  bool RHS() const { return false; }

  bool Exact() const { return true; }

  const char *Name() const { return "Elastic_PhaseFieldNeumann"; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return exp(-1 / (width2 - s * s)) / exp(-1 / width2);
  }

  double A(double s) const { return A0(s - shift); }

  double ut(double t, const Point &x, int i) const {
    if (abs(x[1] - 2) > length) return 0;
    double c_S = sqrt(mu2 / rho);
    //if (x[1] < 0.25) return 0;
    //if (x[1] > 0.75) return 0;
    double y = A(x[0] - c_S * t);
    double y_r = A(-x[0] - c_S * t);
    switch(i) {
      case 2:
        return mu * (y_r - y);
      case 4:
        return c_S * (y + y_r);
      default:
        return 0;
    }
  }
};

class _ElasticPhaseField : public ProblemElasticity {
  double l_c;
  int degrad;
  double eta;
  double eta_stat;
  double eta_dyn;
  double width;
  double lambda2;
  double mu2;
  double rho2;
  double width2;
public:
  const char *Name() const { return "ElasticPhaseField"; }

  _ElasticPhaseField(std::shared_ptr<const DGDiscretization> _disc)
    : ProblemElasticity(_disc, 2), l_c(1), degrad(5), eta(1), eta_stat(1),
      eta_dyn(1), width(0.125), width2(0.01) {
    Config::Get("l_c", l_c);
    if (l_c > 0) Config::Get("degradation", degrad);
    Config::Get("eta", eta);
    eta_stat = eta;
    eta_dyn = eta;
    Config::Get("eta_stat", eta_stat);
    Config::Get("eta_dyn", eta_dyn);
    Config::Get("width", width);
    width2 = width * width;
    Config::Get("lambda2", lambda2);
    Config::Get("mu2", mu2);
    Config::Get("rho2", rho2);
  }

  void PrintInfo() const {
    //int p = logging->GetPrecision();
    //logging->SetPrecision(12);
    mout << " degradation " << degrad << endl
         << " l_c         " << l_c << endl
         << " lambda2     " << lambda2 << endl
         << " mu2         " << mu2 << endl
         << " rho2        " << rho2 << endl
         << " eta_stat    " << eta_stat << endl
         << " eta_dyn     " << eta_dyn << endl;
    //logging->SetPrecision(p);
  }

  double g(double s) const {
    switch(degrad) {
      case 0:
        return s;
      case 1:
        return s * (2 - s);
      case 2:
        return s * s * (3 - 2 * s);
      case 3:
        return s * s * s * (4 - 3 * s);
      case 4:
        return s * s * s * s * (5 - 4 * s);
      case -2:
        return s * s;
      case -1:
        return s;
      default: Exit("not defined");
    }
  }

  double g_stat(double s) const {
    return std::max(g(s), eta_stat);
    return eta_stat + g(s) * (1 - eta_stat);
  }

  double g_dyn(double s) const {
    return std::max(g(s), eta_dyn);
    return eta_dyn + g(s) * (1 - eta_dyn);
  }

  double s(const Point &x) const {
    if (x[0] >= 0) return 0;
    return 1 - exp(x[0] / l_c);
    if (x[0] >= 0) return eta;
    return 1 - exp(x[0] / l_c) * (1 - eta);
  }

  double Lambda(const Point &x) const {
    if (x[0] > 0) return eta_stat * lambda;
    return (eta + g(s(x)) * (1 - eta)) * lambda;
    return g_stat(s(x)) * lambda;
  }

  double Mu(const Point &x) const {
    if (x[0] > 0) return eta_stat * mu;
    return (eta + g(s(x)) * (1 - eta)) * mu;
    return g_stat(s(x)) * mu;
  }

  double Rho(const Point &x) const {
    if (x[0] > 0) return eta_dyn * rho;
    return (eta + g(s(x)) * (1 - eta)) * rho;
    return g_dyn(s(x)) * rho;
  }

  double A0(double s) const {
    using std::numbers::pi;
    if (s * s >= width2)
      return 0;
    return sin(pi * s / width) * exp(-1 / (width2 - s * s)) / exp(-1 / width2);

    if (abs(s) >= width)
      return 0;
    if (abs(s) <= 0.5 * width)
      return sin(pi * s / width);
    return sin(pi * s / width) *
           exp((abs(s) - 0.5 * width) * (abs(s) - 0.5 * width) /
               (s * s - width * width));
  }
};

class __ElasticPhaseField : public ProblemElasticity {
  double l_c;
  int degrad;
  double width;
  double width2;
  double lambda2;
  double mu2;
  double rho2;
public:
  const char *Name() const { return "ElasticPhaseField"; }

  __ElasticPhaseField(std::shared_ptr<const DGDiscretization> _disc)
    : ProblemElasticity(_disc, 2), l_c(1), degrad(5),
      width(0.125), width2(0.01) {
    Config::Get("l_c", l_c);
    if (l_c > 0) Config::Get("degradation", degrad);
    Config::Get("width", width);
    width2 = width * width;
    Config::Get("lambda2", lambda2);
    Config::Get("mu2", mu2);
    Config::Get("rho2", rho2);
  }

  void PrintInfo() const {
    //int p = logging->GetPrecision();
    //logging->SetPrecision(12);
    mout << " degradation " << degrad << endl
         << " l_c         " << l_c << endl
         << " lambda2     " << lambda2 << endl
         << " mu2         " << mu2 << endl
         << " rho2        " << rho2 << endl;
    //logging->SetPrecision(p);
  }

  double g(double s) const {
    switch(degrad) {
      case 0:
        return s;
      case 1:
        return s * (2 - s);
      case 2:
        return s * s * (3 - 2 * s);
      case 3:
        return s * s * s * (4 - 3 * s);
      case 4:
        return s * s * s * s * (5 - 4 * s);
      case -2:
        return s * s;
      case -1:
        return s;
      default: Exit("not defined");
    }
  }

  double s(const Point &x) const {
    if (x[0] >= 0) return 0;
    return 1 - exp(x[0] / l_c);
    return 0.5 + 0.5 * tanh(-x[0] / l_c);
  }

  double Rho(const Point &x) const {
    if (l_c > 0) return rho2 + g(s(x)) * (rho - rho2);
    else return rho;
  }

  double Lambda(const Point &x) const {
    if (l_c > 0) return lambda2 + g(s(x)) * (lambda - lambda2);
    else return lambda;
  }

  double Mu(const Point &x) const {
    if (l_c > 0) return mu2 + g(s(x)) * (mu - mu2);
    else return mu;
  }

  double A0(double s) const {
    using std::numbers::pi;
    if (s * s >= width2)
      return 0;
    return sin(pi * s / width) * exp(-1 / (width2 - s * s)) / exp(-1 / width2);

    if (abs(s) >= width)
      return 0;
    if (abs(s) <= 0.5 * width)
      return sin(pi * s / width);
    return sin(pi * s / width) *
           exp((abs(s) - 0.5 * width) * (abs(s) - 0.5 * width) /
               (s * s - width * width));
  }
};

class ElasticPhaseField_old : public ProblemElasticity {
  int degrad;
  double eta;
  double lambda0;
  double mu0;
  double rho0;
protected:
  double lambda2;
  double mu2;
  double rho2;
  double c2_P;
  double c2_S;
  double l_c;
  double width;
  double width2;
  double delta;
  bool use_pf;
public:
  const char *Name() const { return "ElasticPhaseField"; }

  ElasticPhaseField_old(std::shared_ptr<const DGDiscretization> _disc, int _dim = 2)
    : ProblemElasticity(_disc, _dim), l_c(1), degrad(1), eta(1),
      width(0.125), width2(0.01), use_pf(false), rho2(rho),
      lambda2(lambda), mu2(mu), delta(0.1) {
    Config::Get("l_c", l_c);
    if (l_c > 0) Config::Get("degradation", degrad);
    Config::Get("eta", eta);
    Config::Get("width", width);
    width2 = width * width;
    Config::Get("lambda2", lambda2);
    Config::Get("mu2", mu2);
    Config::Get("rho2", rho2);
    Config::Get("delta", delta);
    Config::Get("use_pf_vector", use_pf);
    c2_P = sqrt((2.0 * mu2 + lambda2) / rho2);
    c2_S = sqrt(mu2 / rho2);
    lambda0 = lambda * eta;
    mu0 = mu * eta;
    rho0 = rho * eta;

    int N = 20;
    double h = 2 * l_c / N;
    double t = 0;
    for (int n = -N; n <= N; ++n) {
      Point x(n * h, 0.0);
      t += h / C_s(x);
      // mout << OUT(x) << OUT(g(s(x[0]))) << OUT(C_s(x)) << OUT(t) << endl;
    }
    mout << OUT(c_S) << OUT(c2_S) << OUT(2 * l_c * (1 / c_S + 1 / c2_S)) << OUT(t)
         << endl;
    t = 0;
    for (int n = -N; n <= N; ++n) {
      Point x(n * h, 0.0);
      t += h / C_p(x);
      // mout << OUT(x) << OUT(g(s(x[0]))) << OUT(C_s(x)) << OUT(t) << endl;
    }
    mout << OUT(c_P) << OUT(c2_P) << OUT(2 * l_c * (1 / c_P + 1 / c2_P)) << OUT(t)
         << endl;
  }

  virtual void UpdatePF() const {
    for (row r = pf->rows(); r != pf->rows_end(); ++r)
      (*pf)(r, 0) = Phasefield(r());
  }

  void SetPF(const Meshes &MM) {
    if (!use_pf) return;
    if (!pf) {
      pf_disc = std::make_shared<LagrangeDiscretizationWithOverlap>(MM, 1, 1);
      pf = new Vector(pf_disc);
      pf2 = new Vector(pf_disc);
      pf_old = new Vector(*pf);
      U_disc = std::make_shared<LagrangeDiscretizationWithOverlap>(MM, 1, 2);
      U = new Vector(U_disc);
      *U = 0;
    }
    *pf = 1;
    *pf2 = 1;
    *pf_old = *pf;
    mout << pf->Min() << " <= pf <= " << pf->Max() << endl;
  }

  void PrintInfo() const {
    //int p = logging->GetPrecision();
    //logging->SetPrecision(12);
    mout << " degradation " << degrad << endl
         << " l_c         " << l_c << endl
         << " delta       " << delta << endl
         << " c_P         " << c_P << endl
         << " c2_P        " << c2_P << endl
         << " c_S         " << c_S << endl
         << " c2_S        " << c2_S << endl
         << " lambda      " << lambda << endl
         << " mu          " << mu << endl
         << " nu          " << 0.5 * lambda / (mu + lambda) << endl
         << " rho         " << rho << endl
         << " lambda2     " << lambda2 << endl
         << " mu2         " << mu2 << endl
         << " rho2        " << rho2 << endl;
    //logging->SetPrecision(p);
  }

  double g(double s) const {
    switch(degrad) {
      case 0:
        return s;
      case 1:
        return s * (2 - s);
      case 2:
        return s * s * (3 - 2 * s);
      case 3:
        return s * s * s * (4 - 3 * s);
      case 4:
        return s * s * s * s * (5 - 4 * s);
      case -2:
        return s * s;
      case -1:
        return s;
      default: Exit("not defined");
    }
  }

  double s(double x) const {
    if (l_c == 0) {
      if (x <= 0) return 1;
      return 0;
    }
    return 0.5 + 0.5 * tanh(-(x + delta) / l_c);
    // return 1 - exp(-(x+delta)/l_c);
  }

  virtual double Phasefield(const Point &x) const {
    return g(s(x[0]));
  }

  virtual double Phasefield(const cell &C, const Point &lz, const Point &z) const {
    if (pf) {
      ScalarElement E(*pf, *C);
      return E.Value(lz, *pf);
    }
    return Phasefield(z);
  }

  virtual double ProjectedPhasefield(const cell &C,
                                     const Point &lz,
                                     const Point &z) const {
    return g(Phasefield(C, lz, z));
  }

  virtual double Phasefield2(const cell &C, const Point &lz, const Point &z) const {
    if (pf2) {
      ScalarElement E(*pf2, *C);
      return E.Value(lz, *pf2);
    }
    return Phasefield(z);
  }

  virtual double ProjectedPhasefield2(const cell &C,
                                      const Point &lz,
                                      const Point &z) const {
    return g(Phasefield2(C, lz, z));
  }

  virtual double Phasefield_old(const cell &C, const Point &lz, const Point &z) const {
    if (pf_old) {
      ScalarElement E(*pf_old, *C);
      return E.Value(lz, *pf_old);
    }
    return Phasefield(z);
  }

  double PhasefieldDifference(const cell &C, const Point &lz, const Point &z) const {
    double x0 = z[0];
    if (x0 <= 0) return 1 - Phasefield(C, lz, z);
    return Phasefield(C, lz, z);
  }

  virtual double Lambda(const cell &C, const Point &lz, const Point &z) const {
    return lambda2 + ProjectedPhasefield2(C, lz, z) * (lambda - lambda2);
  }

  virtual double Mu(const cell &C, const Point &lz, const Point &z) const {
    return mu2 + ProjectedPhasefield2(C, lz, z) * (mu - mu2);
  }

  virtual double Rho(const cell &C, const Point &lz, const Point &z) const {
    return rho2 + ProjectedPhasefield2(C, lz, z) * (rho - rho2);
  }

  double A0(double s) const {
    if (s * s >= width2)
      return 0;
    return sin(std::numbers::pi * s / width) * exp(-1 / (width2 - s * s)) /
           exp(-1 / width2);
  }
};

class ElasticPhaseField : public ProblemElasticity {
  int degrad;
  double eta;
  double scale_g = 10;
  double shift_g = 0.1;
  double lambda0;
  double mu0;
  double rho0;
protected:
  double tau_reg;
  double lambda_reg;
  double mu_reg;
  double rho_reg;
  double tau1;
  double lambda1;
  double mu1;
  double rho1;
  double tau2;
  double lambda2;
  double mu2;
  double rho2;
  double tau3;
  double lambda3;
  double mu3;
  double rho3;
  double c2_P;
  double c2_S;
  double l_c;
  double width;
  double width2;
  double delta;
  bool use_pf;
  bool pf_init = false;
public:
  const char *Name() const { return "ElasticPhaseField"; }

  ElasticPhaseField(std::shared_ptr<const DGDiscretization> _disc, int _dim = 2)
    : ProblemElasticity(_disc, _dim), l_c(1), degrad(-1), eta(1),
      width(0.125), width2(0.01), use_pf(false), rho2(rho),
      lambda2(lambda), mu2(mu), delta(0.1) {
    Config::Get("l_c", l_c);
    if (l_c > 0) Config::Get("degradation", degrad);
    Config::Get("eta", eta);
    Config::Get("scale_g", scale_g);
    Config::Get("shift_g", shift_g);
    Config::Get("width", width);
    width2 = width * width;
    Config::Get("lambda2", lambda2);
    Config::Get("mu2", mu2);
    Config::Get("rho2", rho2);
    lambda_reg = lambda2;
    mu_reg = mu2;
    rho_reg = rho2;
    Config::Get("lambda_reg", lambda_reg);
    Config::Get("mu_reg", mu_reg);
    Config::Get("rho_reg", rho_reg);
    lambda1 = lambda2;
    mu1 = mu2;
    rho1 = rho2;
    lambda3 = lambda2;
    mu3 = mu2;
    rho3 = rho2;
    tau1 = tau2 = tau3 = 0;
    Config::Get("tau1", tau1);
    Config::Get("tau2", tau2);
    Config::Get("tau3", tau3);
    tau_reg = tau2;
    Config::Get("lambda3", lambda3);
    Config::Get("mu3", mu3);
    Config::Get("rho3", rho3);
    Config::Get("lambda1", lambda1);
    Config::Get("mu1", mu1);
    Config::Get("rho1", rho1);
    Config::Get("delta", delta);
    Config::Get("use_pf_vector", use_pf);
    Config::Get("pf_init", pf_init);
    c2_P = sqrt((2.0 * mu2 + lambda2) / rho2);
    c2_S = sqrt(mu2 / rho2);
    lambda0 = lambda * eta;
    mu0 = mu * eta;
    rho0 = rho * eta;

    int N = 20;
    double h = 2 * l_c / N;
    double t = 0;
    for (int n = -N; n <= N; ++n) {
      Point x(n * h, 0.0);
      t += h / C_s(x);
      // mout << OUT(x) << OUT(g(s(x[0]))) << OUT(C_s(x)) << OUT(t) << endl;
    }
    // mout << OUT(c_S) << OUT(c2_S) << OUT(2*l_c*(1/c_S+1/c2_S)) << OUT(t) << endl;
    t = 0;
    for (int n = -N; n <= N; ++n) {
      Point x(n * h, 0.0);
      t += h / C_p(x);
      // mout << OUT(x) << OUT(g(s(x[0]))) << OUT(C_s(x)) << OUT(t) << endl;
    }
    //mout << OUT(c_P) << OUT(c2_P) << OUT(2*l_c*(1/c_P+1/c2_P)) << OUT(t) << endl;
  }

  virtual void UpdatePF() const {
    mout << "Update PF" << endl;
    for (row r = pf->rows(); r != pf->rows_end(); ++r)
      (*pf)(r, 0) = Phasefield(r());
  }

  void SetMat1() {
    mout << "Set Mat 1 " << OUT(lambda1) << OUT(mu1) << OUT(rho1) << OUT(tau1) << endl;
    tau_reg = tau1;
    lambda_reg = lambda1;
    mu_reg = mu1;
    rho_reg = rho1;
  }

  void SetMat(double s) {
    tau_reg = s * tau1;
    lambda_reg = s * lambda1 + (1 - s) * lambda;
    mu_reg = s * mu1 + (1 - s) * mu;
    rho_reg = s * rho1 + (1 - s) * rho;
    mout << "Set Mat " << OUT(s) << OUT(lambda_reg) << OUT(mu_reg) << OUT(rho_reg)
         << endl;
  }

  void SetMat3() {
    mout << "Set Mat3 " << OUT(lambda3) << OUT(mu3) << OUT(rho3) << OUT(tau3) << endl;
    tau_reg = tau3;
    lambda_reg = lambda3;
    mu_reg = mu3;
    rho_reg = rho3;
  }

  void SetPF(const Meshes &MM) {
    if (!use_pf) return;
    if (!pf) {
      pf_disc = std::make_shared<LagrangeDiscretizationWithOverlap>(MM, 1, 1);
      pf = new Vector(pf_disc);
      pf2 = new Vector(pf_disc);
      pf_old = new Vector(*pf);
      U_disc = std::make_shared<LagrangeDiscretizationWithOverlap>(MM, 1, 2);
      U = new Vector(U_disc);
      *U = 0;
    }
    *pf = 1;
    *pf2 = 1;
    *pf_old = *pf;
    if (pf_init) {
      row r = pf2->find_row(Point(0.5, 1));
      if (r != pf2->rows_end())
        (*pf2)(r, 0) = 0;
    }
    mout << pf->Min() << " <= pf <= " << pf->Max() << endl;
    mout << pf2->Min() << " <= pf2 <= " << pf2->Max() << endl;
  }

  void PrintInfo() const {
    //int p = logging->GetPrecision();
    //logging->SetPrecision(12);
    mout << " degradation " << degrad << endl
         << " l_c         " << l_c << endl
         << " scale_g     " << scale_g << endl
         << " shift_g     " << shift_g << endl
         << " delta       " << delta << endl
         << " c_P         " << c_P << endl
         << " c2_P        " << c2_P << endl
         << " c_S         " << c_S << endl
         << " c2_S        " << c2_S << endl
         << " lambda      " << lambda << endl
         << " mu          " << mu << endl
         << " nu          " << 0.5 * lambda / (mu + lambda) << endl
         << " rho         " << rho << endl
         << " mu_reg      " << mu << endl
         << " nu_reg      " << 0.5 * lambda_reg / (mu_reg + lambda_reg) << endl
         << " rho_reg     " << rho_reg << endl
         << " lambda1     " << lambda1 << endl
         << " mu1         " << mu1 << endl
         << " rho1        " << rho1 << endl
         << " lambda2     " << lambda2 << endl
         << " mu2         " << mu2 << endl
         << " rho2        " << rho2 << endl
         << " lambda3     " << lambda3 << endl
         << " mu3         " << mu3 << endl
         << " rho3        " << rho3 << endl;
    //logging->SetPrecision(p);
  }

  double g(double s) const {
    switch(degrad) {
      case 0:
        return s;
      case 1:
        return s * s;
      case 2:
        return s * s * (3 - 2 * s);
      case 3:
        return s * s * s * (4 - 3 * s);
      case 4:
        return s * s * s * s * (5 - 4 * s);
      case -1:
        return 0.5 + 0.5 * tanh(scale_g * (s - shift_g));
      case -2:
        return sin(0.5 * std::numbers::pi * s);
      case -3:
        return 0.5 - 0.5 * cos(std::numbers::pi * s);
      default:
        Exit("not defined");
    }
  }

  double g4(double s) const {
    return s * s * s * s * (5 - 4 * s);
  }

  double s(double x) const {
    if (l_c == 0) {
      if (x <= 0) return 1;
      return 0;
    }
    return 0.5 + 0.5 * tanh(-(x + delta) / l_c);
    // return 1 - exp(-(x+delta)/l_c);
  }

  virtual double Phasefield(const Point &x) const {
    return g(s(x[0]));
  }

  virtual double Phasefield(const cell &C, const Point &lz, const Point &z) const {
    if (pf) {
      ScalarElement E(*pf, *C);
      return E.Value(lz, *pf);
    }
    return Phasefield(z);
  }

  virtual double ProjectedPhasefield(const cell &C,
                                     const Point &lz,
                                     const Point &z) const {
    return g(Phasefield(C, lz, z));
  }

  virtual double Phasefield2(const cell &C, const Point &lz, const Point &z) const {
    if (pf2) {
      ScalarElement E(*pf2, *C);
      return E.Value(lz, *pf2);
    }
    return Phasefield(z);
  }

  virtual double OldPhasefield2(const cell &C, const Point &lz, const Point &z) const {
    if (pf_old) {
      ScalarElement E(*pf_old, *C);
      return E.Value(lz, *pf_old);
    }
    return Phasefield(z);
  }

  virtual double ProjectedPhasefield2(const cell &C,
                                      const Point &lz,
                                      const Point &z) const {
    return g(Phasefield2(C, lz, z));
  }

  virtual double ProjectedPhasefield2_4(const cell &C,
                                        const Point &lz,
                                        const Point &z) const {
    return g4(Phasefield2(C, lz, z));
  }

  virtual double OldProjectedPhasefield2(const cell &C,
                                         const Point &lz,
                                         const Point &z) const {
    return g(OldPhasefield2(C, lz, z));
  }

  virtual double Phasefield_old(const cell &C, const Point &lz, const Point &z) const {
    if (pf_old) {
      ScalarElement E(*pf_old, *C);
      return E.Value(lz, *pf_old);
    }
    return Phasefield(z);
  }

  double PhasefieldDifference(const cell &C, const Point &lz, const Point &z) const {
    double x0 = z[0];
    if (x0 <= 0) return 1 - Phasefield(C, lz, z);
    return Phasefield(C, lz, z);
  }

  virtual double Tau(const cell &C, const Point &lz, const Point &z) const {
    return (1 - ProjectedPhasefield2(C, lz, z)) * tau_reg;
  }

  virtual double OldTau(const cell &C, const Point &lz, const Point &z) const {
    return (1 - OldProjectedPhasefield2(C, lz, z)) * tau_reg;
  }

  virtual double Lambda(const cell &C, const Point &lz, const Point &z) const {
    return lambda_reg + ProjectedPhasefield2(C, lz, z) * (lambda - lambda_reg);
  }

  virtual double Mu(const cell &C, const Point &lz, const Point &z) const {
    return mu_reg + ProjectedPhasefield2(C, lz, z) * (mu - mu_reg);
  }

  virtual double Rho(const cell &C, const Point &lz, const Point &z) const {
    return rho_reg + ProjectedPhasefield2(C, lz, z) * (rho - rho_reg);
  }

  virtual double OldLambda(const cell &C, const Point &lz, const Point &z) const {
    return lambda_reg + OldProjectedPhasefield2(C, lz, z) * (lambda - lambda_reg);
  }

  virtual double OldMu(const cell &C, const Point &lz, const Point &z) const {
    return mu_reg + OldProjectedPhasefield2(C, lz, z) * (mu - mu_reg);
  }

  virtual double OldRho(const cell &C, const Point &lz, const Point &z) const {
    return rho_reg + OldProjectedPhasefield2(C, lz, z) * (rho - rho_reg);
  }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return sin(std::numbers::pi * s / width) * exp(-1 / (width2 - s * s)) /
           exp(-1 / width2);
  }
};

class Elastic_PhaseFieldNeumann : public ElasticPhaseField {
  double shift;
public:
  const char *Name() const { return "Elastic_PhaseFieldNeumann"; }

  Elastic_PhaseFieldNeumann(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc), shift(2.5) {
    Config::Get("shift", shift);
  }

  bool RHS() const { return false; }

  bool Exact() const { return false; }

  double ut(double t, const Point &x, int i) const {
    Point z(x[0] - shift, x[1]);
    double Z = norm(z);
    if (Z < 1.e-6) return 0;
    Point zz = (1 / Z) * z;
    double y = A0(Z);
    switch(i) {
      case 3:
        return y * zz[0];
      case 4:
        return y * zz[1];
        // case 3: return ((lambda+3*mu)*(1+zz[0]*zz[0]) +(lambda+mu)*zz[0]*zz[1]) * y;
        // case 4: return ((lambda+3*mu)*(1+zz[1]*zz[1])+ (lambda+mu)*zz[0]*zz[2]) * y;
      default:
        return 0;
    }
  }
};

class Elastic_PhaseFieldPoint : public ElasticPhaseField {
  double shift;
public:
  const char *Name() const { return "Elastic_PhaseFieldPoint"; }

  Elastic_PhaseFieldPoint(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc), shift(2.5) {
    Config::Get("shift", shift);
  }

  double Phasefield(const Point &x) const {
    double y = abs(x[0]);
    if (x[1] >= 0) y = norm(x);
    if (y < l_c) return 0;
    return 1 - exp((l_c - y) / l_c);
  }

  double Rho(const cell &C, const Point &lz, const Point &z) const { return rho; }

  bool RHS() const { return false; }

  bool Exact() const { return false; }

  double ut(double t, const Point &x, int i) const {
    Point z(x[0] - shift, x[1]);
    double Z = norm(z);
    if (Z < 1.e-6) return 0;
    Point zz = (1 / Z) * z;
    double y = A0(Z);
    switch(i) {
      case 3:
        return y * zz[0];
      case 4:
        return y * zz[1];
        // case 3: return ((lambda+3*mu)*(1+zz[0]*zz[0]) +(lambda+mu)*zz[0]*zz[1]) * y;
        // case 4: return ((lambda+3*mu)*(1+zz[1]*zz[1])+ (lambda+mu)*zz[0]*zz[2]) * y;
      default:
        return 0;
    }
  }
};

class Elastic_PhaseFieldBar : public ElasticPhaseField {
  double shift;
  double b_r;
  double b_t;
  double s_min;
  bool exact;
public:
  const char *Name() const { return "Elastic_PhaseFieldBar"; }

  Elastic_PhaseFieldBar(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc), shift(2.5), s_min(0.1), exact(false) {
    Config::Get("shift", shift);
    Config::Get("s_min", s_min);
    Config::Get("exact", exact);
    b_r = ((2 * mu2 + lambda2) * c_P - (2 * mu + lambda) * c2_P)
      / ((2 * mu2 + lambda2) * c_P + (2 * mu + lambda) * c2_P);
    b_t = 2 * (2 * mu + lambda) * c_P
      / ((2 * mu2 + lambda2) * c_P + (2 * mu + lambda) * c2_P);
    mout << OUT(b_r) << OUT(b_t) << endl;
  }

  double Phasefield(const Point &x) const { return 1; }

  double Phasefield(const cell &C, const Point &lz, const Point &z) const {
    if (pf) {
      ScalarElement E(*pf, *C);
      return E.Value(lz, *pf);
    }
    return Phasefield(z);
  }

  /*
    double Phasefield (const cell& C, const Point& lz, const Point& z) const {
	if (pf) {
	    ScalarElement E(*pf_disc,*pf,C);
	    if (E.Value(lz,*pf) > s_min) return 1;
	    return 0;
	}
	return Phasefield(z);
    }
    double Phasefield_old (const cell& C, const Point& lz, const Point& z) const {
	if (pf_old) {
	    ScalarElement E(*pf_disc,*pf_old,C);
	    if (E.Value(lz,*pf_old) > s_min) return 1;
	    return 0;
	}
	return Phasefield(z);
    }
    */

  /*
    void ProjectStress (Vector& u) const {
	for (cell c=u.cells(); c!=u.cells_end(); ++c) {
	    row r = u.find_row(c());
	    DGElasticityElement Elem(disc,u,c);
	    Elem.NodalPoints(z);
	    Elem.LocalNodalPoints(lz);
	    for (int j=0; j<Elem.shape_size();++j) {
		double p = Phasefield(c,lz[j],z[j]);
		if (p > s_min) continue;
		pout << OUT(z[j]) << OUT(p) << endl;
	    }


	    //for (int i=0; i<Elem.u_dimension();++i)
	    //	for (int j=0; j<Elem.shape_size();++j) {
	    //	    u(r)[i*Elem.shape_size()+j] = ut(t,c,lz[j],z[j],i);
        }
    */

  double ProjectedPhasefield(const cell &C, const Point &lz, const Point &z) const {
    if (pf) {
      ScalarElement E(*pf, *C);
      //   pout << OUT(E.Value(lz,*pf)) << endl;
      if (E.Value(lz, *pf) > s_min) return 1;
      return 0;
    }
    return Phasefield(z);
  }

  double ProjectedPhasefield_old(const cell &C, const Point &lz, const Point &z) const {
    if (pf_old) {
      ScalarElement E(*pf_old, *C);
      if (E.Value(lz, *pf_old) > s_min) return 1;
      return 0;
    }
    return Phasefield(z);
  }

  /*
    double Lambda (const cell& C, const Point& lz, const Point& z) const {
	if (Phasefield(C,lz,z) > s_min) return lambda;
	return lambda2;
	return lambda2 + g(Phasefield(C,lz,z)) * (lambda - lambda2);
    }
    double Mu (const cell& C, const Point& lz, const Point& z) const {
	if (Phasefield(C,lz,z) > s_min) return mu;
	return mu2;
	return mu2 + g(Phasefield(C,lz,z)) * (mu - mu2);
    }
    */
  double Rho(const cell &C, const Point &lz, const Point &z) const {
    if (Phasefield(C, lz, z) > s_min) return rho;
    return rho2;
  }

  bool Exact() const { return exact; }

  double a0(double s) const {
    double s2 = s * s;
    if (s2 >= width2) return 0;
    return -width + abs(s);
    return (5 * s2 - 6 * width2) * s2 + width2 * width2;
  }

  double int_a0(double s) const {
    double s2 = s * s;
    if (s2 >= width2) return 0;
    return 0.5 * (width - abs(s)) * (width - abs(s));
    return s * ((s2 - 2 * width2) * s2 + width2 * width2);
  }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    if (x[0] < 0) {
      double y = A0((x[0] - shift) / c_P - t);
      double y_r = A0((x[0] + shift) / c_P + t) * b_r;
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y_r - y);
        case 1:
          return lambda * (y_r - y);
        case 3:
          return c_P * (y + y_r);
        default:
          return 0;
      }
    } else {
      double y_t = A0(x[0] / c2_P - shift / c_P - t) * b_t;
      switch(i) {
        case 0:
          return -(2 * mu2 + lambda2) * y_t;
        case 1:
          return -lambda2 * y_t;
        case 3:
          return c2_P * y_t;
        default:
          return 0;
      }
    }
  }

  double U0(double t, const Point &x, int i) const {
    if (i == 0) {
      double y = int_a0((x[0] - shift) / c_P - t);
      double y_r = int_a0((x[0] + shift) / c_P + t);
      return c_P * (y + y_r);
    }
    return 0;
  }
};

class Elastic_PhaseFieldNeumann_s : public ElasticPhaseField {
  double shift;
public:
  const char *Name() const { return "Elastic_PhaseFieldNeumann_s"; }

  Elastic_PhaseFieldNeumann_s(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc), shift(2.5) {
    Config::Get("shift", shift);
  }

  bool RHS() const { return false; }

  bool Exact() const { return true; }

  double A(double s) const { return A0(s - shift); }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    if (x[0] > 0) return 0;
    double c_S = C_s(C, lz, x);
    double y = A(x[0] - c_S * t);
    double y_r = A(-x[0] - c_S * t);
    switch(i) {
      case 2:
        return mu * (y_r - y);
      case 4:
        return c_S * (y + y_r);
      default:
        return 0;
    }
  }
};

class Elastic_PhaseFieldInterface_s : public ElasticPhaseField {
  double shift;
  double b_r;
  double b_t;
public:
  const char *Name() const { return "Elastic_PhaseFieldInterface_s"; }

  Elastic_PhaseFieldInterface_s(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc), shift(2.5) {
    Config::Get("shift", shift);
    b_r = (mu2 * c_S - mu * c2_S) / (mu2 * c_S + mu * c2_S);
    b_t = 2 * mu * c_S / (mu2 * c_S + mu * c2_S);
    mout << OUT(b_r) << OUT(b_t) << OUT(c2_S * b_t / c_S) << OUT(mu2 * b_t / mu) << endl;
  }

  bool RHS() const { return false; }

  bool Exact() const { return true; }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    if (x[0] < 0) {
      double y = A0((x[0] - shift) / c_S - t);
      double y_r = A0((x[0] + shift) / c_S + t) * b_r;
      switch(i) {
        case 2:
          return mu * (y_r - y);
        case 4:
          return c_S * (y + y_r);
        default:
          return 0;
      }
    } else {
      double y_t = A0(x[0] / c2_S - shift / c_S - t) * b_t;
      switch(i) {
        case 2:
          return -mu2 * y_t;
        case 4:
          return c2_S * y_t;
        default:
          return 0;
      }
    }
  }
};

class Elastic_PhaseFieldInterface_p : public ElasticPhaseField {
  double shift;
  double b_r;
  double b_t;
public:
  const char *Name() const { return "Elastic_PhaseFieldInterface_p"; }

  Elastic_PhaseFieldInterface_p(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc), shift(2.5) {
    Config::Get("shift", shift);
    b_r = ((2 * mu2 + lambda2) * c_P - (2 * mu + lambda) * c2_P)
      / ((2 * mu2 + lambda2) * c_P + (2 * mu + lambda) * c2_P);
    b_t = 2 * (2 * mu + lambda) * c_P
      / ((2 * mu2 + lambda2) * c_P + (2 * mu + lambda) * c2_P);
    mout << OUT(b_r) << OUT(b_t) << endl;
  }

  bool RHS() const { return false; }

  bool Exact() const { return true; }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    if (x[0] < 0) {
      double y = A0((x[0] - shift) / c_P - t);
      double y_r = A0((x[0] + shift) / c_P + t) * b_r;
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y_r - y);
        case 1:
          return lambda * (y_r - y);
        case 3:
          return c_P * (y + y_r);
        default:
          return 0;
      }
    } else {
      double y_t = A0(x[0] / c2_P - shift / c_P - t) * b_t;
      switch(i) {
        case 0:
          return -(2 * mu2 + lambda2) * y_t;
        case 1:
          return -lambda2 * y_t;
        case 3:
          return c2_P * y_t;
        default:
          return 0;
      }
    }
  }
};

class Elastic_PhaseFieldNeumann_p : public ElasticPhaseField {
  double shift;
  double ampl;
public:
  const char *Name() const { return "Elastic_PhaseFieldNeumann_p"; }

  Elastic_PhaseFieldNeumann_p(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 1), shift(2.5), ampl(1) {
    Config::Get("shift", shift);
    Config::Get("Amplidue", ampl);
  }

  bool RHS() const { return false; }

  bool Exact() const { return exact; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return (ampl / width2) * s * exp(-1 / (width2 - s * s)) / exp(-1 / width2);
  }

  double A(double s) const { return A0(s - shift); }

  virtual bool SmallStrain() const { return false; }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    //if (x[0]>0) return 0;
    double c_P = C_p(C, lz, x);
    double y = A(c_P * t + x[0]);
    double y_r = A(c_P * t - x[0]);
    if (dim == 2) {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y_r - y);
        case 1:
          return lambda * (y_r - y);
        case 3:
          return c_P * (y + y_r);
        default:
          return 0;
      }
    } else {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y - y_r);
        case 1:
          return c_P * (y + y_r);
          // case 0: return (2*mu+lambda) * (y+y_r);
          // case 1: return c_P * (y-y_r);
      }
    }
    return 0;
  }
};

class Elastic_PhaseFieldBnd : public ElasticPhaseField {
  double shift;
  double shift2;
  double t_max;
  double scaleAmplitude = 1;
  double scaleAngle = 0;
public:
  const char *Name() const { return "Elastic_PhaseFieldBnd"; }

  Elastic_PhaseFieldBnd(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 1), shift(2.5), shift2(2.5), t_max(0.75) {
    Config::Get("shift", shift);
    Config::Get("shift2", shift2);
    Config::Get("t_max", t_max);
    Config::Get("scaleAmplitude", scaleAmplitude);
    Config::Get("scaleAngle", scaleAngle);
  }

  bool RHS() const { return true; }

  bool Exact() const { return exact; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return (1 / width2) * s * exp(-1 / (width2 - s * s)) / exp(-1 / width2);
  }

  double A(double s) const { return A0(s - shift); }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    if (t > t_max) return 0;
    //if (x[0]>0) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -(1 + scaleAngle * x[1]) * A(c_P * t + x[0]);
    double y_r = (1 + scaleAngle * x[1]) * scaleAmplitude * A(c_P * t - x[0] - shift2);
    double y2 = scaleAngle * x[1] * A(c_P * t + x[0]);
    double y2_r = -scaleAngle * x[1] * scaleAmplitude * A(c_P * t - x[0] - shift2);
    if (dim == 2) {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y_r - y);
        case 1:
          return lambda * (y_r - y);
        case 3:
          return c_P * (y + y_r);
        case 4:
          return c_P * (y2 + y2_r);
        default:
          return 0;
      }
    } else {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y - y_r);
        case 1:
          return c_P * (y + y_r);
        default:
          return 0;
          // case 0: return (2*mu+lambda) * (y+y_r);
          // case 1: return c_P * (y-y_r);
      }
    }
  }

  double ut0(double t, const Point &x, int i) const {
    if (t > t_max) return 0;
    //if (x[0]>0) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -(1 + scaleAngle * x[1]) * A(c_P * t + x[0]);
    double y_r = (1 + scaleAngle * x[1]) * scaleAmplitude * A(c_P * t - x[0] - shift2);
    double y2 = scaleAngle * x[1] * A(c_P * t + x[0]);
    double y2_r = -scaleAngle * x[1] * scaleAmplitude * A(c_P * t - x[0] - shift2);
    if (dim == 2) {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y_r - y);
        case 1:
          return lambda * (y_r - y);
        case 3:
          return c_P * (y + y_r);
        case 4:
          return c_P * (y2 + y2_r);
        default:
          return 0;
      }
    } else {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y - y_r);
        case 1:
          return c_P * (y + y_r);
        default:
          return 0;
          // case 0: return (2*mu+lambda) * (y+y_r);
          // case 1: return c_P * (y-y_r);
      }
    }
  }
};

class Elastic_PhaseFieldArc : public ElasticPhaseField {
  double shift;
  double shift2;
  double t_max;
  double scaleAmplitude = 1;
  double scaleProfile = 0;
  double scaleAngle = 0;
  double angle = 0;
public:
  const char *Name() const { return "Elastic_PhaseFieldArc"; }

  Elastic_PhaseFieldArc(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 1), shift(2.5), shift2(2.5), t_max(0.75) {
    Config::Get("shift", shift);
    Config::Get("shift2", shift2);
    Config::Get("t_max", t_max);
    Config::Get("scaleAmplitude", scaleAmplitude);
    Config::Get("scaleProfile", scaleProfile);
    Config::Get("scaleAngle", scaleAngle);
    Config::Get("angle", angle);
    dim = 2;
  }

  bool RHS() const { return true; }

  bool Exact() const { return exact; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return (1 / width2) * s * exp(-1 / (width2 - s * s)) / exp(-1 / width2);
  }

  double A(double s) const { return A0(s - shift); }

  double ut0(double t, const Point &x, int i) const {
    if (t > t_max) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -A(c_P * t + x[0]);
    double y_r = scaleAmplitude * A(c_P * t - x[0] - shift2);

    //if (y > 0.00001)
    //pout << OUT(x[0]) << OUT(c_P*t+x[0]) << OUT(y) << endl;
    //if (y_r > 0.00001)
    //pout << OUT(x[0]) << OUT(c_P*t-x[0]-shift2) << OUT(y_r) << endl;


    if (dim == 2) {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y_r + y);
        case 1:
          return lambda * (y_r + y);
        case 3:
          return cos(scaleProfile * x[1]) * c_P * (y_r - y);
        default:
          return 0;
      }
    } else {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y - y_r);
        case 1:
          return c_P * (y + y_r);
        default:
          return 0;
      }
    }
  }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    if (t > t_max) return 0;
    //if (x[0]>0) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -(1 + scaleAngle * x[1]) * A(c_P * t + x[0]);
    double y_r = (1 + scaleAngle * x[1]) * scaleAmplitude * A(c_P * t - x[0] - shift2);
    //if (abs(y) + abs(y_r) > 0.1)
    //    pout << OUT(x[0]) << OUT(1-x[0])
    //	 << OUT(y) << OUT(y_r) << c_P*t+x[0] << " " << c_P*t-x[0]-shift2 << endl;
    double y2 = scaleAngle * x[1] * A(c_P * t + x[0]);
    double y2_r = -scaleAngle * x[1] * scaleAmplitude * A(c_P * t - x[0] - shift2);

    y = A(c_P * t + x[0]);
    y_r = A(c_P * t - x[0] - shift2);
    y2 = 0;
    y2_r = 0;

    //if (abs(y) > 0.01) pout << OUT(x[0]) << OUT(1-x[0]) << OUT(y) << OUT(x[0]) << endl;
    //if (abs(y_r) > 0.01) pout << OUT(x[0]) << OUT(-x[0]) << OUT(y_r) << OUT(-x[0]-shift2) << endl;

    // return Point(z[0]+z1*sin(angle*(z[0]-0.5)),z1+cos(angle*(z[0]-0.5)) - 1);
    // x[0] = return Point(z[0]+z1*sin(angle*(z[0]-0.5)),z1+cos(angle*(z[0]-0.5)) - 1);
    // return Point(z[0]+z1*sin(angle*(z[0]-0.5)),z1+cos(angle*(z[0]-0.5)) - 1);


    if (dim == 2) {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y_r - y);
        case 1:
          return lambda * (y_r - y);
        case 3:
          return c_P * (y + y_r);
        case 4:
          return c_P * (y2 + y2_r);
        default:
          return 0;
      }
    } else {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y - y_r);
        case 1:
          return c_P * (y + y_r);
        default:
          return 0;
          // case 0: return (2*mu+lambda) * (y+y_r);
          // case 1: return c_P * (y-y_r);
      }
    }
  }
};

class Elastic_PhaseFieldArc2 : public ElasticPhaseField {
  double shift;
  double shift2;
  double shift3 = 0;
  double t_max;
  double t_repeat = 0;
  double scaleAmplitude = 1;
  double scaleProfile = 0;
  double scaleAngle = 0;
  double angle = 0;
  double sign = 1;
public:
  const char *Name() const { return "Elastic_PhaseFieldArc2"; }

  Elastic_PhaseFieldArc2(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 1), shift(2.5), shift2(2.53), t_max(0.75) {
    Config::Get("shift", shift);
    Config::Get("shift2", shift2);
    Config::Get("shift3", shift3);
    Config::Get("t_max", t_max);
    Config::Get("t_repeat", t_repeat);
    Config::Get("scaleAmplitude", scaleAmplitude);
    Config::Get("scaleProfile", scaleProfile);
    Config::Get("scaleAngle", scaleAngle);
    Config::Get("angle", angle);
    Config::Get("sign", sign);
  }

  double Repeat() const { return t_repeat; }

  bool RHS() const { return true; }

  bool Exact() const { return exact; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return (1 / width2) * exp(-1 / (width2 - s * s)) / exp(-1 / width2);
  }

  double A(double s) const { return A0(s - shift); }

  double _ut0(double t, const Point &x, int i) const {
    if (i != 3) return 0;
    if (t > t_max) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -A(c_P * t + x[0] + shift3);
    double y_r = scaleAmplitude * A(c_P * t - x[0] - shift2);
    return sign * cos(scaleProfile * x[1]) * c_P * (y - y_r);
  }

  double ut0(double t, const Point &x, int i) const {
    if (dim == 3) {
      if (t < t_repeat) {
        if (t > t_max) return 0;
        double c_P = sqrt((2 * mu + lambda) / rho);
        double y = -A(c_P * t + x[0] + shift3);
        double y_r = scaleAmplitude * A(c_P * t - x[0] - shift2);
        return sign * cos(scaleProfile * x[1]) * cos(scaleProfile * x[2]) * c_P
          * (y - y_r);
      }
      double t1 = t - t_repeat;
      if (t1 > t_max) return 0;
      double c_P = sqrt((2 * mu + lambda) / rho);
      double y = -A(c_P * t1 + x[0] + shift3);
      double y_r = scaleAmplitude * A(c_P * t1 - x[0] - shift2);
      return sign * cos(scaleProfile * x[1]) * cos(scaleProfile * x[2]) * c_P * (y - y_r);
    }
    /*
	if (dim == 3) {
	    if (t > t_max) return 0;
	    double c_P = sqrt((2*mu+lambda)/rho);
	    double y = - A(c_P*t+x[0]);
	    double y_r = scaleAmplitude * A(c_P*t-x[0]-shift2);
	    return sign * cos(scaleProfile*x[1])*c_P * (y-y_r);

	    switch (i) {
	    case 0: return (2*mu+lambda) * (y_r-y);
	    case 1: return lambda * (y_r-y);
	    case 3: return c_P * (y+y_r);
	    default: return 0;
	    }
	}
	else if (dim == 2) {
	    if (t > t_max) return 0;
	    double c_P = sqrt((2*mu+lambda)/rho);
	    double y = - A(c_P*t+x[0]);
	    double y_r = scaleAmplitude * A(c_P*t-x[0]-shift2);
	    return sign * cos(scaleProfile*x[1])*c_P * (y-y_r);

	    switch (i) {
	    case 0: return (2*mu+lambda) * (y_r-y);
	    case 1: return lambda * (y_r-y);
	    case 3: return c_P * (y+y_r);
	    default: return 0;
	    }
	}
	if (i!=3) return 0;
	*/
    if (t < t_repeat) {
      if (t > t_max) return 0;
      double c_P = sqrt((2 * mu + lambda) / rho);
      double y = -A(c_P * t + x[0] + shift3);
      double y_r = scaleAmplitude * A(c_P * t - x[0] - shift2);
      return sign * cos(scaleProfile * x[1]) * c_P * (y - y_r);
    }
    double t1 = t - t_repeat;
    if (t1 > t_max) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -A(c_P * t1 + x[0] + shift3);
    double y_r = scaleAmplitude * A(c_P * t1 - x[0] - shift2);
    return sign * cos(scaleProfile * x[1]) * c_P * (y - y_r);
  }
};

class Elastic_PhaseFieldArc_3d : public ElasticPhaseField {
  double shift;
  double shift2;
  double t_max;
  double t_repeat = 0;
  double scaleAmplitude = 1;
  double scaleProfile = 0;
  double scaleAngle = 0;
  double angle = 0;
  double sign = 1;
  double load_E = 1;
  double c_E = 1;
public:
  const char *Name() const { return "Elastic_PhaseFieldArc_3d"; }

  Elastic_PhaseFieldArc_3d(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 1), shift(2.5), shift2(2.53), t_max(0.75) {
    Config::Get("shift", shift);
    Config::Get("shift2", shift2);
    Config::Get("t_max", t_max);
    Config::Get("t_repeat", t_repeat);
    Config::Get("scaleAmplitude", scaleAmplitude);
    Config::Get("scaleProfile", scaleProfile);
    Config::Get("scaleAngle", scaleAngle);
    Config::Get("angle", angle);
    Config::Get("sign", sign);
    load_E = mu * (3 * lambda + 2 * mu) / (lambda + mu);
    Config::Get("load_E", sign);
    c_E = sqrt(load_E / rho);
    dim = 3;
  }

  double Repeat() const { return t_repeat; }

  bool RHS() const { return true; }

  bool Exact() const { return exact; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return (1 / width2) * exp(-1 / (width2 - s * s)) / exp(-1 / width2);
    // return cos(s) * exp(-1/(width2-s*s)) / exp(-1/width2);
    // return sin(s) * exp(-1/(width2-s*s)) / exp(-1/width2);
  }

  double A(double s) const { return A0(s - shift); }

  double ut0(double t, const Point &x, int i) const {
    if (dim == 3) {
      if (t < t_repeat) {
        if (t > t_max) return 0;
        double y = -A(c_E * t + x[0]);
        double y_r = scaleAmplitude * A(c_E * t - x[0] - shift2);
        //return sign * d * c_P * (y-y_r);
        return sign * cos(scaleProfile * x[1]) * cos(scaleProfile * x[2]) * c_E
          * (y - y_r);
      }
      double t1 = t - t_repeat;
      if (t1 > t_max) return 0;
      double y = -A(c_P * t1 + x[0]);
      double y_r = scaleAmplitude * A(c_E * t1 - x[0] - shift2);
      return sign * cos(scaleProfile * x[1]) * cos(scaleProfile * x[2]) * c_E * (y - y_r);
      //return sign * d * c_P * (y-y_r);
    }
    /*
	if (dim == 3) {
	    if (t > t_max) return 0;
	    double c_P = sqrt((2*mu+lambda)/rho);
	    double y = - A(c_P*t+x[0]);
	    double y_r = scaleAmplitude * A(c_P*t-x[0]-shift2);
	    return sign * cos(scaleProfile*x[1])*c_P * (y-y_r);

	    switch (i) {
	    case 0: return (2*mu+lambda) * (y_r-y);
	    case 1: return lambda * (y_r-y);
	    case 3: return c_P * (y+y_r);
	    default: return 0;
	    }
	}
	else if (dim == 2) {
	    if (t > t_max) return 0;
	    double c_P = sqrt((2*mu+lambda)/rho);
	    double y = - A(c_P*t+x[0]);
	    double y_r = scaleAmplitude * A(c_P*t-x[0]-shift2);
	    return sign * cos(scaleProfile*x[1])*c_P * (y-y_r);

	    switch (i) {
	    case 0: return (2*mu+lambda) * (y_r-y);
	    case 1: return lambda * (y_r-y);
	    case 3: return c_P * (y+y_r);
	    default: return 0;
	    }
	}
	if (i!=3) return 0;
	*/
    if (t < t_repeat) {
      if (t > t_max) return 0;
      double c_P = sqrt((2 * mu + lambda) / rho);
      double y = -A(c_P * t + x[0]);
      double y_r = scaleAmplitude * A(c_P * t - x[0] - shift2);
      return sign * cos(scaleProfile * x[1]) * c_P * (y - y_r);
    }
    double t1 = t - t_repeat;
    if (t1 > t_max) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -A(c_P * t1 + x[0]);
    double y_r = scaleAmplitude * A(c_P * t1 - x[0] - shift2);
    return sign * cos(scaleProfile * x[1]) * c_P * (y - y_r);
  }
};

class Elastic_PhaseFieldArc_3d_test : public ElasticPhaseField {
  double shift;
  double shift2;
  double t_max;
  double t_repeat = 0;
  double scaleAmplitude = 1;
  double scaleProfile = 0;
  double scaleAngle = 0;
  double angle = 0;
  double sign = 1;
public:
  const char *Name() const { return "Elastic_PhaseFieldArc_3d_test"; }

  Elastic_PhaseFieldArc_3d_test(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 1), shift(2.5), shift2(2.53), t_max(0.75) {
    Config::Get("shift", shift);
    Config::Get("shift2", shift2);
    Config::Get("t_max", t_max);
    Config::Get("t_repeat", t_repeat);
    Config::Get("scaleAmplitude", scaleAmplitude);
    Config::Get("scaleProfile", scaleProfile);
    Config::Get("scaleAngle", scaleAngle);
    Config::Get("angle", angle);
    Config::Get("sign", sign);
    dim = 3;
  }

  double Repeat() const { return t_repeat; }

  bool RHS() const { return true; }

  bool Exact() const { return exact; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return sin(s) * exp(-1 / (width2 - s * s)) / exp(-1 / width2);
  }

  double A(double s) const { return A0(s - shift); }

  double _ut0(double t, const Point &x, int i) const {
    if (i != 3) return 0;
    if (t > t_max) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -A(c_P * t + x[0]);
    double y_r = scaleAmplitude * A(c_P * t - x[0] - shift2);
    return sign * cos(scaleProfile * x[1]) * c_P * (y - y_r);
  }

  double ut0(double t, const Point &x, int i) const {
    if (dim == 3) {
      if (t > t_max) return 0;
      double c_P = sqrt((2 * mu + lambda) / rho);
      double y = -1;
      double y_r = 0;
      return sign * c_P * (y - y_r);
    }
    /*
	if (dim == 3) {
	    if (t > t_max) return 0;
	    double c_P = sqrt((2*mu+lambda)/rho);
	    double y = - A(c_P*t+x[0]);
	    double y_r = scaleAmplitude * A(c_P*t-x[0]-shift2);
	    return sign * cos(scaleProfile*x[1])*c_P * (y-y_r);

	    switch (i) {
	    case 0: return (2*mu+lambda) * (y_r-y);
	    case 1: return lambda * (y_r-y);
	    case 3: return c_P * (y+y_r);
	    default: return 0;
	    }
	}
	else if (dim == 2) {
	    if (t > t_max) return 0;
	    double c_P = sqrt((2*mu+lambda)/rho);
	    double y = - A(c_P*t+x[0]);
	    double y_r = scaleAmplitude * A(c_P*t-x[0]-shift2);
	    return sign * cos(scaleProfile*x[1])*c_P * (y-y_r);

	    switch (i) {
	    case 0: return (2*mu+lambda) * (y_r-y);
	    case 1: return lambda * (y_r-y);
	    case 3: return c_P * (y+y_r);
	    default: return 0;
	    }
	}
	if (i!=3) return 0;
	*/
    if (t < t_repeat) {
      if (t > t_max) return 0;
      double c_P = sqrt((2 * mu + lambda) / rho);
      double y = -A(c_P * t + x[0]);
      double y_r = scaleAmplitude * A(c_P * t - x[0] - shift2);
      return sign * cos(scaleProfile * x[1]) * c_P * (y - y_r);
    }
    double t1 = t - t_repeat;
    if (t1 > t_max) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -A(c_P * t1 + x[0]);
    double y_r = scaleAmplitude * A(c_P * t1 - x[0] - shift2);
    return sign * cos(scaleProfile * x[1]) * c_P * (y - y_r);
  }
};

class Elastic_PhaseFieldTest_1d : public ElasticPhaseField {
  double shift;
  double shift2;
  double t_max;
  double scaleAmplitude = 1;
  double scaleProfile = 0;
  double scaleAngle = 0;
  double angle = 0;
public:
  const char *Name() const { return "Elastic_PhaseFieldTest_1d"; }

  Elastic_PhaseFieldTest_1d(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 1), shift(2.5), shift2(2.5), t_max(0.75) {
    Config::Get("shift", shift);
    Config::Get("shift2", shift2);
    Config::Get("t_max", t_max);
    Config::Get("scaleAmplitude", scaleAmplitude);
    Config::Get("scaleProfile", scaleProfile);
    Config::Get("scaleAngle", scaleAngle);
    Config::Get("angle", angle);
    dim = 2;
  }

  /*
    double Lambda (const cell& C, const Point& lz, const Point& x) const {
	return 0.0;
    }
    double Mu (const cell& C, const Point& lz, const Point& x) const {
	double z = abs(x[0] - 0.5) / 0.01;
	if (z > 1) return 2;
	return 2*(1-z)Mu(x);
    }
    double Rho (const cell& C, const Point& lz, const Point& x) const {
	return 1;
    }
    */
  double Lambda(const Point &x) const {
    Exit("do no use");
    return lambda;
  }

  double Mu(const Point &x) const {
    Exit("do no use");
    return mu;
  }

  double Rho(const Point &x) const {
    Exit("do no use");
    return rho;
  }

  double C_s(const Point &x) const {
    Exit("do no use");
    return sqrt(Mu(x) / Rho(x));
  }

  double C_p(const Point &x) const {
    Exit("do no use");
    return sqrt((2 * Mu(x) + Lambda(x)) / Rho(x));
  }

  bool RHS() const { return true; }

  bool Exact() const { return exact; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return (1 / width2) * exp(-1 / (width2 - s * s)) / exp(-1 / width2);
  }

  double A(double s) const { return A0(s - shift); }

  double ut0(double t, const Point &x, int i) const {
    if (i != 3) return 0;
    if (t < 0.4) {
      if (t > t_max) return 0;
      double c_P = sqrt((2 * mu + lambda) / rho);
      double y = -A(c_P * t + x[0]);
      double y_r = scaleAmplitude * A(c_P * t - x[0] - shift2);
      return cos(scaleProfile * x[1]) * c_P * (y - y_r);
    }
    double t1 = t - 0.4;
    if (t1 > t_max) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -A(c_P * t1 + x[0]);
    double y_r = scaleAmplitude * A(c_P * t1 - x[0] - shift2);
    return cos(scaleProfile * x[1]) * c_P * (y - y_r);
  }
};

class Elastic_PhaseFieldBnd_nl : public ElasticPhaseField {
  double shift;
  double shift2;
  double t_max;
  double scaleAmplitude = 1;
  double scaleAngle = 0;
  double scaleNL = 0.1;
public:
  const char *Name() const { return "Elastic_PhaseFieldBnd_nl"; }

  Elastic_PhaseFieldBnd_nl(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 1), shift(2.5), shift2(2.5), t_max(0.75) {
    Config::Get("shift", shift);
    Config::Get("shift2", shift2);
    Config::Get("t_max", t_max);
    Config::Get("scaleAmplitude", scaleAmplitude);
    Config::Get("scaleAngle", scaleAngle);
    Config::Get("scaleNL", scaleNL);
  }

  Tensor StressStrain(const cell &C,
                      const Point &lz, const Point &z, const Tensor &S) const {
    ScalarElement E(*U, *C);
    double Ux = E.Value(lz, *U);
    VectorField DUx = scaleNL * E.Derivative(lz, *U);
    //double Fxx = 1 + DUx[0];
    //if (abs(Ux) > 0.0001) pout << OUT(C()) << OUT(Ux) << OUT(Fxx) << endl;
    double mu = Mu(C, lz, z);
    mu *= exp(mu * DUx[0]);
    double lambda = Lambda(C, lz, z);
    lambda *= exp(lambda * DUx[0]);
    double s = 1 / (2.0 * mu);
    double ss = lambda / (2.0 * mu * (2.0 * lambda + 2.0 * mu));
    return s * S - ss * trace(S) * One;
  }

  bool RHS() const { return true; }

  bool Exact() const { return exact; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return (1 / width2) * s * exp(-1 / (width2 - s * s)) / exp(-1 / width2);
  }

  double A(double s) const { return A0(s - shift); }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    if (t > t_max) return 0;
    //if (x[0]>0) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -(1 + scaleAngle * x[1]) * A(c_P * t + x[0]);
    double y_r = (1 + scaleAngle * x[1]) * scaleAmplitude * A(c_P * t - x[0] - shift2);
    double y2 = scaleAngle * x[1] * A(c_P * t + x[0]);
    double y2_r = -scaleAngle * x[1] * scaleAmplitude * A(c_P * t - x[0] - shift2);
    if (dim == 2) {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y_r - y);
        case 1:
          return lambda * (y_r - y);
        case 3:
          return c_P * (y + y_r);
        case 4:
          return c_P * (y2 + y2_r);
        default:
          return 0;
      }
    } else {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y - y_r);
        case 1:
          return c_P * (y + y_r);
        default:
          return 0;
          // case 0: return (2*mu+lambda) * (y+y_r);
          // case 1: return c_P * (y-y_r);
      }
    }
  }
};

class Elastic_PhaseFieldBnd_2d : public ElasticPhaseField {
  double shift;
  double shift2;
  double t_max;
  double scaleAmplitude = 1;
  double scaleAngle = 0;
public:
  const char *Name() const { return "Elastic_PhaseFieldBnd"; }

  Elastic_PhaseFieldBnd_2d(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 2), shift(2.5), shift2(2.5), t_max(0.75) {
    Config::Get("shift", shift);
    Config::Get("shift2", shift2);
    Config::Get("t_max", t_max);
    Config::Get("scaleAmplitude", scaleAmplitude);
    Config::Get("scaleAngle", scaleAngle);
  }

  bool RHS() const { return true; }

  bool Exact() const { return exact; }

  double A0(double s) const {
    if (s * s >= width2) return 0;
    return (1 / width2) * s * exp(-1 / (width2 - s * s)) / exp(-1 / width2);
  }

  double A(double s) const { return -A0(s - shift); }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    if (t > t_max) return 0;
    //if (x[0]>0) return 0;
    double c_P = sqrt((2 * mu + lambda) / rho);
    double y = -(1 + scaleAngle * x[1]) * A(c_P * t + x[0]);
    double y_r = (1 + scaleAngle * x[1]) * scaleAmplitude * A(c_P * t - x[0] - shift2);
    double y2 = scaleAngle * x[1] * A(c_P * t + x[0]);
    double y2_r = -scaleAngle * x[1] * scaleAmplitude * A(c_P * t - x[0] - shift2);
    if (dim == 2) {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y_r - y);
        case 1:
          return (2 * mu + lambda) * (y2_r - y2);
        case 2:
          return lambda * (y_r - y + y2 + y2_r);
        case 3:
          return c_P * (y + y_r);
        case 4:
          return c_P * (y2 + y2_r);
        default:
          return 0;
      }
    } else {
      switch(i) {
        case 0:
          return (2 * mu + lambda) * (y - y_r);
        case 1:
          return c_P * (y + y_r);
        default:
          return 0;
          // case 0: return (2*mu+lambda) * (y+y_r);
          // case 1: return c_P * (y-y_r);
      }
    }
  }
};

class Elastic_PhaseFieldDirichlet : public ElasticPhaseField {
  double velocity;
  double t_init;
public:
  const char *Name() const { return "Elastic_PhaseFieldNeumann_p"; }

  Elastic_PhaseFieldDirichlet(std::shared_ptr<const DGDiscretization> _disc)
    : ElasticPhaseField(_disc, 1), velocity(1), t_init(0.01) {
    Config::Get("velocity", velocity);
    Config::Get("t_init", velocity);
  }

  bool RHS() const { return true; }

  bool Exact() const { return false; }

  double ut(double t, const cell &C, const Point &lz, const Point &x, int i) const {
    if (i != 1) return 0;
    double s = velocity;
    if (t < t_init) s *= t / t_init;
    if (x[0] == 0) return s;
    if (x[0] == 1) return -s;
    return 0.0;
  }
};

class Elastic_PlaneWave : public ProblemElasticity {
  double A_osc;
  double k_osc;
  double width;
  double shift;
  double lambda2;
  double mu2;
  double jump;
  double delta;
  double omega;
  double length;
public:
  Elastic_PlaneWave(std::shared_ptr<const DGDiscretization> _disc)
    : ProblemElasticity(_disc, 2),
      A_osc(0.5), k_osc(10), omega(1),
      width(0.125), shift(2.5), lambda2(lambda),
      mu2(mu), jump(2), delta(0.5), length(0.125) {
    Config::Get("k_osc", k_osc);
    Config::Get("A_osc", A_osc);
    Config::Get("width", width);
    Config::Get("shift", shift);
    Config::Get("jump", jump);
    Config::Get("lambda2", lambda2);
    Config::Get("mu2", mu2);
    Config::Get("delta", delta);
    Config::Get("length", length);
    Config::Get("omega", omega);
  }

  const char *Name() const { return "Elastic_PlaneWave"; }

  double phi(const Point &x) const {
    //double y0 = x[0]-jump+x[1]*x[1]/11;
    double y0 = dist(x, Point(2.0, 2.0)) - 1;
    if (y0 < -delta) return 0;
    if (y0 > +delta) return 1;
    double y = 0.5 + 0.5 * y0 / delta;
    return (3 - 2 * y) * y * y;
  }

  VectorField Dphi(const Point &x) const {
    //double y0 = x[0]-jump+x[1]*x[1]/11;
    double y0 = dist(x, Point(2.0, 2.0)) - 1;
    if (y0 < -delta) return zero;
    if (y0 > +delta) return zero;
    return (1 / y0) * (x - Point(2.0, 2.0));
    double y = 0.5 + 0.5 * y0 / delta;
    return (6 - 6 * y) * y * VectorField(1.0, 2 * x[1] / 11);
  }

  double Lambda(const Point &x) const {
    double p = phi(x);
    return lambda2 * p;

    return lambda2 * p + lambda * (1 - p);

    if (x[0] < jump) return lambda;
    else return lambda2;
  }

  double Mu(const Point &x) const {
    double p = phi(x);
    return mu2 * p;
    return mu2 * p + mu * (1 - p);

    if (x[0] < jump) return mu;
    else return mu2;
  }

  bool RHS() const { return true; }

  bool Exact() const { return true; }

  double A0(double s) const {
    if (s * s >= width) return 0;
    return exp(-1 / (width - s * s)) / exp(-1 / width);
  }

  double A(double s) const { return A0(s - shift); }

  double ut(double t, const Point &x, int i) const {
    double c_S = sqrt(mu2 / rho);
    //if (x[1] < 0.25) return 0;
    //if (x[1] > 0.75) return 0;
    double y = A(x[0] - c_S * t);
    switch(i) {
      case 2:
        return -mu * y;
      case 4:
        return c_S * y;
      default:
        return 0;
    }
  }
};

class Hat_2D : public ProblemElasticity {
  double mu;
  double lambda;
  double rho;
public:
  Hat_2D(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {}

  const char *Name() const { return "Hat_2D"; }

  double ut(double t, const Point &x, int i) const {
    Point Pmid = Point(0.5, 1.0);
    //Point Pmid = Point(1.0,0.5);
    //Point Pmid = Point(0.5,0.5);
    double rr;
    switch(i) {
      case 4:
        rr = pow(0.5, 3) - norm(Pmid - x);
        if (norm(Pmid - x) < 0.05)
          return rr * 4;
        else
          return 0.0;
    }
    return 0;
  }
};

class Bar_1D : public ProblemElasticity {
  double shift = 0.5;
  double width = 0.1;
public:
  Bar_1D(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {
    Config::Get("width", width);
    Config::Get("shift", shift);
  }

  const char *Name() const { return "Bar_1D"; }

  double A0(double s) const {
    if (s * s >= width) return 0;
    return exp(-1 / (width - s * s)) / exp(-1 / width);
  }

  double A(double s) const { return A0(s - shift); }

  double ut(double t, const Point &x, int i) const {
    double y = A(x[0] - c_P * t);
    switch(i) {
      case 0:
        return (2.0 * mu + lambda) * y;
      case 1:
        return c_P * y;
      default:
        return 0;
    }
  }
};

class Linear : public ProblemElasticity {
public:
  Linear(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {}

  const char *Name() const { return "Linear"; }

  bool RHS() const { return true; }

  bool Exact() const { return true; }

  double ut(double t, const Point &x, int i) const {
    if (i == 0) return 2 * x[0];
    if (i == 3) return 3 * x[0];
    if (i == 4) return x[1];
    return 0.0;
  }

  double utb(double t, const Point &z, int i) const {
    if (i == 0) return -3;
    if (i == 1) return -1;
    if (i == 3) return -2;
    return 0.0;
  }
};

class Spherical_Wave_2D : public ProblemElasticity {
  double aa;
  double rad;
  Point Pmid;
  int linear_weight;
public:
  Spherical_Wave_2D(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {
    aa = 1.0;//0.25;
    rad = 2.;
    Config::Get("ProblemMid", Pmid);
    Config::Get("linear_weight", linear_weight);
  }

  const char *Name() const { return "Spherical_Wave_2D"; }

  virtual double ut(double t, const Point &x, int i) const {
    using std::numbers::pi;
    Point Pmid = Point(2, 2);
    double r = dist(Pmid, x);
    switch (i) {
      case 4:
        if (r < 1.0 / 2.0) {
          return aa * pow(cos(2.0 * pi * r - 2.0 * pi * t) + 1.0, 2.0);
        } else {
          return 0.0;
        }
      default:
        return 0.0;
    }
  }

  virtual double ut_dual(double &t,
                         const Point &x,
                         int i,
                         const Point &a,
                         const Point &b) const {
    if (x[0] < a[0] || x[1] < a[1] || x[0] > b[0] || x[1] > b[1]) return 0.0;
    switch(linear_weight) {
      case 1: {
        Point ba = b - a;
        if (i <= 2) return 0; //S
        else return 1.0 / (ba[0] * ba[1]); //V
      }
      default: Exit("Not implemented");
    }
  }
};

class Div_free_test : public ProblemElasticity {
  double aa;
  double rad;
  Point Pmid;
  int linear_weight;
  double omega;
public:
  Div_free_test(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {
    aa = 1.0;//0.25;
    rad = 2.;
    Config::Get("ProblemMid", Pmid);
    Config::Get("linear_weight", linear_weight);
    omega = M_PI;
  }

  const char *Name() const { return "Div_free_test"; }

  virtual double ut(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
    Point Pmid = Point(2.0, 2.0);
    if (x * x + y * y >= 1) return 0.0;
    switch(i) {
      case 0 :
        return -1.0 / 2.0 * exp(1.0 / (x * x + y * y - 1.0))
          * ((2.0 * y * y) / pow(x * x + y * y - 1.0, 2) - 1);
      case 1 :
        return -1.0 / 2.0 * exp(1.0 / (x * x + y * y - 1.0))
          * ((2.0 * x * x) / pow(x * x + y * y - 1.0, 2) - 1);
      case 2 :
        return x * y / pow(-x * x - y * y + 1.0, 2) * exp(-1.0 / (1.0 - x * x - y * y));
      case 3 :
        return 0;
      case 4 :
        return 0;
      default :
        return 0.0;
    }
  }

  virtual double ut_dual(double &t,
                         const Point &x,
                         int i,
                         const Point &a,
                         const Point &b) const {
    if (x[0] < a[0] || x[1] < a[1] || x[0] > b[0] || x[1] > b[1]) return 0.0;
    switch(linear_weight) {
      case 1: {
        Point ba = b - a;
        if (i <= 2) return 0; //S
        else return 1.0 / (ba[0] * ba[1]); //V
      }
      default: Exit("Not implemented");
    }
  }
};

class Plane_Wave_2D_Test : public ProblemElasticity {
  double mu;
  double lambda;
  double rho;
public:
  Plane_Wave_2D_Test(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {
    mu = 1.0;
    lambda = 2.0;
    rho = 1.0;
    Config::Get("lambda", lambda);
    Config::Get("mu", mu);
    Config::Get("rho", rho);
  }

  const char *Name() const { return "Plane_Wave_2D_Test"; }

  virtual double ut(double t, const Point &x, int i) const {
    switch(i) {
      case 0:
        return cos(c_P * t - x[0]) * (2.0 * mu + lambda);
      case 1:
        return cos(c_P * t - x[0]) * lambda;
      case 2:
        return cos(c_S * t - x[0]) * mu;
      case 3:
        return cos(c_P * t - x[0]) * c_P;
      case 4:
        return cos(c_S * t - x[0]) * c_S;
      default:
        return 0.0;
    }
  }
};

class Plane_Wave_2D : public ProblemElasticity {
  double mu;
  double lambda;
  double rho;
public:
  Plane_Wave_2D(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {
    mu = 1.0;
    lambda = 2.0;
    rho = 1.0;
    Config::Get("lambda", lambda);
    Config::Get("mu", mu);
    Config::Get("rho", rho);
  }

  const char *Name() const { return "Plane_Wave_2D"; }

  virtual double ut(double t, const Point &x, int i) const {
    switch(i) {
      case 0:
        return (c_P * t - x[0]) * (2.0 * mu + lambda);
      case 1:
        return (c_P * t - x[0]) * lambda;
      case 2:
        return (c_S * t - x[0]) * mu;
      case 3:
        return (c_P * t - x[0]) * c_P;
      case 4:
        return (c_S * t - x[0]) * c_S;
      default:
        return 0.0;
    }
  }

  virtual double u0(const Point &x, int i) const {
    return ut(0, x, i);
  }
};

class Plane_Wave_2D_2 : public ProblemElasticity {
  double aa;
  double bb;
public:
  Plane_Wave_2D_2(std::shared_ptr<const DGDiscretization> _disc)
    : ProblemElasticity(_disc, 2) {
    aa = 2.0;
    bb = 2.0;
    Config::Get("aa", aa);
    Config::Get("bb", bb);
  }

  const char *Name() const { return "Plane_Wave_2D_2"; }

  double ut(double t, const Point &x, int i) const {
    using std::numbers::pi;
    Point E0 = Point(bb, -aa);
    double w = sqrt(aa * aa + bb * bb);
    E0 /= w;
    switch (i) {
      case 0:
        return sin(w * pi * t) * sin(2. * pi * (x[0] - bb) / aa + pi);
      case 1:
        return 0;
      case 2:
        return cos(w * pi * t) * cos(2. * pi * ((x[0] - bb) / aa) + pi) + 1.0;
      default:
        return 0.0;
    }
  }
};

class ElasticBenchmark_Constant_V : public ElasticPhaseField {
public:
  ElasticBenchmark_Constant_V(std::shared_ptr<const DGDiscretization> _disc) :
    ElasticPhaseField(_disc, 2) {
    dim = 3;
  }

  const char *Name() const { return "v = (1,2)"; }

  bool Exact() const { return true; }

  bool RHS() const { return true; }

  bool InitialValues() const { return true; }

  double ut(double t, const Point &x, int i) const {
    if (dim == 3) {
      switch(i) {
        case 0 :
          return 0;
        case 1 :
          return 0;
        case 3 :
          return 0;
        case 5 :
          return -2;
        case 6 :
          return 0;
        case 7 :
          return 0;
        case 8 :
          return 0;
        default :
          return 0;
      }
    }
    switch(i) {
      case 3 :
        return 1;
      case 4 :
        return 2;
      default :
        return 0;
    }
  }
};

class ElasticBenchmark_Constant_S : public ProblemElasticity {
public:
  ElasticBenchmark_Constant_S(
      std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc,
                                                                         2) {}

  const char *Name() const { return "S = (1,3,2)"; }

  bool Exact() const { return true; }

  double ut(double t, const Point &x, int i) const {
    switch(i) {
      case 0 :
        return 1;
      case 1 :
        return 3;
      case 2 :
        return 2;
      default :
        return 0;
    }
  }

  bool RHS() const { return false; }
};

class ElasticBenchmark_Constant_P_div_free : public ProblemElasticity {
public:
  ElasticBenchmark_Constant_P_div_free(std::shared_ptr<const DGDiscretization> _disc)
    : ProblemElasticity(_disc, 2) {}

  bool Exact() const { return true; }

  const char *Name() const { return "S = (x,-x-y;-x-y,y)"; }

  double ut(double t, const Point &x, int i) const {
    switch(i) {
      case 1 :
        return x[1];
      case 2 :
        return -x[0];
      default :
        return 0;
    }
  }
};

class ElasticBenchmark_Quadratic : public ProblemElasticity {
public:
  ElasticBenchmark_Quadratic(
      std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc,
                                                                         2) {
    mu = 0.5;
    lambda = 1.0;
    rho = 1.0;
    c_P = sqrt((2.0 * mu + lambda) / rho);
    c_S = sqrt(mu / rho);
    // v = ( t*x+0.75*t^2, x*y) sigma = (t^2 + t * x + 0.5 * x^2, 0.5 * t^2 + 2 * t * x, 0.5*y*t)
  }

  const char *Name() const { return "Q2 Polynom"; }

  double ut(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
//         mout << OUTX(z) << endl;
    switch(i) {
      case 0 : {
//                 mout << OUTX(t*t + t * x + 0.5 * x*x) << endl;
        return t * t + t * x + 0.5 * x * x;
      }
      case 1 :
        return 0.5 * t * t + 2 * t * x;
      case 2 :
        return 0.5 * y * t;
      case 3 :
        return t * x + 0.75 * t * t;
      case 4 :
        return x * y;
      default :
        return 0.0;
    }
  }
};

class ElasticBenchmark_SinCosRHS : public ProblemElasticity {
  double omegat;
  double omega_x;
public:
  ElasticBenchmark_SinCosRHS(
      std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc,
                                                                         2) {
    mu = 0.5;
    lambda = 1.0;
    rho = 1.0;
    c_P = sqrt((2.0 * mu + lambda) / rho);
    c_S = sqrt(mu / rho);
    omegat = M_PI;
    omega_x = M_PI;
    // v = (sin(omega_t * t)*cos(omega_x * y),cos(omega_t * t))
    //sigma = (cos(omega_x * x),sin(omega_t * omega_x * t * y), 0.0)
    //v_RHS = (omega_t*cos(omega_t*t)*cos(omega_x*y) + 1.0*omega_x*sin(omega_x*x), -1.0*omega_t*omega_x*t*cos(omega_t*omega_x*t*y) - omega_t*sin(omega_t*t))
    //s_RHS = (0, omega_t*omega_x*y*cos(omega_t*omega_x*t*y), 0.5*omega_x*sin(omega_t*t)*sin(omega_x*y))

  }

  const char *Name() const { return "sin - cos"; }

  bool RHS() const {
    return true;
  }

  double ut(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
    switch(i) {
      case 0 :
        return cos(omega_x * x);
      case 1 :
        return sin(omegat * omega_x * t * y);
      case 2 :
        return 0.0;
      case 3 :
        return sin(omegat * t) * cos(omega_x * y);
      case 4 :
        return cos(omegat * t);
      default :
        return 0.0;
    }
  }

  double utb(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
    switch(i) {
      case 0 :
        return 0.0;
      case 1 :
        return omegat * omega_x * y * cos(omegat * omega_x * t * y);
      case 2 :
        return 0.5 * omega_x * sin(omegat * t) * sin(omega_x * y);
      case 3 :
        return omegat * cos(omegat * t) * cos(omega_x * y)
          + 1.0 * omega_x * sin(omega_x * x);
      case 4 :
        return -1.0 * omegat * omega_x * t * cos(omegat * omega_x * t * y)
          - omegat * sin(omegat * t);
      default :
        return 0.0;
    }
  }
};

class ElasticBenchmark_RHS : public ProblemElasticity {
public:
  ElasticBenchmark_RHS(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {
    mu = 0.5;
    lambda = 1.0;
    rho = 1.0;
    c_P = sqrt((2.0 * mu + lambda) / rho);
    c_S = sqrt(mu / rho);
  }

  const char *Name() const { return "s_xx = x*x"; }

  bool RHS() const {
    return true;
  }

  double ut(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
    switch(i) {
      case 0 :
        return x * x;
      case 1 :
        return 0.0;
      case 2 :
        return 0.0;
      case 3 : {
        return t;
      }
      case 4 :
        return 0.0;
      default :
        return 0.0;
    }
  }

  double utb(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
    switch(i) {
      case 0 :
        return 0.0;
      case 1 :
        return 0.0;
      case 2 :
        return 0.0;
      case 3 :
        return -2.0 * x + 1;
      case 4 :
        return 0.0;
      default :
        return 0.0;
    }
  }
};

class ElasticBenchmark_RHS_2 : public ProblemElasticity {
public:
  ElasticBenchmark_RHS_2(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {
    mu = 0.5;
    lambda = 1.0;
    rho = 1.0;
    c_P = sqrt((2.0 * mu + lambda) / rho);
    c_S = sqrt(mu / rho);
  }

  const char *Name() const { return "s_xx = x*x, v_y = x*y"; }

  bool RHS() const {
    return true;
  }

  double ut(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
    switch(i) {
      case 0 :
        return x * x;
      case 1 :
        return 0;
      case 2 :
        return 0.0;
      case 3 : {
        return t + x * y;
      }
      case 4 : {
        return x * y;
      }
      default :
        return 0.0;
    }
  }

  double utb(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
    switch(i) {
      case 0 :
        return -(x + 2.0 * y);
      case 1 :
        return -(2.0 * x + y);
      case 2 :
        return -(x + y) / 2.0;
      case 3 :
        return -2.0 * x + 1;
      case 4 :
        return 0.0;
      default :
        return 0.0;
    }
  }
};

class ElasticBenchmark_RHS_3 : public ProblemElasticity {
public:
  ElasticBenchmark_RHS_3(std::shared_ptr<const DGDiscretization> _disc) : ProblemElasticity(_disc, 2) {
    mu = 0.5;
    lambda = 1.0;
    rho = 1.0;
    c_P = sqrt((2.0 * mu + lambda) / rho);
    c_S = sqrt(mu / rho);
  }

  const char *Name() const { return "v(x,y) = x"; }

  bool RHS() const { return true; }

  double ut(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
    switch(i) {
      case 0 :
        return 0;
      case 1 :
        return 0;
      case 2 :
        return 0.0;
      case 3 : {
        return x;
      }
      case 4 : {
        return 0.0;
      }
      default :
        return 0.0;
    }
  }

  double utb(double t, const Point &z, int i) const {
    double x = z[0];
    double y = z[1];
    switch(i) {
      case 0 :
        return -2.0;
      case 1 :
        return -1.0;
      case 2 :
        return 0.0;
      case 3 :
        return 0.0;
      case 4 :
        return 0.0;
      default :
        return 0.0;
    }
  }
};

ProblemElasticity *GetProblemElasticity(
    std::shared_ptr<const DGDiscretization> _disc, const std::string &name) {
  if (name == "PlaneWave2D") return new Plane_Wave_2D(_disc);
  else if (name == "ElasticRiemann") return new Elastic_Riemann(_disc);
  else if (name == "ElasticPhaseField") return new Elastic_PhaseField(_disc);
  else if (name == "ElasticPhaseFieldBnd") return new Elastic_PhaseFieldBnd(_disc);
  else if (name == "ElasticPhaseFieldArc") return new Elastic_PhaseFieldArc(_disc);
  else if (name == "ElasticPhaseFieldArc2") return new Elastic_PhaseFieldArc2(_disc);
  else if (name == "ElasticPhaseFieldArc_3d") return new Elastic_PhaseFieldArc_3d(_disc);
  else if (name == "ElasticPhaseFieldArc_3d_test")
    return new Elastic_PhaseFieldArc_3d_test(_disc);
  else if (name == "ElasticPhaseFieldTest_1d")
    return new Elastic_PhaseFieldTest_1d(_disc);
  else if (name == "ElasticPhaseFieldBnd_nl") return new Elastic_PhaseFieldBnd_nl(_disc);
  else if (name == "ElasticPhaseFieldBnd_2d") return new Elastic_PhaseFieldBnd_2d(_disc);
  else if (name == "ElasticPhaseFieldNeumann")
    return new Elastic_PhaseFieldNeumann(_disc);
  else if (name == "ElasticPhaseFieldDirichlet")
    return new Elastic_PhaseFieldDirichlet(_disc);
  else if (name == "ElasticPhaseFieldPoint") return new Elastic_PhaseFieldPoint(_disc);
  else if (name == "ElasticPhaseFieldBar") return new Elastic_PhaseFieldBar(_disc);
  else if (name == "ElasticPhaseFieldNeumann_s")
    return new Elastic_PhaseFieldNeumann_s(_disc);
  else if (name == "ElasticPhaseFieldInterface_s")
    return new Elastic_PhaseFieldInterface_s(_disc);
  else if (name == "ElasticPhaseFieldNeumann_p")
    return new Elastic_PhaseFieldNeumann_p(_disc);
  else if (name == "ElasticPhaseFieldInterface_p")
    return new Elastic_PhaseFieldInterface_p(_disc);
  else if (name == "ElasticPhaseFieldFracture")
    return new Elastic_PhaseFieldFracture(_disc);
  else if (name == "ElasticPlaneWave") return new Elastic_PlaneWave(_disc);
  else if (name == "PlaneWave2DTest") return new Plane_Wave_2D_Test(_disc);
  else if (name == "SphericalWave2D") return new Spherical_Wave_2D(_disc);
  else if (name == "PlaneWave2D_1") return new Plane_Wave_2D_2(_disc);
  else if (name == "Hat2D") return new Hat_2D(_disc);
  else if (name == "Bar1D") return new Bar_1D(_disc);
  else if (name == "Linear") return new Linear(_disc);
  else if (name == "ElasticBenchmarkConstantV")
    return new ElasticBenchmark_Constant_V(_disc);
  else if (name == "ElasticBenchmarkConstantS")
    return new ElasticBenchmark_Constant_S(_disc);
  else if (name == "ElasticBenchmarkConstantPdivfree")
    return new ElasticBenchmark_Constant_P_div_free(_disc);
  else if (name == "ElasticBenchmarkQuadratic")
    return new ElasticBenchmark_Quadratic(_disc);
  else if (name == "ElasticBenchmarkRHS") return new ElasticBenchmark_RHS(_disc);
  else if (name == "ElasticBenchmarkRHS_2") return new ElasticBenchmark_RHS_2(_disc);
  else if (name == "ElasticBenchmarkRHS_3") return new ElasticBenchmark_RHS_3(_disc);
  else if (name == "ElasticBenchmark_SinCosRHS")
    return new ElasticBenchmark_SinCosRHS(_disc);
  else if (name == "Div_free_test") return new Div_free_test(_disc);
  else Exit("Error: no wave found!");
}

void MeshBar(int N = 8) {
  std::string filename = "Bar_per";
  Config::Get("Mesh", filename);
  // filename = std::string("TimeStepping/conf/geo/") + filename +
  // std::to_string(N) + std::string(".geo");
  filename =
      std::string("TimeStepping/conf/geo/") + filename + std::string(".geo");
  M_ofstream out(filename.c_str());
  double h = 1;
  out << "POINTS:" << endl;
  for (int i = -N; i <= 0; ++i) {
    double x = i * h;
    out << Point(x, -0.5) << endl
        << Point(x, 0.5) << endl;
  }
  out << "CELLS:" << endl;
  for (int i = 0; i < N; ++i)
    out << "4 1 "
        << 2 * i << " " << 2 * (i + 1) << " "
        << 2 * (i + 1) + 1 << " " << 2 * i + 1
        << endl;
  out << "FACES:" << endl;
  out << "2 0 0 1" << endl;
  for (int i = 0; i < N; ++i)
    out << "2 990 " << 2 * i << " " << 2 * (i + 1) << endl
        << "2 991 " << 2 * i + 1 << " " << 2 * (i + 1) + 1 << endl;
  out << "2 0 " << 2 * N << " " << 2 * N + 1 << endl;
}

void MeshBar0_3D(int N = 8) {
  std::string filename = "Bar_per";
  Config::Get("Mesh", filename);
  // filename = std::string("TimeStepping/conf/geo/") + filename +
  // std::to_string(N) + std::string(".geo");
  filename =
      std::string("TimeStepping/conf/geo/") + filename + std::string(".geo");
  M_ofstream out(filename.c_str());
  double h = 1.0 / N;
  out << "POINTS:" << endl;
  for (int i = 0; i <= N; ++i) {
    double x = i * h;
    out << Point(x, -0.5 * h, -0.5 * h) << endl
        << Point(x, 0.5 * h, -0.5 * h) << endl
        << Point(x, 0.5 * h, 0.5 * h) << endl
        << Point(x, -0.5 * h, 0.5 * h) << endl;
  }
  out << "CELLS:" << endl;
  for (int i = 0; i < N; ++i)
    out << "8 1 "
        << 4 * i << " " << 4 * (i + 1) << " " << 4 * (i + 1) + 1 << " " << 4 * i + 1
        << " "
        << 4 * i + 3 << " " << 4 * (i + 1) + 3 << " " << 4 * (i + 1) + 2 << " "
        << 4 * i + 2
        << endl;
  out << "FACES:" << endl;
  out << "4 2 0 1 3 2" << endl;
  for (int i = 0; i < N; ++i)
    out << "4 0 " << 4 * i << " " << 4 * (i + 1) << " " << 4 * (i + 1) + 1 << " "
        << 4 * i + 1 << endl
        << "4 0 " << 4 * i << " " << 4 * (i + 1) << " " << 4 * (i + 1) + 3 << " "
        << 4 * i + 3 << endl
        << "4 0 " << 4 * (i + 1) + 1 << " " << 4 * i + 1 << " " << 4 * (i + 1) + 2 << " "
        << 4 * i + 2 << endl
        << "4 0 " << 4 * i + 3 << " " << 4 * (i + 1) + 3 << " " << 4 * (i + 1) + 2 << " "
        << 4 * i + 2 << endl;
  out << "4 2 " << 4 * N << " " << 4 * N + 1 << " " << 4 * N + 3 << " " << 4 * N + 2
      << endl;
}

void MeshBar0(int N = 8) {
  int dim = 2;
  Config::Get("dim", dim);
  if (dim == 3) {
    MeshBar0_3D(N);
    return;
  }
  std::string filename = "Bar_per";
  Config::Get("Mesh", filename);
  // filename = std::string("TimeStepping/conf/geo/") + filename +
  // std::to_string(N) + std::string(".geo");
  filename =
      std::string("TimeStepping/conf/geo/") + filename + std::string(".geo");
  M_ofstream out(filename.c_str());
  double h = 1.0 / N;
  out << "POINTS:" << endl;
  for (int i = 0; i <= N; ++i) {
    double x = i * h;
    out << Point(x, -0.5 * h) << endl
        << Point(x, 0.5 * h) << endl;
  }
  out << "CELLS:" << endl;
  for (int i = 0; i < N; ++i)
    out << "4 1 "
        << 2 * i << " " << 2 * (i + 1) << " "
        << 2 * (i + 1) + 1 << " " << 2 * i + 1
        << endl;
  out << "FACES:" << endl;
  out << "2 2 0 1" << endl;
  for (int i = 0; i < N; ++i)
    out << "2 0 " << 2 * i << " " << 2 * (i + 1) << endl
        << "2 0 " << 2 * i + 1 << " " << 2 * (i + 1) + 1 << endl;
  out << "2 2 " << 2 * N << " " << 2 * N + 1 << endl;
}
