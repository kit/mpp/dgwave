#ifndef _DGASSEMBLE_H_
#define _DGASSEMBLE_H_

#include "m++.hpp"
#include "Assemble.hpp"

const int MaxPointTypesDG = 10;
const int MaxQuadraturePointsDG = 100;
const int MaxNodalPointsDG = 100;

class DGAssemble : public IAssemble {
protected:
  VtuPlot &plot;
  double gamma;

public:
  DGAssemble(VtuPlot &_plot) : plot(_plot), gamma(1) {}

  double Residual(const Vector &, Vector &) const { return 0; }

  virtual const char *Name() const { return "DGAssemble"; }

  virtual std::string DiscName() const = 0;

  virtual double Energy(const Vector &u) const = 0;

  virtual void EnergyCorrection(Vector &u) const {Exit("not implemented"); }

  virtual void SetInitialValue(double, Vector &u) = 0;

  virtual void PrintStep(double, Vector &, int) = 0;

  virtual double PV_Max(Vector &u) const { return -1111; }

  void VtkPlotting(const Vector &, int) {}

  void VtkPlotting(const Vector &, int, double) {}

  virtual void InitialStep(double t, Vector &u) {
    u = 0;
    PrintStep(0, u, t);
    VtkPlotting(u, 0);
  }

  virtual double InitTimeStep(double, double &, Vector &) { return 0; }

  virtual bool reassemble() const { return true; }

  virtual void FinishStep(Vector &u, double t, double dt,
                          int step, int plot_step = 1) {
    if (step % plot_step == 0) {
      PrintStep(step, u, t);
      VtkPlotting(u, step);
    }
  }

  virtual void PrintMatrixInfo(Matrix &A, int diagonal = 1) = 0;

  virtual void FluxMatrix(Matrix &) const = 0;

  virtual void MassMatrix(Matrix &) const = 0;

  virtual void MassMatrix2(Matrix &M) const { MassMatrix(M); }

  virtual void RHS(double t, Vector &u, Vector &b) const { RHS(t, b); }

  virtual void RHS(double, Vector &) const = 0;

  virtual bool RHS() const = 0;

  void SetGamma(double g) { gamma = g; }

  void PrintInfo(const Vector &u) const {
    mout << endl << " " << Name() << endl;
    const Mesh &M = u.GetMesh();
    size_t no_cells = M.CellCount();
    int NE = 0;
    for (edge e = u.edges(); e != u.edges_end(); ++e)
      if (u.master(e())) ++NE;
    int NF = 0;
    for (face f = u.faces(); f != u.faces_end(); ++f) {
      if (u.master(f())) ++NF;
    }
    int NV = 0;
    for (vertex v = u.vertices(); v != u.vertices_end(); ++v)
      if (u.master(v())) ++NV;
    mout << endl
         << " Dimension:      " << M.dim() << endl
         << " Cells:          " << PPM->Sum(no_cells) << endl
         << " Problem size:   " << u.pSize() << endl
         << " Vertices:       " << PPM->Sum(NV) << endl
         << " Edges:          " << PPM->Sum(NE) << endl
         << " Faces:          " << PPM->Sum(NF) << endl
         << " Mesh width:     ["
         << M.MinMeshWidth() << "," << M.MaxMeshWidth() << "]" << endl
         << " Assemble        " << Name() << endl
         << endl;

  }

  virtual void SetMat(double) {}

  virtual void SetMat1() {}

  virtual void SetMat2() {}

  virtual void SetMat3() {}

  virtual double Repeat() { return 0; }
};

#endif
