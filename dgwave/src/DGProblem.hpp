#include "DGDiscretization.hpp"
#include "LagrangeDiscretization.hpp"
#include "MeshIndex.hpp"
#include "Point.hpp"
#include "Vector.hpp"
#include "VtuPlot.hpp"
#include "ctools.hpp"

#include "m++.hpp"

class MatrixGraphWithOverlap : public IMatrixGraph {
public:
  MatrixGraphWithOverlap(const Mesh &mesh, std::unique_ptr<IDoF> _dof, int depth = 1) :
      IMatrixGraph(mesh, std::move(_dof)) {
    CellsOverlap_dG1();
    AddCells(depth);
    AddOverlapCells(depth);
    Init();

  }

  void CellsOverlap_dG1() {
    int commSplit = CommSplit();
    std::map<Point, bool> clean_list;

    for (procset p = meshAndOverlapProcSets.Begin(); p != meshAndOverlapProcSets.End(); ++p) {
      if (find_vertex(p()) != vertices_end()) continue;
      if (find_face(p()) != faces_end()) continue;
      if (find_edge(p()) != edges_end()) continue;
      if (p() == Infty) continue;
      clean_list[p()] = true;
    }

    for (auto &p: clean_list) {
      meshAndOverlapProcSets.Remove(p.first);
    }

    ExchangeBuffer cellBuffer(commSplit);
    ExchangeBuffer faceBuffer(commSplit);
    ExchangeBuffer edgeBuffer(commSplit);

    for (cell c = cells(); c != cells_end(); ++c) {
      for (int fi = 0; fi < c.Faces(); fi++) {
        face f_tmp = find_face(c.Face(fi));
        procset pf = meshAndOverlapProcSets.Find(f_tmp());
        if (pf != meshAndOverlapProcSets.End()) {
          if (meshAndOverlapProcSets.Find(c()) == meshAndOverlapProcSets.End()) {
            meshAndOverlapProcSets.Add(c(), PPM->Proc(commSplit));
          }

          for (int i = 0; i < pf.size(); ++i)
            meshAndOverlapProcSets.Append(c(), pf[i]);
        }
      }

      procset pc = meshAndOverlapProcSets.Find(c());
      if (pc != meshAndOverlapProcSets.End())
        for (int q = 0; q < pc.size(); ++q) {
          if (pc[q] == PPM->Proc(commSplit)) continue;
          cellBuffer.Send(pc[q]) << c;
          cellBuffer.Send(pc[q]) << short(pc.size() - 1); // send size of procset
          for (int ql = 1; ql < pc.size(); ++ql) // send procset without master
            cellBuffer.Send(pc[q]) << short(pc[ql]);
        }
    }

    cellBuffer.Communicate();

    for (int q = 0; q < PPM->Size(commSplit); ++q) {
      if (q == PPM->Proc(commSplit)) continue;
      while (cellBuffer.Receive(q).size() < cellBuffer.Receive(q).Size()) {
        int tp;
        short n, sd, q_list_size;
        cellBuffer.Receive(q) >> tp >> sd >> n;
        std::vector<Point> x(n);
        for (int i = 0; i < n; ++i) cellBuffer.Receive(q) >> x[i];

        Cell *C = CreateCell(CELLTYPE(tp), sd, x);
#ifdef USE_DATAMESH
        DataContainer d;
      cellBuffer.Receive(q) >> d;
      C->SetData(d);
#endif

        InsertOverlapCell(C);
        meshAndOverlapProcSets.Add(C->Center(), q);

        for (int i = 0; i < C->Faces(); ++i)
          InsertOverlapFace(C->Face(i), C->Center());

        cellBuffer.Receive(q) >> q_list_size;
        for (int ql = 0; ql < q_list_size; ++ql) {
          short proc;
          cellBuffer.Receive(q) >> proc;
          meshAndOverlapProcSets.Append(C->Center(), proc);
        }
        delete C;
      }
    }
  }
};

class LagrangeDiscretizationWithOverlap : public LagrangeDiscretization {
public:
  LagrangeDiscretizationWithOverlap(const Meshes &meshes, int degree, int size = 1) :
  LagrangeDiscretization(meshes, degree, size) {}

  std::unique_ptr<IMatrixGraph> createMatrixGraph(
      MeshIndex levels) const override {
    return std::make_unique<MatrixGraphWithOverlap>((this->meshes)[levels], createDoF(levels));
  }
};


#define OUTS(s)         __STRING(s) << " " << s << " "
#define OUTX(s)         __STRING(s) << endl << s

class ProblemElasticity {
  std::shared_ptr<const DGDiscretization> disc;
  int verbose;
protected:
  bool exact;
  int dim;
  double lambda;
  double mu;
  double rho;
  double c_S;
  double c_P;
  double c_E;
  std::string meshname;

  virtual double Lambda(const Point &x) const { return lambda; }

  virtual double Mu(const Point &x) const { return mu; }

  virtual double Rho(const Point &x) const { return rho; }

  virtual double C_s(const Point &x) const { return sqrt(Mu(x) / Rho(x)); }

  virtual double C_p(const Point &x) const { return sqrt((2 * Mu(x) + Lambda(x)) / Rho(x)); }

  virtual double ut(double, const Point &, int) const { return 0; }

  virtual double utb(double, const Point &, int) const { return 0; }

  std::shared_ptr<LagrangeDiscretizationWithOverlap> pf_disc;
  std::shared_ptr<LagrangeDiscretizationWithOverlap> U_disc;
  Vector *U;
  Vector *pf;
  Vector *pf2;
  Vector *pf_old;
public:
  virtual double ut0(double, const Point &, int) const { return 0; }

  ProblemElasticity(std::shared_ptr<const DGDiscretization> _disc, int d = 3) :
      disc(_disc), dim(d), verbose(0), exact(false),
      mu(1), rho(1), lambda(1), meshname("none"),
      U(nullptr), pf_disc(nullptr), pf(nullptr), pf2(nullptr), pf_old(nullptr) {
    Config::Get("ProblemVerbose", verbose);
    Config::Get("ProblemExact", exact);
    Config::Get("lambda", lambda);
    Config::Get("mu", mu);
    Config::Get("rho", rho);
    Config::Get("Mesh", meshname);
    c_P = sqrt((2.0 * mu + lambda) / rho);
    c_S = sqrt(mu / rho);
    c_E = sqrt(mu / rho);
  }

  const LagrangeDiscretizationWithOverlap &GetPFDisc() const { return *pf_disc; }

  const LagrangeDiscretizationWithOverlap &GetUDisc() const { return *U_disc; }

  Vector *GetU() const { return U; }

  Vector *GetPF() const { return pf; }

  Vector *GetPF2() const { return pf2; }

  Vector *GetPF_old() const { return pf_old; }

  bool UsePF() const { return (pf != 0); }

  virtual void SetPF(const Meshes &MM) { pf = 0; }

  virtual void UpdatePF() const {}

  virtual void PrintInfo() const {}

  virtual const char *Name() const = 0;

  virtual const char *MeshName() const { return meshname.c_str(); }

  virtual bool Exact() const { return exact; }

  virtual bool RHS() const { return false; }

  virtual bool VolumeData() const { return false; }

  virtual bool InitialValues() const { return false; }

  virtual double U0(double t, const Point &z, int i) const {
    return 0;
  }

  virtual double ut(double t, const cell &C, const Point &lz, const Point &z, int i) const {
    return ut(t, z, i);
  }

  virtual double utb(double t, const cell &C, const Point &lz, const Point &z, int i) const {
    return utb(t, z, i);
  }

  virtual double ut_dual(double &, const Point &, int, const Point &, const Point &) const {
    return 0;
  }

  virtual double Phasefield(const cell &C, const Point &lz, const Point &z) const { return 1; }

  virtual double ProjectedPhasefield(const cell &C, const Point &lz, const Point &z) const {
    return Phasefield(C, lz, z);
  }

  virtual double Phasefield2(const cell &C, const Point &lz, const Point &z) const { return 1; }

  virtual double ProjectedPhasefield2(const cell &C, const Point &lz, const Point &z) const {
    return Phasefield2(C, lz, z);
  }

  virtual double Phasefield_old(const cell &C, const Point &lz, const Point &z) const { return 1; }

  virtual double ProjectedPhasefield_old(const cell &C, const Point &lz, const Point &z) const {
    return Phasefield_old(C, lz, z);
  }

  virtual double PhasefieldDifference(const cell &C, const Point &lz, const Point &z) const {
    return 0;
  }

  virtual double Repeat() const { return 0; }

  virtual double Tau(const cell &C, const Point &lz, const Point &x) const {
    return 0.0;
  }

  virtual double Lambda(const cell &C, const Point &lz, const Point &x) const {
    return Lambda(x);
  }

  virtual double Mu(const cell &C, const Point &lz, const Point &x) const { return Mu(x); }

  virtual double OldLambda(const cell &C, const Point &lz, const Point &x) const {
    return Lambda(x);
  }

  virtual double OldMu(const cell &C, const Point &lz, const Point &x) const { return Mu(x); }

  virtual double Rho(const cell &C, const Point &lz, const Point &x) const { return Rho(x); }

  virtual double OldRho(const cell &C, const Point &lz, const Point &x) const { return Rho(x); }

  double C_s(const cell &C, const Point &lz, const Point &x) const {
    return sqrt(Mu(C, lz, x) / Rho(C, lz, x));
  }

  double C_p(const cell &C, const Point &lz, const Point &x) const {
    return sqrt((2 * Mu(C, lz, x) + Lambda(C, lz, x)) / Rho(C, lz, x));
  }

  double Z_s(const cell &C, const Point &lz, const Point &x) const {
    return sqrt(Mu(C, lz, x) * Rho(C, lz, x));
  }

  double Z_p(const cell &C, const Point &lz, const Point &x) const {
    return sqrt((2 * Mu(C, lz, x) + Lambda(C, lz, x)) * Rho(C, lz, x));
  }

  virtual Tensor StressStrain(const cell &C,
                              const Point &lz, const Point &z, const Tensor &S) const {
    double mu = Mu(C, lz, z);
    double lambda = Lambda(C, lz, z);
    double s = 1 / (2.0 * mu);
    double ss = lambda / (2.0 * mu * (3.0 * lambda + 2.0 * mu));
    return s * S - ss * trace(S) * One;
  }

  virtual Tensor OldStressStrain(const cell &C,
                                 const Point &lz, const Point &z, const Tensor &S) const {
    double mu = OldMu(C, lz, z);
    double lambda = OldLambda(C, lz, z);
    double s = 1 / (2.0 * mu);
    double ss = lambda / (2.0 * mu * (3.0 * lambda + 2.0 * mu));
    return s * S - ss * trace(S) * One;
  }

  virtual Tensor StrainStress(const cell &C,
                              const Point &lz, const Point &z, const Tensor &E) const {
    double mu = Mu(C, lz, z);
    double lambda = Lambda(C, lz, z);
    return 2 * mu * E + lambda * trace(E) * One;
  }

  virtual VectorField Dirichlet(double t, const cell &C, const Point &lz,
                                const Point &z) const {
    return VectorField(ut(t, C, lz, z, 3), ut(t, C, lz, z, 4), 0.0);
  }

  virtual VectorField Neumann(double t, const cell &C, const Point &lz,
                              const Point z,
                              const VectorField &N) const {

    Tensor S(ut(t, C, lz, z, 0), ut(t, C, lz, z, 2), 0.0,
             ut(t, C, lz, z, 2), ut(t, C, lz, z, 1), 0.0,
             0.0, 0.0, 0.0);
    return S * N;
  }

  void get_u_int(double t, Vector &u) const;

  virtual bool SmallStrain() const { return true; }

  virtual void SetMat(double) {}

  virtual void SetMat1() {}

  virtual void SetMat2() {}

  virtual void SetMat3() {}
};

ProblemElasticity *GetProblemElasticity(
    std::shared_ptr<const DGDiscretization> _disc, const std::string &name);
