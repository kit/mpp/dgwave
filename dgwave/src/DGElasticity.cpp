#include "DGProblem.hpp"
#include "Tools.hpp"
#include "Point.hpp"
#include "Matrix.hpp"
#include "Vector.hpp"
#include "RMatrix.hpp"
#include "RVector.hpp"
#include "TimeSeries.hpp"
#include "DGAssemble.hpp"
#include "DGTimeIntegrator.hpp"
#include "Preconditioner.hpp"
#include "LinearSolver.hpp"
#include "Spectrum.hpp"
#include "Element.hpp"
#include "ScalarElement.hpp"
#include "VectorFieldElement.hpp"
#include "ctools.hpp"
#include "MeshesCreator.hpp"
#include "DGMatrixGraph.hpp"

#include <iostream>
#include <numbers>
#include <unordered_map>

int CellDoFs(const Meshes &M, int c_deg) {
  const cell &c = M.coarse().cells();
  int m = 9;
  if (c.dim() == 2) m = 5;
  if (c.dim() == 1) m = 2;
  return m;

  mout << "Dofs " << c.Type() << " m " << m << " c_deg " << c_deg << " t "
       << QUADRILATERAL << endl;

  switch (c.Type()) {
    case INTERVAL:
      return m * (c_deg + 1);
    case TRIANGLE:
      return m * (c_deg + 1) * (c_deg + 2) / 2;
    case TETRAHEDRON: Exit("not implemented");
    case QUADRILATERAL:
      return m * (c_deg + 1) * (c_deg + 1);
    case HEXAHEDRON:
      return m * (c_deg + 1) * (c_deg + 1) * (c_deg + 1);
  }
  Exit("not implemented");
  return -1;
}

class RowEntries_dG {
  double *a;
  int n;
  int nf;

  void fill(Matrix &A, const rows &R, const rows &Rf) {
    n = R[0].n();
    nf = Rf[0].n();
    a = A(R[0], Rf[0]);
  }

public:
  RowEntries_dG(Matrix &A, const rows &R, const rows &Rf) { fill(A, R, Rf); }

  RowEntries_dG(Matrix &A, const cell &c, const cell &cf) {
    row r = A.find_row(c());
    row rf = A.find_row(cf());
    n = r.n();
    nf = rf.n();
    a = A(r, rf);
  }

  RowEntries_dG(Matrix &A, const cell &c) {
    row r = A.find_row(c());
    n = nf = r.n();
    a = A(r, r);
  }

  double &operator()(int k, int l) {
    if (k * nf + l >= n * nf) {
      mout << OUT(n) << OUT(nf) << OUT(k) << OUT(l) << endl;

      Exit("too large");
    }
    return a[k * nf + l];
  }

  void Print() const {
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < nf; ++j)
        mout << " " << a[i * nf + j];
      mout << endl;
    }
  }
};

class DGElasticityElement : public Element {
  const Shape &S;
  const ShapeValues<double> &value;
  VectorField gradient[MaxQuadraturePointsDG][MaxNodalPointsDG];
  VectorField velocity[MaxQuadraturePointsDG][MaxNodalPointsDG];
  Tensor stress[MaxQuadraturePointsDG][MaxNodalPointsDG];
  Tensor strain[MaxQuadraturePointsDG][MaxNodalPointsDG];
  VectorField divstress[MaxQuadraturePointsDG][MaxNodalPointsDG];
  int deg;
  const cell &C;
  int dim;
public:
  Point GlobalToLocal(const Point &z) const {
    switch (C.Type()) {
      case INTERVAL: {
        Tensor T;
        T[0][0] = C.Corner(1)[0] - C.Corner(0)[0];
        T[0][1] = 0;
        T[1][0] = 0;
        T[1][1] = 1;
        T[0][2] = 0;
        T[1][2] = 0;
        T[2][0] = 0;
        T[2][1] = 0;
        T[2][2] = 1;
        Tensor IT = Invert(T);
        return IT * (z - C.Corner(0));
      }

      case TRIANGLE: {
        Tensor T;
        T[0][0] = C.Corner(1)[0] - C.Corner(0)[0];
        T[0][1] = C.Corner(2)[0] - C.Corner(0)[0];
        T[1][0] = C.Corner(1)[1] - C.Corner(0)[1];
        T[1][1] = C.Corner(2)[1] - C.Corner(0)[1];
        T[0][2] = 0;
        T[1][2] = 0;
        T[2][0] = 0;
        T[2][1] = 0;
        T[2][2] = 1;
        Tensor IT = Invert(T);
        return IT * (z - C.Corner(0));
      }

      case TETRAHEDRON: Exit("not implemented");

      case QUADRILATERAL: {
        Tensor T;
        T[0][0] = C.Corner(1)[0] - C.Corner(0)[0];
        T[0][1] = C.Corner(3)[0] - C.Corner(0)[0];
        T[1][0] = C.Corner(1)[1] - C.Corner(0)[1];
        T[1][1] = C.Corner(3)[1] - C.Corner(0)[1];
        T[0][2] = 0;
        T[1][2] = 0;
        T[2][0] = 0;
        T[2][1] = 0;
        T[2][2] = 1;
        Tensor IT = Invert(T);
        return IT * (z - C.Corner(0));
      }

      case HEXAHEDRON: Exit("not implemented");
      default: Exit("not implemented")
    }
  }

  void NodalPoints(Point *z) const {
    if (C.Type() == HEXAHEDRON) {
      if (deg > 1) {
        int index = 0;
        for (int i = 0; i < (deg + 1); i++) {
          for (int j = 0; j < (deg + 1); j++) {
            for (int k = 0; k < (deg + 1); k++) {
              z[index] = ((c[1] - c[0])
                          * ((double) k / deg))
                         + ((c[3] - c[0])
                            * ((double) j / deg))
                         + ((c[4] - c[0])
                            * ((double) i / deg));
              index++;
            }
          }
        }
      } else if (deg == 1) {
        for (int index = 0; index < c.Corners(); index++)
          z[index] = c[index];
      } else
        z[0] = c();
    }
    Exit("not implemented");
  }

  void LocalNodalPoints(Point *z) const {
    if (C.Type() == HEXAHEDRON) {
      if (deg > 1) {
        int index = 0;
        for (int i = 0; i < (deg + 1); i++) {
          for (int j = 0; j < (deg + 1); j++) {
            for (int k = 0; k < (deg + 1); k++) {
              z[index] = ((c.LocalCorner(1) - c.LocalCorner(0)) * ((double) k / deg))
                         + ((c.LocalCorner(3) - c.LocalCorner(0)) * ((double) j / deg))
                         + ((c.LocalCorner(4) - c.LocalCorner(0)) * ((double) i / deg));
              index++;
            }
          }
        }
      } else if (deg == 1) {
        for (int index = 0; index < c.Corners(); index++)
          z[index] = c.LocalCorner(index);
      } else {
        z[0] = Point();
        for (int index = 0; index < c.Corners(); index++)
          z[0] += c.LocalCorner(index);
        z[0] /= c.Corners();
      }
    }
    Exit("not implemented");
  }

  int get_deg() { return deg; }

  DGElasticityElement(const DGDiscretization &disc, const VectorMatrixBase &g,
                      const cell &c) :
      Element(g, *c),
      S(disc.GetShape(*c)), value(S.values()),
      C(c), dim(c.dim()) {
    Config::Get("deg", deg);
    for (int q = 0; q < nQ(); ++q) {
      for (int i = 0; i < S.size(); ++i) {
        gradient[q][i] = GetTransformation(q) * S.LocalGradient(q, i);
      }
      for (int k = 0; k < dim; ++k) {
        VectorField V = zero;
        V[k] = 1.0;
        for (int i = 0; i < S.size(); ++i) {
          velocity[q][i + k * S.size()] = value[q][i] * V;
          Tensor Dv = Zero;
          Dv[k] = gradient[q][i];
          strain[q][i + k * S.size()] = sym(Dv);
        }
      }
      for (int i = 0; i < S.size(); ++i) {
        for (int k = 0; k < dim; ++k) {
          stress[q][i + k * S.size()] = Zero;
          stress[q][i + k * S.size()][k][k] = value[q][i];
          divstress[q][i + k * S.size()] = zero;
          divstress[q][i + k * S.size()][k] = gradient[q][i][k];
        }
        if (dim == 2) {
          stress[q][i + 2 * S.size()] = Zero;
          stress[q][i + 2 * S.size()][0][1] = value[q][i];
          stress[q][i + 2 * S.size()][1][0] = value[q][i];
          divstress[q][i + 2 * S.size()][0] = gradient[q][i][1];
          divstress[q][i + 2 * S.size()][1] = gradient[q][i][0];
        } else if (dim == 3) {
          stress[q][i + 3 * S.size()] = Zero;
          stress[q][i + 3 * S.size()][0][1] = value[q][i];
          stress[q][i + 3 * S.size()][1][0] = value[q][i];
          stress[q][i + 4 * S.size()] = Zero;
          stress[q][i + 4 * S.size()][0][2] = value[q][i];
          stress[q][i + 4 * S.size()][2][0] = value[q][i];
          stress[q][i + 5 * S.size()] = Zero;
          stress[q][i + 5 * S.size()][1][2] = value[q][i];
          stress[q][i + 5 * S.size()][2][1] = value[q][i];
          divstress[q][i + 3 * S.size()][0] = gradient[q][i][1];
          divstress[q][i + 3 * S.size()][1] = gradient[q][i][0];
          divstress[q][i + 3 * S.size()][2] = 0;
          divstress[q][i + 4 * S.size()][0] = gradient[q][i][2];
          divstress[q][i + 4 * S.size()][1] = 0;
          divstress[q][i + 4 * S.size()][2] = gradient[q][i][0];
          divstress[q][i + 5 * S.size()][0] = 0;
          divstress[q][i + 5 * S.size()][1] = gradient[q][i][2];
          divstress[q][i + 5 * S.size()][2] = gradient[q][i][1];
        }
      }
    }
  }

  int shape_size() const { return S.size(); }

  int u_dimension() const {
    if (dim == 3) return 9;
    if (dim == 2) return 5;
    if (dim == 1) return 2;
    return -1;
  }

  int V_dimension() const { return dim * shape_size(); }

  int S_dimension() const {
    if (dim == 3) return 6 * shape_size();
    if (dim == 2) return 3 * shape_size();
    if (dim == 1) return shape_size();
    return -1;
  }

  VectorField Velocity(int q, int i) {
    return velocity[q][i];
  }

  Tensor Stress(int q, int i) {
    return stress[q][i];
  }

  VectorField divStress(int q, int i) {
    return divstress[q][i];
  }

  Tensor Strain(int q, int i) {
    return strain[q][i];
  }

  VectorField divStress(int q, const Vector &u) {
    VectorField S_div = zero;
    for (int i = 0; i < S_dimension(); ++i) {
      S_div += u(C(), i) * divStress(q, i);
    }
    return S_div;
  }

  VectorField Velocity(int q, const Vector &u) {
    VectorField V = zero;
    for (int i = 0; i < V_dimension(); ++i) {
      V += u(C(), S_dimension() + i) * velocity[q][i];
    }
    return V;
  }

  VectorField Velocity(Point &lz, const Vector &u) {
    VectorField V = zero;
    for (int i = 0; i < S.size(); ++i) {
      double v_i = S(lz, i);
      for (int k = 0; k < dim; ++k) {
        V[k] += u(C(), S_dimension() + i + k * S.size()) * v_i;
      }
    }
    return V;
  }

  Tensor Strain(int q, const Vector &u) {
    Tensor E = Zero;
    for (int i = 0; i < V_dimension(); ++i) {
      E += u(C(), S_dimension() + i) * strain[q][i];
      mout << "strain[" << q << "][" << i << "]: " << endl << strain[q][i] << endl
           << endl;
    }
    return E;
  }

  Tensor Stress(const Point &lz, const Vector &u) {
    Tensor P = Zero;
    for (int k = 0; k < dim; ++k) {
      for (int i = 0; i < S.size(); ++i) {
        double v = S(lz, i);
        P[k][k] += u(C(), i + k * S.size()) * v;
        if (dim == 2) {
          P[0][1] += u(C(), i + 2 * S.size()) * v;
          P[1][0] += u(C(), i + 2 * S.size()) * v;
        } else if (dim == 3) {
          P[0][1] += u(C(), i + 3 * S.size()) * v;
          P[1][0] += u(C(), i + 3 * S.size()) * v;
          P[0][2] += u(C(), i + 4 * S.size()) * v;
          P[2][0] += u(C(), i + 4 * S.size()) * v;
          P[2][1] += u(C(), i + 5 * S.size()) * v;
          P[1][2] += u(C(), i + 5 * S.size()) * v;
        }
      }
    }
    return P;
  }

  Tensor GetStress(const Vector &u, int i) {
    if (dim == 1)
      return Tensor(u(C(), i), 0.0, 0.0,
                    0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0);
    else if (dim == 2)
      return Tensor(u(C(), i), u(C(), i + 2 * S.size()), 0.0,
                    u(C(), i + 2 * S.size()), u(C(), i + S.size()), 0.0,
                    0.0, 0.0, 0.0);
    return Tensor(u(C(), i),
                  u(C(), i + 3 * S.size()),
                  u(C(), i + 4 * S.size()),
                  u(C(), i + 3 * S.size()),
                  u(C(), i + S.size()),
                  u(C(), i + 5 * S.size()),
                  u(C(), i + 4 * S.size()),
                  u(C(), i + 5 * S.size()),
                  u(C(), i + 2 * S.size()));
  }

  void SetStress(const Tensor &P, Vector &u, int i) {
    if (dim == 1) {
      u(C(), i) = P[0][0];
    } else if (dim == 2) {
      u(C(), i) = P[0][0];
      u(C(), i + S.size()) = P[1][1];
      u(C(), i + 2 * S.size()) = P[0][1];
    } else {
      u(C(), i) = P[0][0];
      u(C(), i + S.size()) = P[1][1];
      u(C(), i + 2 * S.size()) = P[2][2];
      u(C(), i + 3 * S.size()) = P[0][1];
      u(C(), i + 4 * S.size()) = P[0][2];
      u(C(), i + 5 * S.size()) = P[1][2];
    }
  }

  VectorField GetVelocity(const Vector &u, int i) {
    if (dim == 1)
      return VectorField(u(C(), S_dimension() + i), 0.0, 0.0);
    else if (dim == 2)
      return VectorField(u(C(), S_dimension() + i), u(C(), S_dimension() + i), 0.0);
    else if (dim == 3)
      return VectorField(u(C(), S_dimension() + i),
                         u(C(), S_dimension() + i + S.size()),
                         u(C(), S_dimension() + i + 2 * S.size()));
    Exit("not implemented")
  }

  void SetVelocity(const VectorField &V, Vector &u, int i) {
    for (int j = 0; j < dim; ++j)
      u(C(), S_dimension() + i + j * S.size()) = V[j];
  }

  Tensor Stress(int q, const Vector &u) {
    Tensor S = Zero;
    for (int i = 0; i < S_dimension(); ++i)
      S += u(C(), i) * stress[q][i];
    return S;
  }

  double SVValue(const Point &lz, const Vector &u, int k) const {
    double v = 0;
    for (int i = 0; i < S.size(); ++i)
      v += u(C(), k * S.size() + i) * S(lz, i);
    return v;
  }
};

class DGElasticityFaceElement : public FaceElement {
  VectorField velocity[MaxQuadraturePointsDG][MaxNodalPointsDG];
  Tensor stress[MaxQuadraturePointsDG][MaxNodalPointsDG];
  int d;
  int dim;
  const Shape &S;
  const cell &c;
  face f;
  int fid;
  Point qTangent[MaxQuadraturePointsDG];
  double value[MaxQuadraturePointsDG][MaxNodalPointsDG];
  double ww;
public:
  DGElasticityFaceElement(const DGDiscretization &D, const VectorMatrixBase &g,
                          const cell &C, int f_id) :
      FaceElement(g, *C, f_id),
      S(D.GetShape(*C)),
      c(C), f(g.find_face(c.Face(f_id))),
      fid(f_id), dim(C.dim()) {
    d = dim * S.size();
    ww = 0;
    for (int q = 0; q < nQ(); ++q) ww += QWeight(q);
    ww = c.LocalFaceArea(fid) / ww;
    if (dim == 1) {
      for (int q = 0; q < nQ(); ++q)
        qTangent[q] = Point(0.0, 1.0);
    } else {
      for (int q = 0; q < nQ(); ++q) {
        qTangent[q] = c.FaceCorner(f_id, 1) - c.FaceCorner(f_id, 0);
        double w = norm(qTangent[q]);
        qTangent[q] *= (1 / w);
      }
    }
    for (int q = 0; q < nQ(); ++q) {
      for (int i = 0; i < S.size(); ++i) {
        Point lz = QLocal(q);
        value[q][i] = S(lz, i);
      }
      for (int k = 0; k < dim; ++k) {
        VectorField V = zero;
        V[k] = 1.0;
        for (int i = 0; i < S.size(); ++i) {
          velocity[q][i + k * S.size()] = zero;
          velocity[q][i + k * S.size()][k] = value[q][i] * V[k];
        }
      }
      for (int k = 0; k < dim; ++k) {
        for (int i = 0; i < S.size(); ++i) {
          stress[q][i + k * S.size()] = Zero;
          stress[q][i + k * S.size()][k][k] = value[q][i];
          if (dim == 2) {
            stress[q][i + 2 * S.size()] = Zero;
            stress[q][i + 2 * S.size()][0][1] = value[q][i];
            stress[q][i + 2 * S.size()][1][0] = value[q][i];
          }
          if (dim == 3) {
            stress[q][i + 3 * S.size()] = Zero;
            stress[q][i + 3 * S.size()][0][1] = value[q][i];
            stress[q][i + 3 * S.size()][1][0] = value[q][i];
            stress[q][i + 4 * S.size()] = Zero;
            stress[q][i + 4 * S.size()][0][2] = value[q][i];
            stress[q][i + 4 * S.size()][2][0] = value[q][i];
            stress[q][i + 5 * S.size()] = Zero;
            stress[q][i + 5 * S.size()][1][2] = value[q][i];
            stress[q][i + 5 * S.size()][2][1] = value[q][i];
          }
        }
      }
    }
  }

  const Point &LocalFaceCorner(int i) const {
    return c.LocalCorner(c.facecorner(fid, i));
  }

  Point Local(const Point &local) const {
    switch (c.FaceCorners(fid)) {
      case 2:
        return (1 - local[0]) * LocalFaceCorner(0)
               + local[0] * LocalFaceCorner(1);
      case 3:
        return (1 - local[0] - local[1]) * LocalFaceCorner(0)
               + local[0] * LocalFaceCorner(1)
               + local[1] * LocalFaceCorner(2);
      case 4:
        return (1 - local[0]) * (1 - local[1]) * LocalFaceCorner(0)
               + local[0] * (1 - local[1]) * LocalFaceCorner(1)
               + local[0] * local[1] * LocalFaceCorner(2)
               + (1 - local[0]) * local[1] * LocalFaceCorner(3);
      default: Exit("not implemented")
    }
  }

  const Point &QTangent(int q) const { return qTangent[q]; }

  int shape_size() const { return S.size(); }

  int dimension() const {
    if (dim == 3) return 9 * shape_size();
    if (dim == 2) return 5 * shape_size();
    if (dim == 1) return 2 * shape_size();
    return -1;
  }

  int V_dimension() const { return dim * shape_size(); }

  int S_dimension() const {
    if (dim == 3) return 6 * shape_size();
    if (dim == 2) return 3 * shape_size();
    if (dim == 1) return shape_size();
    return -1;
  }

  VectorField Velocity(int q, int i) {
    return velocity[q][i];
  }

  Tensor Stress(int q, int i) {
    return stress[q][i];
  }
};

void ProblemElasticity::get_u_int(double t, Vector &u) const {
  u = 0;
  Point lz[MaxNodalPointsDG];
  Point z[MaxNodalPointsDG];
  int deg = 0;
  Config::Get("deg", deg);
  if (deg == 0) {
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      row r = u.find_row(c());
      DGElasticityElement Elem(*disc, u, c);
      for (int i = 0; i < Elem.u_dimension(); ++i)
        for (int j = 0; j < c.Corners(); ++j)
          u(r)[i] += ut(t, c, c.LocalCorner(j), c[j], i) / c.Corners();
    }
  } else {
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      row r = u.find_row(c());
      DGElasticityElement Elem(*disc, u, c);
      Elem.NodalPoints(z);
      Elem.LocalNodalPoints(lz);
      for (int i = 0; i < Elem.u_dimension(); ++i)
        for (int j = 0; j < Elem.shape_size(); ++j)
          u(r)[i * Elem.shape_size() + j] = ut(t, c, lz[j], z[j], i);
    }
  }
  u.Accumulate();
}

class DGElasticityAssembleMatrix : public DGAssemble {
protected:
  std::shared_ptr<const DGDiscretization> disc;
  ProblemElasticity &problem;
  double flux_alpha;
  Point GF_a;
  Point GF_b;
  Point z_eval;
  double EE;
  const Vector *U;
  double _dt;
  double _t;
  int _step;
  int _pstep = 0;
  double sigma_c;
  double sigma_c2;
  double tau_r;
  double scale_u;
  double s_min;
  double s_min2;
  double s_min3;
  LinearSolver *S;
  double l_c;
  double W_c;
  double dt_pf;
  double dt_pf2;
  double t_max = 1000;
  double f_max = 0;
  bool _reassemble = true;
  bool _reversible = false;
  bool project_pf = false;
  bool _inelastic = false;
  bool plot_inelastic = false;
  bool print_V = false;
  bool StressProjection;
  bool mass_correction = false;
  bool checkConsistency = false;
  double penalty = 0;
  int PFVerbose;
  int cnt_all = 0;
public:
  DGElasticityAssembleMatrix(std::shared_ptr<const DGDiscretization> disc,
                             ProblemElasticity &P, VtuPlot &plot) :
      DGAssemble(plot), disc(disc), problem(P),
      flux_alpha(1), z_eval(Infty), EE(1), U(0), _dt(0),
      _reassemble(false), _inelastic(false), StressProjection(true), PFVerbose(0),
      sigma_c(1), sigma_c2(1), tau_r(1), S(0), scale_u(1), l_c(0), _step(0),
      s_min(0.1), s_min3(1), W_c(0.1), s_min2(-10), dt_pf(1), dt_pf2(1) {
    Config::Get("flux_alpha", flux_alpha);
    //Config::Get("gamma",gamma);
    Config::Get("t_max", t_max);
    Config::Get("roi_min", GF_a);
    Config::Get("roi_max", GF_b);
    Config::Get("z_eval", z_eval);
    Config::Get("sigma_c", sigma_c);
    Config::Get("penalty", penalty);
    sigma_c2 = sigma_c;
    Config::Get("sigma_c2", sigma_c2);
    Config::Get("tau_r", tau_r);
    Config::Get("scale_u", scale_u);
    Config::Get("s_min", s_min);
    Config::Get("s_min2", s_min2);
    s_min3 = s_min;
    Config::Get("s_min3", s_min3);
    Config::Get("l_c", l_c);
    Config::Get("W_c", W_c);
    Config::Get("dt_pf", dt_pf);
    Config::Get("dt_pf2", dt_pf2);
    Config::Get("StressProjection", StressProjection);
    Config::Get("PFVerbose", PFVerbose);
    Config::Get("plot_inelastic", plot_inelastic);
    Config::Get("print_V", print_V);
    Config::Get("reversible", _reversible);
    Config::Get("project_pf", project_pf);
    Config::Get("mass_correction", mass_correction);
    Config::Get("checkConsisteny", checkConsistency);
    S = GetLinearSolver();
  }

  double PV_Max(Vector &u) const {
    double pv_max = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      DGElasticityElement elem(*disc, u, c);
      for (int q = 0; q < elem.nQ(); ++q) {
        Tensor Sq = elem.Stress(q, u);
        double pv = max_pv(Sq);
        pv_max = std::max(pv_max, pv);
      }
    }
    pv_max = PPM->Max(pv_max);
    if (abs(pv_max) < 1e-20) pv_max = 0;
    return PPM->Max(pv_max);
  }

  bool reassemble() const {
    if (problem.SmallStrain()) return _reassemble;
    else return true;
  }

  const char *Name() const { return "DGAssembleMatrix"; }

  double min_pv(const Tensor &S) const {
    SymRMatrix SM(3, 3);
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        SM(i, j) = S[i][j];
    Spectrum Spec(SM);
    return Spec.min();
  }

  double max_pv(const Tensor &S) const {
    SymRMatrix SM(3, 3);
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        SM(i, j) = S[i][j];
    Spectrum Spec(SM);
    return Spec.max();
  }

  VectorField max_ev(const Tensor &S) const {
    SymRMatrix SM(3, 3);
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        SM(i, j) = S[i][j];
    RVector w(3);
    Spectrum Spec(SM);
    for (int j = 0; j < 3; ++j)
      if (Spec(j) == Spec.max())
        Spec(j, w);
    return VectorField(w[0], w[1], w[2]);
  }

  double mid_pv(const Tensor &S) const {
    SymRMatrix SM(3, 3);
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        SM(i, j) = S[i][j];
    Spectrum Spec(SM);
    //mout << Spec();
    return Spec()[1];
  }

  double min2_pv(const Tensor &S) const {
    SymRMatrix SM(3, 3);
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        SM(i, j) = S[i][j];
    Spectrum Spec(SM);
    mout << OUT(trace(S)) << Spec();
    return Spec.min();
  }

  double RHS_PF(double t, const Vector &u, const Vector &pf, Vector &rhs_pf) {
    rhs_pf = 0;
    f_max = 0;
    double pf_min = 1;
    Tensor S_min = Zero;
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      ScalarElement E(rhs_pf, *c);
      DGElasticityElement elem(*disc, u, c);
      RowBndValues r_c(rhs_pf, *c);
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        Tensor Sq = elem.Stress(q, u);
        double PF = E.Value(elem.LocalQPoint(q), pf);
        if (PF < s_min) continue;
        VectorField D_PF = E.Derivative(elem.LocalQPoint(q), pf);
        double f = std::max(max_pv(Sq) - sigma_c, 0.0) / sigma_c;
        pf_min = std::min(PF, pf_min);
        f_max = std::max(f_max, f);
        if (f_max == f) S_min = Sq;
        for (unsigned int i = 0; i < E.size(); ++i) {
          double PF_i = E.Value(elem.LocalQPoint(q), i);
          VectorField D_PF_i = E.Derivative(elem.LocalQPoint(q), i);
          r_c(i) += w * ((W_c * (1 - PF) - f) * PF_i - W_c * l_c * l_c * D_PF * D_PF_i);
        }
      }
    }
    f_max = PPM->Max(f_max);
    pf_min = PPM->Min(pf_min);
    if (f_max > 0) {
      _inelastic = true;
    }
    rhs_pf.Collect();
    _inelastic = PPM->Boolean(_inelastic);
    return rhs_pf.norm();
  }

  void Jacobi_PF(Matrix &A) const {
    A = 0;
    for (cell c = A.cells(); c != A.cells_end(); ++c) {
      ScalarElement E(A, *c);
      RowEntries A_c(A, E);
      for (int q = 0; q < E.nQ(); ++q) {
        double w = E.QWeight(q);
        for (int i = 0; i < E.size(); ++i) {
          double PF_i = E.Value(q, i);
          VectorField D_PF_i = E.Derivative(q, i);
          for (int j = 0; j < E.size(); ++j) {
            double PF_j = E.Value(q, j);
            VectorField D_PF_j = E.Derivative(q, j);
            A_c(i, j) += w
                         *
                         ((tau_r / dt_pf + W_c) * PF_i * PF_j + W_c * l_c * l_c * D_PF_i * D_PF_j);
          }
        }
      }
    }
    A.ClearDirichletValues();
    return;
  }

  double RHS_U(const Vector &u, Vector &rhs_U) const {
    rhs_U = 0;
    double v_max = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      VectorFieldElement E(rhs_U, *c);
      DGElasticityElement elem(*disc, u, c);
      RowBndValues r_c(rhs_U, *c);
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        VectorField Vq = elem.Velocity(q, u);
        Point z = E.QPoint(q);
        Point lz = E.LocalQPoint(q);
        v_max = std::max(v_max, norm(Vq));
        for (unsigned int i = 0; i < E.size(); ++i) {
          double PF_i = E.Value(elem.LocalQPoint(q), i);
          r_c(i, 0) += w * scale_u * _dt * Vq[0] * PF_i;
          r_c(i, 1) += w * scale_u * _dt * Vq[1] * PF_i;
        }
      }
    }
    rhs_U.Collect();
    return rhs_U.norm();
  }

  void Jacobi_U(Matrix &A) const {
    A = 0;
    for (cell c = A.cells(); c != A.cells_end(); ++c) {
      VectorFieldElement E(A, *c);
      RowEntries A_c(A, E);
      for (int q = 0; q < E.nQ(); ++q) {
        double w = E.QWeight(q);
        for (int i = 0; i < E.size(); ++i) {
          double PF_i = E.Value(q, i);
          for (int j = 0; j < E.size(); ++j) {
            double PF_j = E.Value(q, j);
            A_c(i, j, 0, 0) += w * PF_i * PF_j;
            A_c(i, j, 1, 1) += w * PF_i * PF_j;
          }
        }
      }
    }
    A.ClearDirichletValues();
  }

  void U2(double t, const Vector &U) const {
    double U_max = -infty;
    double U_min = infty;
    double U_sum = 0;
    double U_diff = 0;
    double A = 0;
    for (cell c = U.cells(); c != U.cells_end(); ++c) {
      VectorFieldElement E(U, *c);
      for (int q = 0; q < E.nQ(); ++q) {
        double w = E.QWeight(q);
        Point z = E.QPoint(q);
        VectorField Uq = E.VectorValue(q, U);
        U_max = std::max(U_max, norm(Uq));
        U_min = std::min(U_min, norm(Uq));
        U_sum += w * Uq * Uq;
        A += w;
        Uq -=
            (scale_u / (EE * EE)) * VectorField(problem.U0(t, z, 0), problem.U0(t, z, 1));
        U_diff += w * Uq * Uq;
      }
    }
    U_max = PPM->Max(U_max);
    U_min = PPM->Min(U_min);
    U_sum = PPM->Sum(U_sum);
    U_diff = PPM->Sum(U_diff);
    A = PPM->Sum(A);
    mout << "|U|_2 = " << sqrt(U_sum / A) << " " << U_min << " < |U| < " << U_max;
    if (problem.Exact()) mout << " |U-U_h|_2 = " << sqrt(U_diff / A);
    mout << endl;
  }

  double Admissible(double s, double ds) {
    if (s < s_min) return s_min2;
    s += ds;
    if (s > 1) return 1;
    if (s < s_min) return s_min2;
    return s;
    if (s < s_min) return -10;
    s += ds;
    if (s > 1) return 1;
    if (s < s_min) return -10;
    return s;
    if (s < s_min) return s;
    s += ds;
    if (s > 1) return 1;
    if (s < s_min) return 0;
    return s;
  }

  double Repeat() { return problem.Repeat(); }

  double InitTimeStep(double t_old, double &t, Vector &u) {
    _t = t;
    Vector u_tmp(u);
    _reassemble = false;
    _dt = t - t_old;
    if (!problem.UsePF()) return 0;
    Vector &pf = *problem.GetPF();
    Vector rhs_pf(pf);
    double d = RHS_PF(t, u, pf, rhs_pf);
    if (d > 1e-11) {
      Matrix A(pf);
      Jacobi_PF(A);
      Vector pfold(pf);
      pf += (*S)(A) * rhs_pf;
      int cnt_pf = 0;
      for (row r = pf.rows(); r != pf.rows_end(); ++r) {
        double s = pf(r, 0);
        if (pfold(r, 0) < s_min) {
          pf(r, 0) = 0;
        } else {
          if (s > 1) {
            if (project_pf)
              pf(r, 0) = 1;
          } else if (s < s_min) {
            procset ps = pf.find_procset(r());
            if (ps == pf.procsets_end())
              ++cnt_pf;
            else if (ps.master() == PPM->Proc())
              ++cnt_pf;
            if (project_pf)
              pf(r, 0) = 0;
          }
          if (!_reversible)
            pf(r, 0) = std::min(s, pfold(r, 0));
        }
      }
      cnt_pf = PPM->Sum(cnt_pf);
      cnt_all += cnt_pf;
      if (cnt_pf > 0) {
        Vector &pf_old = *problem.GetPF_old();
        Vector &pf2 = *problem.GetPF2();
        mout << "    " << OUTS(t) << OUTS(pf.Max()) << OUTS(pf.Min())
             << OUTS(pf2.Max()) << OUTS(pf2.Min()) << endl;
        mout << "    Reassemble cnt_pf "
             << cnt_pf << " of " << cnt_all << " at " << t << endl;
        _reassemble = true;
        pf_old = pf2;
        for (row r = pf.rows(); r != pf.rows_end(); ++r) {
          double s = pf(r, 0);
          if (s > 1) s = 1;
          else if (s < s_min) s = 0;
          pf2(r, 0) = std::min(pf2(r, 0), s);
        }
        if (Repeat() > 0) {
          u = 0;
          mout << "set u=0 " << endl;
        }
      }
    }
    Vector &U = *problem.GetU();
    Vector V(U);
    Vector Vcnt(U);
    V.Clear();
    Vcnt.Clear();
    for (cell c = U.cells(); c != U.cells_end(); ++c) {
      DGElasticityElement elem(*disc, U, c);
      for (int i = 0; i < c.Corners(); ++i) {
        Point lz = c.LocalCorner(i);
        VectorField Vel = elem.Velocity(lz, u);
        Vcnt(c[i], 0) += 1;
        for (int j = 0; j < U.dim(); ++j)
          V(c[i], j) += Vel[j];
      }
    }
    V.Accumulate();
    Vcnt.Accumulate();
    for (row r = V.rows(); r != V.rows_end(); ++r)
      if (Vcnt(r, 0) > 0)
        for (int j = 0; j < U.dim(); ++j)
          V(r, j) *= 1 / Vcnt(r, 0);
    U += _dt * V;
    if (U.dim() == 1) return f_max;
    return f_max;
    for (row r = U.rows(); r != U.rows_end(); ++r)
      mout << r() << " Ux " << U(r, 0) << " Vx " << V(r, 0) << " Vcnt " << Vcnt(r, 0)
           << endl;
    return f_max;
  }

  void MassMatrix(Matrix &M) const {
    M = 0;
    for (cell c = M.cells(); c != M.cells_end(); ++c) {
      DGElasticityElement elem(*disc, M, c);
      RowEntries_dG M_c(M, c, c);
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        Point lz = elem.LocalQPoint(q);
        Point z = elem.QPoint(q);
        double rho = problem.Rho(c, lz, z);
        for (int i = 0; i < elem.S_dimension(); ++i) {
          Tensor S_i = elem.Stress(q, i);
          Tensor E_i = problem.StressStrain(c, lz, z, S_i);
          for (int j = 0; j < elem.S_dimension(); ++j) {
            Tensor S_j = elem.Stress(q, j);
            M_c(i, j) += w * Frobenius(E_i, S_j);
          }
        }
        for (int i = 0; i < elem.V_dimension(); ++i) {
          VectorField V_i = elem.Velocity(q, i);
          for (int j = 0; j < elem.V_dimension(); ++j) {
            VectorField V_j = elem.Velocity(q, j);
            M_c(elem.S_dimension() + i, elem.S_dimension() + j)
                += w * rho * V_i * V_j;
          }
        }
      }
    }
  }

  void MassMatrix2(Matrix &M) const {
    Vector &pf2 = *problem.GetPF2();
    M = 0;
    for (cell c = M.cells(); c != M.cells_end(); ++c) {
      DGElasticityElement elem(*disc, M, c);
      RowEntries_dG M_c(M, c, c);
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        Point lz = elem.LocalQPoint(q);
        Point z = elem.QPoint(q);
        double rho = problem.Rho(c, lz, z);
        for (int i = 0; i < elem.S_dimension(); ++i) {
          Tensor S_i = elem.Stress(q, i);
          Tensor E_i = problem.StressStrain(c, lz, z, S_i);
          for (int j = 0; j < elem.S_dimension(); ++j) {
            Tensor S_j = elem.Stress(q, j);
            M_c(i, j) += w * Frobenius(E_i, S_j);
          }
        }
        for (int i = 0; i < elem.V_dimension(); ++i) {
          VectorField V_i = elem.Velocity(q, i);
          for (int j = 0; j < elem.V_dimension(); ++j) {
            VectorField V_j = elem.Velocity(q, j);
            M_c(elem.S_dimension() + i, elem.S_dimension() + j)
                += w * rho * V_i * V_j;
          }
        }
      }
      if (penalty == 0) continue;
      for (int f = 0; f < c.Faces(); ++f) {
        if (check_if_bnd(M, c, f)) continue;
        cell cf = M.find_neighbour_cell(c, f);
        int f1 = M.find_neighbour_face_id(c.Face(f), cf);
        DGElasticityFaceElement felem(*disc, M, c, f);
        DGElasticityFaceElement felem_1(*disc, M, cf, f1);
        RowEntries_dG M_cf(M, c, cf);
        for (int q = 0; q < felem.nQ(); ++q) {
          double w = felem.QWeight(q);
          VectorField Nq = felem.QNormal(q);
          for (int i = 0; i < felem.S_dimension(); ++i) {
            Tensor S_i = felem.Stress(q, i);
            for (int j = 0; j < felem.S_dimension(); ++j) {
              Tensor S_j = felem.Stress(q, j);
              M_c(i, j) += w * penalty * (S_i * Nq) * (S_j * Nq);
            }
            for (int j = 0; j < felem_1.S_dimension(); ++j) {
              Tensor S_j = felem_1.Stress(q, j);
              M_cf(i, j) -= w * penalty * (S_i * Nq) * (S_j * Nq);
            }
          }
          for (int i = 0; i < felem.V_dimension(); ++i) {
            VectorField V_i = felem.Velocity(q, i);
            for (int j = 0; j < felem.V_dimension(); ++j) {
              VectorField V_j = felem.Velocity(q, j);
              M_c(felem.S_dimension() + i, felem.S_dimension() + j) +=
                  w * penalty * V_i * V_j;
            }
            for (int j = 0; j < felem_1.V_dimension(); ++j) {
              VectorField V_j = felem_1.Velocity(q, j);
              M_cf(felem.S_dimension() + i, felem_1.S_dimension() + j) -=
                  w * penalty * V_i * V_j;
            }
          }
        }
      }
      if (!mass_correction) continue;
      ScalarElement E(pf2, *c);
      for (int f = 0; f < c.Faces(); ++f) {
        double PF = E.Value(c.LocalFace(f), pf2);
        if (PF != 0.0) continue;
        DGElasticityFaceElement felem(*disc, M, c, f);
        for (int q = 0; q < felem.nQ(); ++q) {
          double w = felem.QWeight(q);
          Point lz = felem.QLocal(q);
          Point z = felem.QPoint(q);
          VectorField Nq = felem.QNormal(q);
          double mu = problem.Mu(c, lz, z);
          for (int i = 0; i < elem.S_dimension(); ++i) {
            VectorField SN_i = felem.Stress(q, i) * Nq;
            for (int j = 0; j < elem.S_dimension(); ++j) {
              VectorField SN_j = felem.Stress(q, j) * Nq;
              M_c(i, j) += (w / mu) * (SN_i * SN_j);
            }
          }
        }
      }
    }
  }

  void Consistency(const Vector &u) const {
    return;
    if (!checkConsistency) return;
    double cons_V = 0;
    double cons_S = 0;
    const Vector &pf2 = *problem.GetPF2();
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      DGElasticityElement elem(*disc, u, c);
      for (int f = 0; f < c.Faces(); ++f) {
        if (check_if_bnd(u, c, f)) continue;
        cell cf = u.find_neighbour_cell(c, f);
        if (c() < cf()) continue;
        DGElasticityElement elem_f1(*disc, u, cf);
        int f1 = u.find_neighbour_face_id(c.Face(f), cf);
        DGElasticityFaceElement felem(*disc, u, c, f);
        DGElasticityFaceElement felem_1(*disc, u, cf, f1);
        ScalarElement E(pf2, *c);
        for (int q = 0; q < felem.nQ(); ++q) {
          VectorField Nq = felem.QNormal(q);
          Point z = felem.QPoint(q);
          Point lz = felem.QLocal(q);
          double Pf = E.Value(lz, pf2);
          if (Pf == 0) continue;
          Tensor S = elem.Stress(lz, u);
          VectorField V = elem.Velocity(lz, u);
          Point lz_1 = felem_1.QLocal(q);
          Tensor Sf = elem_f1.Stress(lz_1, u);
          VectorField Vf = elem_f1.Velocity(lz_1, u);
          cons_S = std::max(cons_S, abs(S[0][0] - Sf[0][0]));
          cons_V = std::max(cons_V, abs(V[0] - Vf[0]));
        }
      }
    }
    cons_V = PPM->Max(cons_V);
    cons_S = PPM->Max(cons_S);
    mout << " " << OUT(cons_V) << OUT(cons_S) << endl;
  }

  double VFlux_c(const VectorField &V, const VectorField &TN) const {
    return TN * V;
  }

  double VFlux_cf(const VectorField &V, const VectorField &TN,
                  short bnd) const {
    if (bnd == 1) return -(TN * V);             // Dirichlet bnd condition
    else return TN * V;
  }

  double SFlux_c(const Tensor &S, const VectorField &TN,
                 const VectorField &N) const {
    VectorField NS = S * N;
    return NS * TN;
  }

  double SFlux_cf(const Tensor &S, const VectorField &TN, const VectorField &N,
                  short bnd) const {
    VectorField NS = S * N;
    double TNNS = NS * TN;
    if ((bnd == 0) || (bnd == 2)) return -TNNS;
    else return TNNS;
  }

  void FluxMatrix(Matrix &A) const {
    A = 0;
    const Vector &pf2 = *problem.GetPF2();
    for (cell c = A.cells(); c != A.cells_end(); ++c) {
      DGElasticityElement elem(*disc, A, c);
      RowEntries_dG A_c(A, c, c);
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        Point lz = elem.LocalQPoint(q);
        Point z = elem.QPoint(q);
        double tau = problem.Tau(c, lz, z);
        double mu = problem.Mu(c, lz, z);
        if (mu > 0.001) tau = 0;
        for (int i = 0; i < elem.S_dimension(); ++i) {
          VectorField divS_i = elem.divStress(q, i);
          for (int j = 0; j < elem.V_dimension(); ++j) {
            VectorField V_j = elem.Velocity(q, j);
            A_c(elem.S_dimension() + j, i) += w * V_j * divS_i;
          }
        }
        for (int i = 0; i < elem.V_dimension(); ++i) {
          Tensor E_i = elem.Strain(q, i);
          for (int j = 0; j < elem.S_dimension(); ++j) {
            Tensor S_j = elem.Stress(q, j);
            A_c(j, elem.S_dimension() + i) += w * Frobenius(S_j, E_i);
          }
          VectorField V_i = elem.Velocity(q, i);
          for (int j = 0; j < elem.V_dimension(); ++j) {
            VectorField V_j = elem.Velocity(q, j);
            A_c(elem.S_dimension() + j, elem.S_dimension() + i) -= w * tau * V_j * V_i;
          }
        }
      }
      ScalarElement E(pf2, *c);
      for (int f = 0; f < c.Faces(); ++f) {
        cell cf = c;
        int f1 = f;
        int idf = -1;
        double PF = E.Value(c.LocalFace(f), pf2);
        if (check_if_bnd(A, c, f)) {
          idf = bnd_id(A, c, f);
        } else if (PF == 0) {}
        DGElasticityFaceElement felem(*disc, A, c, f);
        if (idf != -1) {
          for (int q = 0; q < felem.nQ(); ++q) {
            double w = felem.QWeight(q);
            VectorField Nq = felem.QNormal(q);
            Point lz = felem.QLocal(q);
            Point z = felem.QPoint(q);
            double Z_p = problem.Z_p(c, lz, z);
            double Z_s = problem.Z_s(c, lz, z);
            double alpha_p = 1.0 / Z_p;
            double alpha_s = 1.0 / Z_s;
            if (idf == 1) {
              VectorField Tq = felem.QTangent(q);
              for (int j = 0; j < felem.V_dimension(); ++j) {
                VectorField V_j = felem.Velocity(q, j);
                for (int i = 0; i < felem.S_dimension(); ++i) {
                  VectorField S_i = felem.Stress(q, i) * Nq;
                  A_c(i, felem.S_dimension() + j) -= w * (S_i * V_j);
                }
                for (int i = 0; i < felem.V_dimension(); ++i) {
                  VectorField V_i = felem.Velocity(q, i);
                  A_c(felem.S_dimension() + i, felem.S_dimension() + j)
                      -=
                      w * (Z_p * (Nq * V_i) * (Nq * V_j) + Z_s * (Nq ^ V_i) * (Nq ^ V_j));
                }
              }
            } else if ((idf == 0) || (idf == 2) || (idf == 22)) {
              for (int j = 0; j < felem.S_dimension(); ++j) {
                VectorField S_j = felem.Stress(q, j) * Nq;
                for (int i = 0; i < felem.S_dimension(); ++i) {
                  VectorField S_i = felem.Stress(q, i) * Nq;
                  A_c(i, j) -= w * (alpha_p * (Nq * S_i) * (Nq * S_j)
                                    + alpha_s * (Nq ^ S_i) * (Nq ^ S_j));
                }
                for (int i = 0; i < felem.V_dimension(); ++i) {
                  VectorField V_i = felem.Velocity(q, i);
                  A_c(felem.S_dimension() + i, j) -= w * (V_i * S_j);
                }
              }
            }
          }
        } else {
          cf = A.find_neighbour_cell(c, f);
          f1 = A.find_neighbour_face_id(c.Face(f), cf);
          DGElasticityFaceElement felem_1(*disc, A, cf, f1);
          RowEntries_dG A_cf(A, c, cf);
          for (int q = 0; q < felem.nQ(); ++q) {
            double w = felem.QWeight(q);
            VectorField Nq = felem.QNormal(q);
            Point lz = felem.QLocal(q);
            Point lz_1 = felem_1.QLocal(q);
            Point z = felem.QPoint(q);
            double Z_s = problem.Z_s(c, lz, z);
            double Z_p = problem.Z_p(c, lz, z);
            double Z_s_f = problem.Z_s(cf, lz_1, z);
            double Z_p_f = problem.Z_p(cf, lz_1, z);
            const double alpha1 = 1.0 / (Z_p + Z_p_f);
            const double alpha2 = Z_p * alpha1;
            const double alpha3 = Z_p_f * alpha1;
            const double alpha4 = Z_p_f * alpha2;
            const double alpha5 = 1.0 / (Z_s + Z_s_f);
            const double alpha6 = Z_s * alpha5;
            const double alpha7 = Z_s_f * alpha5;
            const double alpha8 = Z_s_f * alpha6;
            for (int i = 0; i < felem.S_dimension(); ++i) {
              VectorField S_i = felem.Stress(q, i) * Nq;
              for (int j = 0; j < felem.S_dimension(); ++j) {
                VectorField S_j = felem.Stress(q, j) * Nq;
                A_c(i, j) -= w
                             *
                             (alpha1 * (Nq * S_i) * (Nq * S_j) + alpha5 * (Nq ^ S_i) * (Nq ^ S_j));
              }
              for (int j = 0; j < felem_1.S_dimension(); ++j) {
                VectorField S_j = felem_1.Stress(q, j) * Nq;
                A_cf(i, j) += w
                              *
                              (alpha1 * (Nq * S_i) * (Nq * S_j) + alpha5 * (Nq ^ S_i) * (Nq ^ S_j));
              }
              for (int j = 0; j < felem.V_dimension(); ++j) {
                VectorField V_j = felem.Velocity(q, j);
                A_c(i, felem.S_dimension() + j) -= w
                                                   * (alpha3 * (Nq * S_i) * (Nq * V_j) +
                                                      alpha7 * (Nq ^ S_i) * (Nq ^ V_j));
              }
              for (int j = 0; j < felem_1.V_dimension(); ++j) {
                VectorField V_j = felem_1.Velocity(q, j);
                A_cf(i, felem_1.S_dimension() + j) += w
                                                      * (alpha3 * (Nq * S_i) * (Nq * V_j) +
                                                         alpha7 * (Nq ^ S_i) * (Nq ^ V_j));
              }
            }
            for (int i = 0; i < felem.V_dimension(); ++i) {
              VectorField V_i = felem.Velocity(q, i);
              for (int j = 0; j < felem.S_dimension(); ++j) {
                VectorField S_j = felem.Stress(q, j) * Nq;
                A_c(felem.S_dimension() + i, j) -=
                    w * (alpha2 * (Nq * V_i) * (Nq * S_j)
                         + alpha6 * (Nq ^ V_i) * (Nq ^ S_j));
              }
              for (int j = 0; j < felem_1.S_dimension(); ++j) {
                VectorField S_j = felem_1.Stress(q, j) * Nq;
                A_cf(felem.S_dimension() + i, j) +=
                    w * (alpha2 * (Nq * V_i) * (Nq * S_j)
                         + alpha6 * (Nq ^ V_i) * (Nq ^ S_j));
              }
              for (int j = 0; j < felem.V_dimension(); ++j) {
                VectorField V_j = felem.Velocity(q, j);
                A_c(felem.S_dimension() + i, felem.S_dimension() + j) -=
                    w * (alpha4 * (Nq * V_i) * (Nq * V_j)
                         + alpha8 * (Nq ^ V_i) * (Nq ^ V_j));
              }
              for (int j = 0; j < felem_1.V_dimension(); ++j) {
                VectorField V_j = felem_1.Velocity(q, j);
                A_cf(felem.S_dimension() + i, felem_1.S_dimension() + j) +=
                    w * (alpha4 * (Nq * V_i) * (Nq * V_j)
                         + alpha8 * (Nq ^ V_i) * (Nq ^ V_j));
              }
            }
          }
        }
      }
    }
  }

  bool RHS() const { return problem.RHS(); }

  void RHS(double t, Vector &u, Vector &b) const {
    RHS(t, b);
  }

  void RHS(double t, Vector &b) const {
    b = 0.0;
    if (!problem.RHS()) return;
    if (0)
      if (problem.Exact()) {
        for (cell c = b.cells(); c != b.cells_end(); ++c) {
          for (int f = 0; f < c.Faces(); ++f) {
            if (!check_if_bnd(b, c, f)) continue;
            row r = b.find_row(c());
            int idf = bnd_id(b, c, f);
            if (idf == 0) continue;
            DGElasticityFaceElement felem(*disc, b, c, f);
            for (int q = 0; q < felem.nQ(); ++q) {
              double w = felem.QWeight(q);
              VectorField Nq = felem.QNormal(q);
              VectorField Tq = felem.QTangent(q);
              Point lz = felem.QLocal(q);
              Point z = felem.QPoint(q);
              double Z_s = problem.Z_s(c, lz, z);
              double Z_p = problem.Z_p(c, lz, z);
              double Z_s_f = Z_s;
              double Z_p_f = Z_p;
              const double alpha1 = 1.0 / (Z_p + Z_p_f);
              const double alpha2 = Z_p * alpha1;
              const double alpha3 = Z_p_f * alpha1;
              const double alpha4 = Z_p_f * alpha2;
              const double alpha5 = 1.0 / (Z_s + Z_s_f);
              const double alpha6 = Z_s * alpha5;
              const double alpha7 = Z_s_f * alpha5;
              const double alpha8 = Z_s_f * alpha6;
              if (idf == 1) {
                VectorField V_bnd = problem.Dirichlet(t, c, lz, z);
                V_bnd *= (1 / EE);
                for (int i = 0; i < felem.V_dimension(); ++i) {
                  VectorField V_i = felem.Velocity(q, i);
                  b(r, felem.S_dimension() + i) +=
                      2 * w * (alpha2 * VFlux_c(V_i, Nq) * VFlux_c(V_bnd, Nq)
                               + alpha4 * VFlux_c(V_i, Tq) * VFlux_c(V_bnd, Tq));
                }
                for (int j = 0; j < felem.S_dimension(); ++j) {
                  Tensor S_j = felem.Stress(q, j);
                  b(r, j) +=
                      2 * w * (alpha1 * SFlux_c(S_j, Nq, Nq) * VFlux_c(V_bnd, Nq)
                               + alpha3 * SFlux_c(S_j, Tq, Nq) * VFlux_c(V_bnd, Tq));
                }
              } else if (idf == 2) {
                Tensor S_bnd(problem.ut(t, c, lz, z, 0), problem.ut(t, c, lz, z, 2),
                             problem.ut(t, c, lz, z, 2), problem.ut(t, c, lz, z, 1));
                S_bnd *= (1 / EE);
                for (int i = 0; i < felem.V_dimension(); ++i) {
                  VectorField V_i = felem.Velocity(q, i);
                  b(r, felem.S_dimension() + i) +=
                      2 * w * (alpha6 * VFlux_c(V_i, Nq) * SFlux_c(S_bnd, Nq, Nq)
                               + alpha8 * VFlux_c(V_i, Tq) * SFlux_c(S_bnd, Tq, Nq));
                }
                for (int j = 0; j < felem.S_dimension(); ++j) {
                  Tensor S_i = felem.Stress(q, j);
                  b(r, j) +=
                      2 * w * (alpha5 * SFlux_c(S_i, Nq, Nq) * SFlux_c(S_bnd, Nq, Nq)
                               + alpha7 * SFlux_c(S_i, Tq, Nq) * SFlux_c(S_bnd, Tq, Nq));
                }
              }
            }
          }
        }
      }
    if (problem.RHS()) {
      for (cell c = b.cells(); c != b.cells_end(); ++c) {
        if (problem.VolumeData()) {
          DGElasticityElement elem(*disc, b, c);
          for (int q = 0; q < elem.nQ(); ++q) {
            double w = elem.QWeight(q);
            Point lz = elem.LocalQPoint(q);
            Point z = elem.QPoint(q);
            VectorField V_rhs(problem.utb(t, c, lz, z, 3), problem.utb(t, c, lz, z, 4));
            Tensor S_rhs(problem.utb(t, c, lz, z, 0), problem.utb(t, c, lz, z, 2), 0.0,
                         problem.utb(t, c, lz, z, 2), problem.utb(t, c, lz, z, 1), 0.0,
                         0.0, 0.0, 0.0);
            for (int i = 0; i < elem.S_dimension(); ++i) {
              Tensor S_i = elem.Stress(q, i);
              b(c(), i) += w * Frobenius(S_i, S_rhs);
            }
            for (int i = 0; i < elem.V_dimension(); ++i) {
              VectorField V_i = elem.Velocity(q, i);
              b(c(), elem.S_dimension() + i) += w * V_i * V_rhs;
            }
          }
        }
        for (int f = 0; f < c.Faces(); ++f) {
          if (!check_if_bnd(b, c, f)) continue;
          int idf = bnd_id(b, c, f);
          if ((idf != 2) & (idf != 1) & (idf != 22)) continue;
          row r = b.find_row(c());
          DGElasticityFaceElement felem(*disc, b, c, f);
          for (int q = 0; q < felem.nQ(); ++q) {
            double w = felem.QWeight(q);
            Point lz = felem.QLocal(q);
            Point z = felem.QPoint(q);
            VectorField Nq = felem.QNormal(q);
            VectorField Tq = felem.QTangent(q);
            double Z_s = problem.Z_s(c, lz, z);
            double Z_p = problem.Z_p(c, lz, z);
            const double alpha_p = 1.0 / Z_p;
            const double alpha_s = 1.0 / Z_s;
            if (idf == 1) {
              VectorField V = zero;
              V[0] = problem.ut(t, c, lz, z, 6);
              V[1] = problem.ut(t, c, lz, z, 7);
              V[2] = problem.ut(t, c, lz, z, 8);
              for (int j = 0; j < felem.S_dimension(); ++j) {
                VectorField S_j = felem.Stress(q, j) * Nq;
                b(r, j) += w * (V * S_j);
              }
              for (int j = 0; j < felem.V_dimension(); ++j) {
                VectorField V_j = felem.Velocity(q, j);
                b(r, felem.S_dimension() + j) += w * (Z_p * (V * Nq) * (V_j * Nq)
                                                      + Z_s * (V ^ Nq) * (V_j ^ Nq));
              }
            } else if (idf == 22) {
              Tensor S = Zero;
              S[0][0] = problem.ut(t, c, lz, z, 0);
              S[1][1] = problem.ut(t, c, lz, z, 1);
              S[2][2] = problem.ut(t, c, lz, z, 2);
              S[0][1] = S[1][0] = problem.ut(t, c, lz, z, 3);
              S[0][2] = S[2][0] = problem.ut(t, c, lz, z, 4);
              S[1][2] = S[2][1] = problem.ut(t, c, lz, z, 5);
              VectorField SN = S * Nq;
              for (int j = 0; j < felem.S_dimension(); ++j) {
                VectorField S_j = felem.Stress(q, j) * Nq;
                b(r, j) += w * (alpha_p * (SN * Nq) * (S_j * Nq)
                                + alpha_s * (SN ^ Nq) * (S_j ^ Nq));
              }
              for (int j = 0; j < felem.V_dimension(); ++j) {
                VectorField V_j = felem.Velocity(q, j);
                b(r, felem.S_dimension() + j) += w * (SN * V_j);
              }
            } else if (idf == 2) {
              Point x = z;
              if (z[0] < 0.5) x[0] = 0;
              if (z[0] > 0.5) x[0] = 1;
              double v0 = problem.ut0(t, x, 3);
              VectorField SN = v0 * Nq;
              for (int j = 0; j < felem.S_dimension(); ++j) {
                VectorField S_j = felem.Stress(q, j) * Nq;
                b(r, j) += w * alpha_p * (SN * Nq) * (S_j * Nq);
              }
              for (int j = 0; j < felem.V_dimension(); ++j) {
                VectorField V_j = felem.Velocity(q, j);
                b(r, felem.S_dimension() + j) += w * (SN * V_j);
              }
            }
          }
          continue;
          if (r()[0] > 0.5) continue;
          RVector bb(felem.S_dimension() + felem.V_dimension());
          for (int j = 0; j < felem.S_dimension(); ++j)
            bb[j] = b(r, j);
          for (int j = 0; j < felem.V_dimension(); ++j)
            bb[felem.S_dimension() + j] = b(r, felem.S_dimension() + j);
          pout << OUT(r()) << "b " << endl << bb;
        }
      }
    }
    b.Collect();

    return;
  }

  int bnd_id(const VectorMatrixBase &A, const cell &c, int f) const {
    bnd_face fff = A.find_bnd_face(c.Face(f));
    if (fff == A.bnd_faces_end()) return -1;
    return fff.Part();
  }

  bool check_if_bnd(const VectorMatrixBase &A, const cell &c, int f) const {
    face ff = A.find_face(c.Face(f));

    if ((A.find_bnd_face(c.Face(f)) != A.bnd_faces_end())
        && ((ff.Right() == Infty) || (ff.Left() == Infty)))
      return true;
    else
      return false;
  }

  void Jacobi(const Vector &u, Matrix &B) const {
    MassMatrix(B);
    Matrix A(u);
    FluxMatrix(A);
    B += (-gamma) * A;
  }

  void Project(const Vector &F, Vector &C) const {
    C = 0;
    for (cell c = C.cells(); c != C.cells_end(); ++c) {
      row r_c = C.find_row(c());
      for (int k = 0; k < c.Children(); ++k) {
        row r_f = F.find_row(c.Child(k));
        for (int i = 0; i < r_c.n(); ++i)
          C(r_c, i) += F(r_f, i);
      }
      for (int i = 0; i < r_c.n(); ++i)
        C(r_c, i) /= c.Children();
    }
  }
};

class DGElasticityAssemble : public DGElasticityAssembleMatrix {
  double flux_alpha;
  double X0 = 0;
  double X1 = 0;
  double Y0 = -infty;
  double Y1 = infty;

  Point Pobs;
  double MessRadius;
public:
  DGElasticityAssemble(std::shared_ptr<const DGDiscretization> _disc,
                       ProblemElasticity &_prob, VtuPlot &plot) :
      DGElasticityAssembleMatrix(_disc, _prob, plot) {

    Config::Get("measurement_radius", MessRadius);

    Config::Get("X0", X0);
    Config::Get("X1", X1);
    Config::Get("Y0", Y0);
    Config::Get("Y1", Y1);
    Config::Get("Pobs", Pobs);
  }

  const char *Name() const { return "DGElasticityAssemble"; }

  void PrintInfo(const Vector &u) {
    DGAssemble::PrintInfo(u);
    problem.PrintInfo();
    mout << " tau_r       " << tau_r << endl
         << " W_c         " << W_c << endl
         << " sigma_c     " << sigma_c << endl
         << " s_min       " << s_min << endl;
    int level = 0;
    int deg = 0;
    std::string DataPath;
    Config::Get("level", level);
    Config::Get("deg", deg);
    Config::Get("DataPath", DataPath);

    mout << " Mesh            " << problem.MeshName() << endl
         << " level           " << level << endl
         << " deg             " << deg << endl
         << " DataPath        " << DataPath << endl
         << " Problem         " << problem.Name() << endl << endl;
  }

  std::string DiscName() const { return disc->DiscName(); }

  double Energy(const Vector &u, double x0, double x1, double y0, double y1,
                double &E_kin, double &E_pot) const {
    Vector &pf = *problem.GetPF();
    const Vector &pf2 = *problem.GetPF2();
    E_kin = E_pot = 0;

    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      double x = c()[0];
      if (x < x0) continue;
      if (x > x1) continue;
      double y = c()[1];
      if (y < y0) continue;
      if (y > y1) continue;
      DGElasticityElement elem(*disc, u, c);
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        Point lz = elem.LocalQPoint(q);
        Point z = elem.QPoint(q);
        double rho = problem.Rho(c, lz, z);
        VectorField V = elem.Velocity(q, u);
        Tensor S = elem.Stress(q, u);
        Tensor E = problem.StressStrain(c, lz, z, S);
        E_kin += w * rho * V * V;
        E_pot += w * Frobenius(E, S);
      }
    }
    E_kin = 0.5 * PPM->Sum(E_kin);
    E_pot = 0.5 * PPM->Sum(E_pot);
    return E_kin + E_pot;
  }

  double Energy(const Vector &u) const {
    double k, p;
    return Energy(u, -infty, infty, Y0, Y1, k, p);
  }

  double Energy(const Vector &u, double &E_kin, double &E_pot) const {
    return Energy(u, -infty, infty, Y0, Y1, E_kin, E_pot);
  }

  double dist(const Vector &u, const Vector &v) const {
    Vector w(u);
    w -= v;
    return sqrt(Energy(w));
  }

  void EnergyCorrection(Vector &u) const {
    Consistency(u);
    double e = 0;
    double v_max = 0;
    double v_min = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      DGElasticityElement elem(*disc, u, c);
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        Point z = elem.QPoint(q);
        Point lz = elem.LocalQPoint(q);
        double mu = problem.OldMu(c, lz, z);
        Tensor S = elem.Stress(q, u);
        VectorField V = elem.Velocity(q, u);
        v_max = std::max(v_max, V[0]);
        v_min = std::min(v_min, V[0]);
        e += w * (1 / mu) * Frobenius(S, S);
      }
    }
    v_max = PPM->Max(v_max);
    v_min = PPM->Min(v_min);
    e = PPM->Sum(e);
    mout << "EnergyCorrection before " << e << " "
         << OUT(v_min)
         << OUT(v_max)
         << endl;

    Point z[MaxNodalPointsDG];
    Point lz[MaxNodalPointsDG];
    Vector &pf2 = *problem.GetPF2();
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      DGElasticityElement Elem(*disc, u, c);
      Elem.NodalPoints(z);
      Elem.LocalNodalPoints(lz);
      ScalarElement E(pf2, *c);
      for (int j = 0; j < Elem.shape_size(); ++j) {
        VectorField Dpf = E.Derivative(lz[j], pf2);
        double d = norm(Dpf);
        if (d < 1e-4) continue;
        Tensor S = Elem.GetStress(u, j);
        Tensor E = problem.OldStressStrain(c, lz[j], z[j], S);
        Tensor S2 = problem.StrainStress(c, lz[j], z[j], E);
        Elem.SetStress(S2, u, j);

        continue;

        double Z_p = problem.Z_p(c, lz[j], z[j]);
        Dpf *= (1 / d);
        VectorField V = Elem.GetVelocity(u, j);
        V += (1 / Z_p) * (S - S2) * Dpf;
        Elem.SetVelocity(V, u, j);

        S2 = Elem.GetStress(u, j);
        Point zz = c[lz[j]];

        pout << OUT(z[j][0])
             << OUT(lz[j][0])
             << OUT(Z_p)
             << OUT(Dpf[0])
             << OUT(V[0])
             << OUT(S2[0][0])
             << endl;
      }
    }

    e = 0;
    v_max = 0;
    v_min = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      DGElasticityElement elem(*disc, u, c);
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        Point z = elem.QPoint(q);
        Point lz = elem.LocalQPoint(q);
        double mu = problem.Mu(c, lz, z);
        Tensor S = elem.Stress(q, u);
        VectorField V = elem.Velocity(q, u);
        v_max = std::max(v_max, V[0]);
        v_min = std::min(v_min, V[0]);
        e += w * (1 / mu) * Frobenius(S, S);
      }
    }
    v_max = PPM->Max(v_max);
    v_min = PPM->Min(v_min);
    e = PPM->Sum(e);
    mout << "EnergyCorrection after  " << e << " "
         << OUT(v_min)
         << OUT(v_max)
         << endl;
    Consistency(u);
  }

  void Initialize(Vector &u) const { u = 0; }

  void SetInitialValue(double t, Vector &u) {
    problem.get_u_int(t, u);
    double factor = 1;
    Config::Get("RescaleEnergy", factor);
    if (factor == 0) EE = 1;
    else {
      EE = sqrt(Energy(u));
      mout << "energy before rescaling " << EE << endl;
      EE *= factor;
    }
    if (abs(EE) > 1e-8) {
      mout << "no energy scaling" << endl;
      EE = 1;
    } else u *= (1 / EE);
    if (problem.UsePF()) {
      Vector &U = *problem.GetU();
      for (row r = U.rows(); r != U.rows_end(); ++r) {
        U(r, 0) = problem.U0(t, r(), 0);
        U(r, 1) = problem.U0(t, r(), 1);
      }
      U *= scale_u / EE;
    }
  }

  void PrintStep(double t, Vector &u, int step) {
    Vector u_tmp(u);
    double E_kin, E_pot;
    Energy(u, E_kin, E_pot);
    mout << "Step " << step
         << " (" << _pstep << ")"
         << "\t t=" << t * 1.000000000000001
         << "\t E=" << E_kin + E_pot << "=" << E_kin << "+" << E_pot;

    if (problem.Exact()) {
      problem.get_u_int(t, u_tmp);
      u_tmp *= (1 / EE);
      u_tmp -= u;
      mout << " Err " << u_tmp.norm();
    }
    u_tmp = 0;

    if (problem.GetPF() == 0) return;
    double Vx_min = infty;
    double Vx_max = -infty;
    double Vy_min = infty;
    double Vy_max = -infty;
    double V_max = -infty;
    double S_max = -infty;
    double d = infty;
    VectorField V_eval = zero;
    Tensor S_eval = Zero;
    double pv_max = 0;
    Vector &pf2 = *problem.GetPF2();
    for (cell c = u_tmp.cells(); c != u_tmp.cells_end(); ++c) {
      row r = u.find_row(c());
      DGElasticityElement elem(*disc, u_tmp, c);
      ScalarElement E(pf2, *c);
      VectorField V = zero;
      Tensor S = Zero;
      double a = 0;
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        Point z = elem.QPoint(q);
        VectorField Vq = elem.Velocity(q, u);
        Tensor Sq = elem.Stress(q, u);
        double pv = max_pv(Sq);
        pv_max = std::max(pv_max, pv);

        double sq = norm(Sq);
        if (sq < -1e-5)
          mout << "Sq " << sq << " " << Sq[0][1]
               << " pv " << pv
               << " pv_max " << pv_max
               << " q " << q << " z " << z[0] << " " << z[1] << " c " << c()[0] << " "
               << c()[1] << endl;

        Vx_min = std::min(Vx_min, Vq[0]);
        Vx_max = std::max(Vx_max, Vq[0]);
        Vy_min = std::min(Vy_min, Vq[1]);
        Vy_max = std::max(Vy_max, Vq[1]);
        V += w * Vq;
        S += w * Sq;
        a += w;
      }
      V *= (1 / a);
      S *= (1 / a);
      u_tmp(r)[0] = V[0];
      u_tmp(r)[1] = V[1];
      if (c()[0] > 0) {
        V_max = std::max(norm(V), V_max);
        S_max = std::max(norm(S), S_max);
      }
      double d_c = norm(z_eval - c());
      if (d_c < d) {
        d = d_c;
        S_eval = S;
        V_eval = V;
      }
    }
    Vx_min = PPM->Min(Vx_min);
    Vx_max = PPM->Max(Vx_max);
    Vy_min = PPM->Min(Vy_min);
    Vy_max = PPM->Max(Vy_max);
    V_max = PPM->Max(V_max);
    S_max = PPM->Max(S_max);
    pv_max = PPM->Max(pv_max);
    if (abs(pv_max) < 1e-20) pv_max = 0;
    mout << "\t pv_max " << pv_max;
    if ((V_max > 0) && print_V)
      mout << " |V|<" << V_max
           << " |S|<" << S_max;
    mout << endl;

    Consistency(u);

    double d_min = PPM->Min(d);
    if (d != d_min) {
      V_eval = zero;
      S_eval = Zero;
    }
    V_eval[0] = PPM->Sum(V_eval[0]);
    V_eval[1] = PPM->Sum(V_eval[1]);
    S_eval[0][0] = PPM->Sum(S_eval[0][0]);
    S_eval[1][1] = PPM->Sum(S_eval[1][1]);
    S_eval[0][1] = PPM->Sum(S_eval[0][1]);

    return;

    if (z_eval != Infty) return;

    mout << "Evaluation at" << z_eval
         << " t=" << t
         << " V=" << V_eval[0] << " " << V_eval[1]
         << " S=" << S_eval[0][0] << " " << S_eval[1][1] << " " << S_eval[0][1]
         << endl;
  }

  void InitialStep(double t, Vector &u) {
    U = &u;
    if (problem.InitialValues())
      SetInitialValue(t, u);
    else u = 0;
    PrintStep(t, u, 0);
    VtkPlotting(t, u, 0);
  }

  void FinishStep(Vector &u, double t, double dt,
                  int step, int plot_step = 1) {
    if ((step % plot_step == 0) || (_inelastic && plot_inelastic)) {
      PrintStep(t, u, step);
      VtkPlotting(t, u, step);
    }
    if (false) {
      for (cell c = u.cells(); c != u.cells_end(); ++c) {
        row r = u.find_row(c());
        if (c.dim() == 2) {
          double lambda2;
          Point Pa = c.Corner(1) - c.Corner(0);
          Point Pb = c.Corner(2) - c.Corner(0);
          Point Pc = Pobs - c.Corner(0);
          if (Pa[0] == 0) {
            lambda2 = Pc[0] / Pb[0];
          } else {
            lambda2 = (Pc[0] / Pa[0] - Pc[1] / Pa[1]) / (Pb[0] / Pa[0] - Pb[1] / Pa[1]);
          }
          if (-1.0e-9 < lambda2 && lambda2 < 1 + 1.0e-9) {
            double lambda1 = (Pc[1] - lambda2 * Pb[1]) / Pa[1];
            if (-1.0e-9 < lambda1 && lambda1 < 1 + 1.0e-9) {
              if (lambda1 + lambda2 < 1 + 1.0e-9) {
                int shift = 0;
                int NN = 5;
              }
            }
          }
        }
      }
    }
  }

  void VtkPlotting(double t, const Vector &u, int step) {
    _step = step;
    vout(2) << "  => VtkPlotting result of step " << step << ".\n";
    char filename[128];
    auto disc_dim1 = std::make_shared<DGDiscretization>(u.GetMeshes(), 0, 1);
    auto disc_dim2 = std::make_shared<DGDiscretization>(u.GetMeshes(), 0, 2);
    Vector V_vec(disc_dim2, u.Level());
    Vector traceS(disc_dim1, u.Level());
    Vector normDevS(disc_dim1, u.Level());
    Vector projectedPhaseField(disc_dim1, u.Level());
    Vector S00(disc_dim1, u.Level());
    Vector minPV_S(disc_dim1, u.Level());
    Vector maxPV_S(disc_dim1, u.Level());
    Vector problem_phasefield(disc_dim1, u.Level());
    Vector phaseField(disc_dim1, u.Level());
    Vector rhs_vec(disc_dim1, u.Level());
    Vector material(u);

    Vector traceS_int(disc_dim1, u.Level());
    Vector V_int_vec(disc_dim1, u.Level());

    Vector u_int(u);
    Vector rhs(u);
    double c_S_min = infty;
    double c_S_max = -infty;
    double c_P_min = infty;
    double c_P_max = -infty;
    double rho_min = infty;
    double rho_max = -infty;
    double mu_min = infty;
    double mu_max = -infty;
    double lambda_min = infty;
    double lambda_max = -infty;
    double pf_diff = 0;
    double pf_diff2 = 0;
    double pf_diff4 = 0;
    if (problem.Exact()) {
      problem.get_u_int(t, u_int);
      u_int *= (1 / EE);
      RHS(t, rhs);
    } else {
      u_int = 0;
      rhs = 0;
    }
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      DGElasticityElement elem(*disc, V_vec, c);
      VectorField V = zero;
      VectorField V_int = zero;
      Tensor S_int = Zero;
      VectorField S_div = zero;
      Tensor S = Zero;
      double a = 0.0;
      for (int q = 0; q < elem.nQ(); ++q) {
        double w = elem.QWeight(q);
        S_div += w * elem.divStress(q, u);
        V += w * elem.Velocity(q, u);
        V_int += w * elem.Velocity(q, u_int);
        S += w * elem.Stress(q, u);
        S_int += w * elem.Stress(q, u_int);
        a += w;
      }
      V *= (1 / a);
      V_int *= (1 / a);
      S *= (1 / a);
      S_int *= (1 / a);
      S_div *= (1.0 / a);
      row r = u.find_row(c());
      V_vec(r)[0] = V[0];
      V_vec(r)[1] = V[1];
      traceS(r)[0] = trace(S);
      normDevS(r)[0] = norm(dev(S));
      projectedPhaseField(r)[0] = problem.ProjectedPhasefield(c, c.LocalCenter(), c());
      S00(r)[0] = S[0][0];
      maxPV_S(r)[0] = max_pv(S);
      minPV_S(r)[0] = min_pv(S);
      phaseField(r)[0] = problem.Phasefield(c, c.LocalCenter(), c());
      if (problem.Exact()) {
        double x = 0;
        for (int i = 0; i < r.n(); ++i) {
          x += rhs(r, i) * rhs(r, i);
        }
        rhs_vec(r)[0] = sqrt(x);
        traceS_int(r)[0] = trace(S_int);
        V_int_vec(r)[0] = norm(V_int);
      }
      material(r)[0] = problem.Lambda(c, c.LocalCenter(), c());
      material(r)[1] = problem.Mu(c, c.LocalCenter(), c());
      material(r)[2] = problem.Rho(c, c.LocalCenter(), c());
      problem_phasefield(r)[0] = problem.Phasefield(c, c.LocalCenter(), c());
      if (t == 0) {
        for (int q = 0; q < elem.nQ(); ++q) {
          Point lz = elem.LocalQPoint(q);
          Point z = elem.QPoint(q);
          double c_S = problem.C_s(c, lz, z);
          c_S_min = std::min(c_S_min, c_S);
          c_S_max = std::max(c_S_max, c_S);
          double c_P = problem.C_p(c, lz, z);
          c_P_min = std::min(c_P_min, c_P);
          c_P_max = std::max(c_P_max, c_P);
          double rho = problem.Rho(c, lz, z);
          rho_min = std::min(rho_min, rho);
          rho_max = std::max(rho_max, rho);
          double mu = problem.Mu(c, lz, z);
          mu_min = std::min(mu_min, mu);
          mu_max = std::max(mu_max, mu);
          double lambda = problem.Lambda(c, lz, z);
          lambda_min = std::min(lambda_min, lambda);
          lambda_max = std::max(lambda_max, lambda);
          double p_f = problem.PhasefieldDifference(c, lz, z);
          pf_diff += elem.QWeight(q) * p_f;
          pf_diff2 += elem.QWeight(q) * p_f * p_f;
          pf_diff4 += elem.QWeight(q) * p_f * p_f * p_f * p_f;
        }
      }
    }
    Vector *U = problem.GetU();
    if (problem.UsePF() || U != nullptr) {
      plot.AddData("problem_U", *U, {0, 1, 2});

    }
    plot.AddData("trace_S", traceS, 0);
    plot.AddData("S00", S00, 0);
    plot.AddData("S_min", minPV_S, 0);
    plot.AddData("S_max", maxPV_S, 0);


    NumberName("NumericalSolution", filename, _step);
    plot.PlotFile(filename);

    if (t == 0) {
      VtuPlot material_plot;
      material_plot.AddData("Lambda", material, 0);
      material_plot.AddData("Mu", material, 1);
      material_plot.AddData("Rho", material, 2);
      material_plot.PlotFile("Material");
    }
    VtuPlot exactPlot;
    if (problem.Exact()) {
      exactPlot.AddData("rhs", rhs_vec, 0);
      exactPlot.AddData("trace_S_int", traceS_int, 0);
      exactPlot.AddData("norm_V_int", V_int_vec, 0);
      if (t <= 0) {
        exactPlot.AddData("ProblemPhaseField", problem_phasefield, 0);
      }
      NumberName("ExactData", filename, _step);
      exactPlot.PlotFile("ExactData");
    }

    ++_pstep;
    if (t > 0) return;

    c_S_min = PPM->Min(c_S_min);
    c_S_max = PPM->Max(c_S_max);
    c_P_min = PPM->Min(c_P_min);
    c_P_max = PPM->Max(c_P_max);
    rho_min = PPM->Min(rho_min);
    rho_max = PPM->Max(rho_max);
    mu_min = PPM->Min(mu_min);
    mu_max = PPM->Max(mu_max);
    lambda_min = PPM->Min(lambda_min);
    lambda_max = PPM->Max(lambda_max);
    pf_diff = PPM->Sum(pf_diff);
    pf_diff2 = sqrt(PPM->Sum(pf_diff2));
    pf_diff4 = sqrt(sqrt(PPM->Sum(pf_diff4)));
    mout << "c_S in [" << c_S_min << "," << c_S_max << "]" << endl;
    mout << "c_P in [" << c_P_min << "," << c_P_max << "]" << endl;
    mout << "rho in [" << rho_min << "," << rho_max << "]" << endl;
    mout << "mu in [" << mu_min << "," << mu_max << "]" << endl;
    mout << "lambda in [" << lambda_min << "," << lambda_max << "]" << endl;
    mout << "|pf|_1 = " << pf_diff << endl;
    mout << "|pf|_2 = " << pf_diff2 << endl;
    mout << "|pf|_4 = " << pf_diff4 << endl;
  }

  void PrintMatrixInfo(Matrix &A, int diagonal = 1) {
    for (cell c = A.cells(); c != A.cells_end(); ++c) {
      DGElasticityElement elem(*disc, A, c);
      RowEntries_dG A_c(A, c, c);
      for (int i = 0; i < elem.u_dimension(); ++i) {
        for (int j = 0; j < elem.u_dimension(); ++j) {
          mout << A_c(i, j) << " ";
        }
        mout << endl;
      }
      mout << endl;
      if (diagonal) continue;

      for (int f = 0; f < c.Faces(); ++f) {
        cell cf = c;
        if (check_if_bnd(A, c, f))
          continue;
        else
          cf = A.find_neighbour_cell(c, f);
        RowEntries_dG A_cf(A, c, cf);
        for (int i = 0; i < elem.u_dimension(); ++i) {
          for (int j = 0; j < elem.u_dimension(); ++j) {
            mout << A_cf(i, j) << " ";
          }
          mout << endl;
        }
        mout << endl;
      }
      mout << "------------------------" << endl;
    }
  }

  void SetMat(double s) {
    problem.SetMat(s);
  }

  void SetMat1() {
    problem.SetMat1();
  }

  void SetMat2() {
    problem.SetMat1();
  }

  void SetMat3() {
    problem.SetMat3();
  }
};

void MeshBar0(int N = 8);

class ArcGeometry2D : public CoarseGeometry {
  double angle = 0;
  double height = 0;
  double height2 = 0;
public:
  ArcGeometry2D(const std::string &geometryFilename)
      : CoarseGeometry(geometryFilename) {
    Config::Get("angle", angle);
    Config::Get("height", height);
    Config::Get("height2", height2);
    angle *= std::numbers::pi / 180;
  }

  bool HasMapGeometry() const override {
    return true;
  }

  Point operator()(const Point &X) const override {
    double x = X[0];
    double y = X[1];
    double z = X[2];
    x -= 0.5;
    y *= height * (1 + height2 * x * (1 - x));
    return Point(x + 0.5, cos(angle * x)) + y * Point(sin(angle * x), cos(angle * x));
  }
};

class ArcGeometry3D : public CoarseGeometry {
  double angle = 0;
  double angle2 = 0;
  double scale = 1;
public:
  explicit ArcGeometry3D(const std::string &geometryFilename)
      : CoarseGeometry(geometryFilename) {
    Config::Get("angle", angle);
    Config::Get("angle2", angle2);
    Config::Get("scaleGeometry", scale);
    angle *= std::numbers::pi / 180;
    angle2 *= std::numbers::pi / 180;
  }

  bool HasMapGeometry() const override {
    return true;
  }

  Point operator()(const Point &X) const override {
    double x = X[0];
    double y = X[1];
    double z = X[2];
    x -= 0.5;
    return scale * (Point(x + 0.5, cos(angle * x), cos(angle2 * x))
                    + y * Point(sin(angle * x), cos(angle * x))
                    + z * Point(sin(angle2 * x), 0, cos(angle2 * x)));
  }
};

double MaindG_Elasticity() {
  Config::PrintInfo();

  int N_bar = 0;
  Config::Get("N_bar", N_bar);
  if (N_bar) {
    MeshBar0(N_bar);
    return 0.0;
  }

  std::string meshName;
  Config::Get("Mesh", meshName);

  Meshes *M = MeshesCreator().
      WithCoarseGeometry(std::make_shared<ArcGeometry3D>(meshName)).
      Create();

  M->PrintInfo();

  int dim = M->dim();
  int Scales = 1;
  double ScaleFactor = 0.5;

  bool adaptive = false;
  Config::Get("Scales", Scales);
  Config::Get("ScaleFactor", ScaleFactor);
  Config::Get("adaptive", adaptive);
  double finaleTime = 1;
  double ProblemExact = 0;
  Config::Get("T", finaleTime);
  Config::Get("ProblemExact", ProblemExact);
  int cell_deg = 0;
  Config::Get("deg", cell_deg);

  auto disc_dg = std::make_shared<DGDiscretization>(*M, cell_deg, CellDoFs(*M, cell_deg));
  Vector u(disc_dg);
  u = 1;

  std::string problem_name = "Hat2D";
  Config::Get("Problem", problem_name);
  ProblemElasticity *problem = GetProblemElasticity(disc_dg, problem_name);


  VtuPlot plot;
  DGElasticityAssemble assemble(disc_dg, *problem, plot);
  assemble.PrintInfo(u);

  int L = 0;
  std::vector<Vector *> UU((L + 1) * Scales);
  std::vector<Vector *> Uexact((L + 1) * Scales);
  std::vector<int> S_Sum((L + 1) * Scales);
  std::vector<int> M_Sum((L + 1) * Scales);
  std::vector<int> K_Sum((L + 1) * Scales);
  std::vector<int> K_Pre((L + 1) * Scales);
  RVector GF(0.0, L + 1);
  RVector L2E(0.0, L + 1);
  RVector L2I(0.0, L + 1);
  std::vector<int> NDoFsSpace(L + 1);
  std::vector<int> NTSteps(L + 1);
  std::vector<int> NDoFs(L + 1);
  double s = 1;
  double energy = 0.0;
  for (int level = 0; level <= L; ++level) {
    mout << endl << "Level " << level << endl;
    Vector U(disc_dg);
    U = 0;
    U.ClearDirichletFlags();
    Vector u_1(disc_dg);
    u_1.ClearDirichletFlags();
    u_1 = 0;
    problem->SetPF(*M);
    Preconditioner *dGPC = GetPC();
    LinearSolver *S = GetLinearSolver(dGPC);
    Preconditioner *dGPC2 = GetPC("PointBlockJacobi");
    LinearSolver *S2 = GetLinearSolver(dGPC2);
    Preconditioner *dGPC3 = GetPC("PointBlockJacobi");
    LinearSolver *S3 = GetLinearSolver(dGPC3);
    Preconditioner *BJ = GetPC("PointBlockJacobi");
    LinearSolver *IM = GetLinearSolver(BJ);
    DG_TimeIntegrator dgrk(*S, *S2, *S3, *IM);

    if (Scales > 1) s = 1;
    for (int i = 0; i < Scales; ++i) {
      TimeSeries TS;

      NTSteps[level] = int(finaleTime / TS.SecondTStep());
      Date Start;
      U = 0;
      dgrk(assemble, TS, U);
      mout << "steps " << dgrk.K_Sum()
           << " in " << Date() - Start << endl;
      S_Sum[level * Scales + i] = dgrk.S_Sum();
      M_Sum[level * Scales + i] = dgrk.M_Sum();
      K_Sum[level * Scales + i] = dgrk.K_Sum();
      K_Pre[level * Scales + i] = dgrk.K_Pre();
      UU[level * Scales + i] = new Vector(U);
      s *= ScaleFactor;
    }
    NDoFsSpace[level] = U.pSize();
    NDoFs[level] = NDoFsSpace[level] * NTSteps[level];
    energy = assemble.Energy(U);
  }
  mout << endl;
  mout << "GoalFunctional E(U) = " << energy << endl;
  mout << "space DoFs           " << NDoFsSpace;
  mout << "time steps           " << NTSteps;
  mout << "space-time DoFs      " << NDoFs;

  if (problem->Exact()) {
    PrintValues("L2 Error             ", L2E);
    PrintValues("L2 Interpolation     ", L2I);
  }
  return energy;
}

