#ifndef _DGTIMEINTEGRATOR_H_
#define _DGTIMEINTEGRATOR_H_

#include "TimeDate.hpp"
#include "TimeSeries.hpp"
#include "LinearSolver.hpp"


const double alpha = sqrt(3) / 6.0;

class DG_TimeIntegrator {
  LinearSolver &S;
  LinearSolver &S2;
  LinearSolver &S3;
  LinearSolver &IM;
  int plot_tStep;
  int rkorder;
  int Kmin;
  int Kmax;
  double Keps;
  double gamma;
  double gamma0;
  int SSum;
  int MSum;
  int KSum;
  int KPre;
  Time TPre;
  Time TComp;
  int verbose;
  double theta_ex;
  bool EnergyCorrection;
  int repeat_step = 1;
  static constexpr double Eps = 1e-10;

public:
  DG_TimeIntegrator(LinearSolver &_S,
                    LinearSolver &_S2,
                    LinearSolver &_S3,
                    LinearSolver &_IM) :
    S(_S), S2(_S2), S3(_S3), IM(_IM), plot_tStep(1), rkorder(1),
    Kmin(2), Kmax(10), Keps(0.001), theta_ex(0), EnergyCorrection(true),
    SSum(0), MSum(0), KSum(0), KPre(0), gamma(1), verbose(0) {
    Config::Get("plot_tStep", plot_tStep);
    Config::Get("rkorder", rkorder);
    if ((rkorder == 0) || (rkorder == 1000)) {
      Config::Get("Kmin", Kmin);
      Config::Get("Kmax", Kmax);
      Config::Get("Keps", Keps);
    } else if (rkorder == -22)
      Config::Get("theta_ex", theta_ex);
    else if (rkorder == -4)
      Config::Get("Keps", Keps);
    Config::Get("TimeVerbose", verbose);
    double g = gamma;
    Config::Get("gamma", g);
    Config::Get("repeat_step", repeat_step);
    gamma0 = gamma = g;
    Config::Get("EnergyCorrection", EnergyCorrection);
  }

  void Euler(double dt, const Vector &u, const Matrix &A,
             Vector &du, const Vector &RHS) {
    Vector Au(u);
    Au = A * u;
    Au += RHS;
    du = IM * Au;
    du *= dt;
    MSum += 1;
  }

  void Heun(double dt, const Vector &u, const Matrix &A,
            Vector &du, const Vector &RHS, const Vector &RHS1) {
    Vector u1(u);
    Vector u2(u);
    Vector Au(u);
    Au = A * u;
    Au += RHS;
    u1 = IM * Au;
    u2 = dt * u1;
    u2 += u;
    Au = A * u2;
    Au += RHS1;
    u2 = IM * Au;
    u2 += u1;
    du = (0.5 * dt) * u2;
    MSum += 2;
  }

  void Runge(double dt, const Vector &u, const Matrix &A, Vector &du) {
    const double w3 = 1.0 / 6.0;
    Vector u1(u);
    Vector u2(u);
    Vector u3(u);
    Vector Au(u);
    Au = A * u;
    u1 = IM * Au;

    u2 = 0.5 * dt * u1;
    u2 += u;
    Au = A * u2;
    u2 = IM * Au;

    u3 = 2.0 * dt * u2;
    u3 -= dt * u1;
    u3 += u;
    Au = A * u3;
    u3 = IM * Au;

    u3 += 4.0 * u2;
    u3 += u1;

    du = (w3 * dt) * u3;
    MSum += 3;
  }

  void Kutta(double dt, const Vector &u, const Matrix &A, Vector &du) {
    const double w4 = 1.0 / 6.0;
    Vector u1(u);
    Vector u2(u);
    Vector u3(u);
    Vector u4(u);
    Vector Au(u);

    Au = A * u;
    u1 = IM * Au;

    u2 = 0.5 * dt * u1;
    u2 += u;
    Au = A * u2;
    u2 = IM * Au;

    u3 = 0.5 * dt * u2;
    u3 += u;
    Au = A * u3;
    u3 = IM * Au;

    u4 = dt * u3;
    u4 += u;
    Au = A * u4;
    u4 = IM * Au;

    u4 += 2.0 * u3;
    u4 += 2.0 * u2;
    u4 += u1;
    du = (w4 * dt) * u4;

    MSum += 4;
  }

  void Kutta(double dt, const Vector &u, const Matrix &A, Vector &du,
             const Vector &RHS1, const Vector &RHS2, const Vector &RHS3) {
    const double w4 = 1.0 / 6.0;
    Vector u1(u);
    Vector u2(u);
    Vector u3(u);
    Vector u4(u);
    Vector Au(u);

    Au = A * u;
    Au += RHS1;
    u1 = IM * Au;

    u2 = 0.5 * dt * u1;
    u2 += u;
    Au = A * u2;
    Au += RHS2;
    u2 = IM * Au;

    u3 = 0.5 * dt * u2;
    u3 += u;
    Au = A * u3;
    Au += RHS2;
    u3 = IM * Au;

    u4 = dt * u3;
    u4 += u;
    Au = A * u4;
    Au += RHS3;
    u4 = IM * Au;

    u4 += 2.0 * u3;
    u4 += 2.0 * u2;
    u4 += u1;
    du = (w4 * dt) * u4;

    MSum += 4;
  }

  void Pade2(double dt, const Vector &u, const Matrix &A, Vector &du) {
    Vector Au(u);
    Au = A * u;
    du = S * Au;
    // KPre += S.Iter();
    du *= dt;
    ++KSum;
  }

  void Pade2(double dt, const Vector &u, const Matrix &A, const Matrix &B,
             Vector &du, const Vector &RHS) {
    Vector Au(u);
    Au = A * u;
    Au += RHS;
    du *= (1 / dt);
    Au -= B * du;
    du += S * Au;
    // KPre += S.Iter();
    du *= dt;
    ++KSum;
  }

  void Pade4(double dt, const Vector &u, const Matrix &A, const Matrix &B,
             Vector &du, const Vector &RHS, const Vector &RHS2) {
    Vector r1(u);
    r1 = A * u;
    Vector r2(r1);
    r1 += RHS;
    r2 += RHS2;
    Vector k1(du);
    k1 = S * r1;
    // KPre += S.Iter();
    du = (0.5 * dt) * k1;
    Vector k2(k1);
    Vector r22(r2);
    r22 += A * du;
    r22 -= B * k2;
    k2 += S * r22;
    // KPre += S.Iter();
    du += (0.5 * dt) * k2;
    for (int i = 0; i < 20; ++i) {
      r1 -= B * k1;
      r2 -= B * k2;
      k1 *= ((0.25 + alpha) * dt);
      k2 *= ((0.25 - alpha) * dt);
      r1 += A * k2;
      r2 += A * k1;
      k1 = S * r1;
      // KPre += S.Iter();
      du += (0.5 * dt) * k1;
      k2 = S * r2;
      // KPre += S.Iter();
      du += (0.5 * dt) * k2;
      if ((k1.norm() + k2.norm()) < Keps * (u.norm() + RHS.norm())) break;
    }
    ++KSum;
  }

  void Pade2ex(double dt, const Vector &u, const Matrix &A,
               const Matrix &B, const Matrix &B2, Vector &du,
               const Vector &RHS, const Vector &RHS2, const Vector &RHS3) {
    Vector Au(u);
    Au = A * u;
    Vector u2(u);
    Vector Au2(Au);
    Vector du0(du);
    Au += RHS;
    du0 *= (1 / dt);
    Au -= B * du0;
    du0 += S * Au;
    // KPre += S.Iter();
    du0 *= dt;
    ++KSum;

    Au2 += RHS2;
    Vector du2(du0);
    du2 *= (1 / dt);
    Au2 -= B2 * du2;
    du2 += S2 * Au2;
    du2 *= (0.5 * dt);
    u2 += du2;
    Au2 = A * u2;
    Au2 += RHS3;
    du = du2;
    du *= (2 / dt);
    Au2 -= B2 * du;
    du += S2 * Au2;
    du *= (0.5 * dt);
    du += du2;

    du *= (4.0 / 3.0);
    du -= (1.0 / 3.0) * du0;
  }

  void Pade3(double dt, const Vector &u,
             const Matrix &M, const Matrix &A, Vector &du) {
    Vector Au(u);
    Vector Mu(u);
    Au = A * u;
    du = S * Au;
    // KPre += S.Iter();
    Mu = M * du;
    Au = A * du;
    Au *= (dt * -1 / sqrt(60));
    Mu -= Au;
    du = S2 * Mu;
    // KPre += S2.Iter();
    Mu = M * du;
    Au = A * du;
    Au *= (dt * -1 / sqrt(60));
    Mu += Au;
    du = S3 * Mu;
    // KPre += S3.Iter();
    du *= dt;
    KSum += 3;
  }

  void Pade3(double dt, const Vector &u,
             const Matrix &M, const Matrix &A, Vector &du,
             const Vector &RHS, const Vector &RHS2, const Vector &RHS3) {
    Vector Au(u);
    Vector T(u);
    Vector MT(u);
    Vector AT(u);

    Vector T1(u);
    Vector T2(u);
    Vector T11(u);
    Vector T22(u);

    Au = A * u;

    T = Au;
    T += 5. / 18. * RHS;
    T += 8. / 18. * RHS2;
    T += 5. / 18. * RHS3;
    T1 = S * T;

    T = Au;
    T += 5. / 6. * RHS3;
    T2 = S * T;

    MT = M * T1;
    AT = A * T2;
    AT *= (-dt * -1 / sqrt(60));

    MT += AT;
    T11 = S2 * MT;

    //  ------------------------

    T = Au;
    T += 5. / 6. * RHS;
    T1 = S * T;

    T = Au;
    T += 5. / 6. * RHS;
    T -= 4. / 6. * RHS2;
    T += 5. / 6. * RHS3;
    T2 = S * T;

    MT = M * T1;
    AT = A * T2;
    AT *= (-dt * -1 / sqrt(60));

    MT += AT;
    T22 = S2 * MT;

    //  ------------------------

    MT = M * T11;
    AT = A * T22;
    AT *= (dt * -1 / sqrt(60));

    MT += AT;

    du = S3 * MT;

    // KPre += 4 * S.Iter();
    // KPre += 2 * S2.Iter();
    // KPre += S3.Iter();
    du *= dt;
    KSum += 7;
  }

  void ExponentialIntegrator(double dt, const Vector &u,
                             const Matrix &M, const Matrix &A, Vector &du) {
    std::vector<Vector> v(Kmax + 1, u);
    std::vector<Vector> w(Kmax + 1, u);
    Vector Sw(u);
    RMatrix h(Kmax + 1, Kmax);
    RVector s(Kmax);
    RVector s_old(Kmax);
    s_old = double(0.0);
    h = double(0.0);
    v[0] = u;
    w[0] = M * u;
    double beta = sqrt(std::real(v[0] * w[0]));
    v[0] *= 1 / beta;
    w[0] *= 1 / beta;
    s_old[0] = beta;
    S.SetReduction(Keps);
    int k = 0;
    for (; k < Kmax; ++k) {
      Sw = S * w[k];
      // KPre += S.Iter();
      Sw *= gamma / dt;
      v[k + 1] = Sw;
      for (int j = 0; j <= k; ++j) {
        h[j][k] = Sw * w[j];
        v[k + 1] -= h[j][k] * v[j];
      }
      w[k + 1] = M * v[k + 1];
      h[k + 1][k] = sqrt(std::real(v[k + 1] * w[k + 1]));
      v[k + 1] *= (1.0 / h[k + 1][k]);
      w[k + 1] *= (1.0 / h[k + 1][k]);
      RMatrix S_k(k + 1, k + 1);
      for (int j = 0; j <= k; ++j)
        for (int l = 0; l <= k; ++l)
          S_k[j][l] = h[j][l];
      S_k.Invert();
      for (int j = 0; j <= k; ++j)
        S_k[j][j] -= dt / gamma;
      S_k *= double(-1.0);
      S_k.Exp();
      for (int j = 0; j <= k; ++j)
        s[j] = beta * S_k[j][0];
      //	    mout << "S_k " << s << endl << S_k;
      //	    mout << "S_k " << s << endl;
      for (int j = 0; j <= k; ++j)
        s_old[j] -= s[j];
      double norm_u = s.norm();
      double delta = s_old.norm() / norm_u;
      for (int j = 0; j <= k; ++j)
        s_old[j] = s[j];
      if (k == 0) continue;
      double eta = 1 + norm_u;
      if (delta < 1)
        eta = std::min(eta, delta * norm_u / (1 - delta));
      vout(2) << "eta(" << k << ") = " << eta << endl;
      S.SetReduction(0.1 * Keps / (Keps + eta));
      if (eta < Keps) {
        KSum += k;
        break;
      }
    }
    du = 0.0;
    for (int j = 0; j <= k; ++j)
      du += s[j] * v[j];
    //	s *= (1/beta);
    //	mout << "s " << s << endl;
    du -= u;
    if (k == Kmax) du[0] = 2e10;
  }

  void Arnoldi(double dt, const Vector &u,
               const Matrix &M, const Matrix &A, Vector &du) {
    std::vector<Vector> v(Kmax + 1, u);
    Vector Mv(u);
    Vector Av(u);
    RMatrix h(Kmax + 1, Kmax);
    RVector s(Kmax);
    RVector s_old(Kmax);
    s_old = double(0.0);
    h = double(0.0);
    v[0] = u;
    Mv = M * v[0];
    double beta = sqrt(std::real(v[0] * Mv));
    if (beta < Eps) {
      du = 0;
      return;
    }
    v[0] *= 1 / beta;
    s_old[0] = beta;
    int k = 0;
    for (; k < Kmax; ++k) {
      Av = A * v[k];
      v[k + 1] = IM * Av;
      for (int j = 0; j <= k; ++j) {
        h[j][k] = Av * v[j];
        v[k + 1] -= h[j][k] * v[j];
      }
      Mv = M * v[k + 1];
      h[k + 1][k] = sqrt(std::real(v[k + 1] * Mv));
      v[k + 1] *= (1.0 / h[k + 1][k]);
      RMatrix H_k(k + 1, k + 1);
      for (int j = 0; j <= k; ++j)
        for (int l = 0; l <= k; ++l)
          H_k[j][l] = h[j][l] * dt;
      H_k.Exp();
      for (int j = 0; j <= k; ++j)
        s[j] = beta * H_k[j][0];
      for (int j = 0; j <= k; ++j)
        s_old[j] -= s[j];
      double norm_u = s.norm();
      double delta = s_old.norm() / norm_u;
      for (int j = 0; j <= k; ++j)
        s_old[j] = s[j];
      if (k == 0) continue;
      double eta = 1 + norm_u;
      if (delta < 1)
        eta = std::min(eta, delta * norm_u / (1 - delta));
      vout(2) << "eta(" << k << ") = " << eta << endl;
      if (eta < Keps) break;
    }
    du = 0.0;
    for (int j = 0; j <= k; ++j)
      du += s[j] * v[j];
    MSum += k + 1;
    du -= u;
    if (k == Kmax) du[0] = 2e10;
  }

  void Arnoldi2(double dt, const Vector &u,
                const Matrix &M, const Matrix &A, Vector &du) {
    std::vector<Vector> v(Kmax + 1, u);
    std::vector<Vector> w(Kmax + 1, u);
    Vector Sw(u);
    RMatrix h(Kmax + 1, Kmax);
    RVector s(Kmax);
    RVector s_old(Kmax);
    s_old = double(0.0);
    h = double(0.0);
    v[0] = u;
    w[0] = M * u;
    double beta = sqrt(std::real(v[0] * w[0]));
    v[0] *= 1 / beta;
    w[0] *= 1 / beta;
    s_old[0] = beta;
    S.SetReduction(Keps);
    int k = 0;
    for (; k < Kmax; ++k) {
      Sw = S * w[k];
      // KPre += S.Iter();
      Sw *= gamma / dt;
      v[k + 1] = Sw;
      for (int j = 0; j <= k; ++j) {
        h[j][k] = Sw * w[j];
        v[k + 1] -= h[j][k] * v[j];
      }
      w[k + 1] = M * v[k + 1];
      h[k + 1][k] = sqrt(std::real(v[k + 1] * w[k + 1]));
      v[k + 1] *= (1.0 / h[k + 1][k]);
      w[k + 1] *= (1.0 / h[k + 1][k]);
      RMatrix S_k(k + 1, k + 1);
      for (int j = 0; j <= k; ++j)
        for (int l = 0; l <= k; ++l)
          S_k[j][l] = h[j][l];
      S_k.Invert();
      for (int j = 0; j <= k; ++j)
        S_k[j][j] -= dt / gamma;
      S_k *= double(-1.0);
      S_k.Exp();
      for (int j = 0; j <= k; ++j)
        s[j] = beta * S_k[j][0];
      //	    mout << "S_k " << s << endl << S_k;
      //	    mout << "S_k " << s << endl;
      for (int j = 0; j <= k; ++j)
        s_old[j] -= s[j];
      double norm_u = s.norm();
      double delta = s_old.norm() / norm_u;
      for (int j = 0; j <= k; ++j)
        s_old[j] = s[j];
      if (k == 0) continue;
      double eta = 1 + norm_u;
      if (delta < 1)
        eta = std::min(eta, delta * norm_u / (1 - delta));
      vout(2) << "eta(" << k << ") = " << eta << endl;
      S.SetReduction(Keps / (Keps + eta));
      if (eta < Keps) {
        KSum += k;
        break;
      }
    }
    du = 0.0;
    for (int j = 0; j <= k; ++j)
      du += s[j] * v[j];
    //	s *= (1/beta);
    //	mout << "s " << s << endl;
    du -= u;
    if (k == Kmax) du[0] = 2e10;
  }

  void operator()(DGAssemble &assemble, TimeSeries &TS, Vector &u) {
    Date Start;
    Matrix M(u);
    Matrix B(u);
    Matrix B2(u);
    Matrix B3(u);
    Matrix A(u);
    Vector RHS(u);
    Vector RHS2(u);
    Vector RHS3(u);
    Vector Mu(u);
    RHS = 0.;
    RHS2 = 0.;
    RHS3 = 0.;

    double T = TS.LastTStep();
    double t = TS.FirstTStep();
    int step = 0;
    assemble.InitialStep(t, u);
    assemble.MassMatrix(M);
    IM(M);
    assemble.FluxMatrix(A);
    Vector du(0.0, u);
    double dt_old = 0;
    SSum = MSum = KSum = KPre = 0;
    Date CStart;
    TPre = CStart - Start;
    double f_max = 0;
    Vector u_old(u);
    while (t < T) {
      double t_old = t;
      if (f_max == 0) t = TS.NextTStep(t);
      else t = TS.NextSmallTStep(t);
      f_max = assemble.InitTimeStep(t_old, t, u);
      double dt = t - t_old;
      switch(rkorder) {
        case 1: {
          if (dt_old == 0) mout << "explicit Euler " << endl;
          assemble.RHS(t - dt, RHS);
          Euler(dt, u, A, du, RHS);
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du);
          break;
        }
        case 11: {
          if (dt_old == 0) mout << "explicit Euler " << endl;
          assemble.MassMatrix(M);
          IM(M);
          assemble.FluxMatrix(A);
          assemble.RHS(t - dt, RHS);
          Euler(dt, u, A, du, RHS);
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du);
          break;
        }
        case 2: {
          if (dt_old == 0) mout << "explicit Heun " << endl;
          assemble.RHS(t - dt, RHS);
          assemble.RHS(t, RHS2);
          Heun(dt, u, A, du, RHS, RHS2);
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du);
          break;
        }
        case 3:
          if (dt_old == 0) {
            if (assemble.RHS() == true) Exit(
              "RHS not implemented in Exponential Integrator");
          }
          mout << "explicit Runge " << endl;
          Runge(dt, u, A, du);
          break;
        case 4:
          if (dt_old == 0) mout << "classical Runge-Kutta" << endl;
          assemble.RHS(t - dt, RHS);
          assemble.RHS(t - 0.5 * dt, RHS2);
          assemble.RHS(t, RHS3);
          Kutta(dt, u, A, du, RHS, RHS2, RHS3);
          break;
        case 0:
          if (dt_old == 0) {
            if (assemble.RHS() == true) Exit(
              "RHS not implemented in Exponential Integrator");
            B = M;
            gamma = gamma0 * dt;
            B += (-gamma) * A;
            mout << "Exponential Integrator gamma = " << gamma << endl;
            S(B, 1);
          }
          ExponentialIntegrator(dt, u, M, A, du);
          break;
        case 1000:
          if (dt_old == 0) {
            if (assemble.RHS() == true) Exit("RHS not implemented in Arnoldi");
            mout << "Arnoldi " << endl;
          }
          Arnoldi(dt, u, M, A, du);
          break;
        case 2000:
          if (dt_old == 0) {
            if (assemble.RHS() == true) Exit("RHS not implemented in Arnoldi2");
            B = M;
            gamma = gamma0 * dt;
            B += (-gamma) * A;
            S(B, 1);
            mout << "Arnoldi2  gamma = " << gamma << endl;
          }
          Arnoldi2(dt, u, M, A, du);
          break;
        case -2:
          if (abs(dt - dt_old) > 1e-13) {
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule: gamma = " << gamma << endl;
            S(B, 1);
//                     SmallMatrix SmB(B);
//                     Spectrum spec2(SmB);
//                     pout << OUTX(spec2) << endl;
//                     Exit("asd")
          }
          assemble.RHS(t - 0.5 * dt, RHS);
          Pade2(dt, u, A, B, du, RHS);
          break;
        case -29:
          if (abs(dt - dt_old) > 1e-13) {
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            mout << "Implicit Midpoint Rule with reassemble: gamma = " << gamma << endl;
          }
          assemble.MassMatrix(M);
          IM(M);
          assemble.FluxMatrix(A);
          B = M;
          B += (-gamma) * A;
          S(B, 1);
          assemble.RHS(t - 0.5 * dt, RHS);
          Pade2(dt, u, A, B, du, RHS);
          break;
        case -4:
          if (abs(dt - dt_old) > 1e-13) {
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            B2 = M;
            gamma = 0.25 * dt;
            assemble.SetGamma(gamma);
            B2 += (-gamma) * A;
            mout << "Extrapolated Implicit Midpoint Rule: gamma = " << gamma << endl;
            S(B, 1);
            S2(B2, 1);
          }
          assemble.RHS(t - 0.5 * dt, RHS);
          assemble.RHS(t - 0.75 * dt, RHS2);
          assemble.RHS(t - 0.25 * dt, RHS3);
          Pade2ex(dt, u, A, B, B2, du, RHS, RHS2, RHS3);
          break;
        case -20:
          if (abs(dt - dt_old) > 1e-13) {
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            B2 = M;
            gamma = 0.25 * dt;
            assemble.SetGamma(gamma);
            B2 += (-gamma) * A;
            mout << "Extrapolated Implicit Midpoint Rule: gamma = " << gamma << endl;
            S(B, 1);
            S2(B2, 1);
//                     SmallMatrix SmB(B);
//                     Spectrum spec2(SmB);
//                     pout << OUTX(spec2) << endl;
//                     Exit("asd")
          }
          assemble.RHS(t - 0.5 * dt, RHS);
          assemble.RHS(t - 0.75 * dt, RHS2);
          assemble.RHS(t - 0.25 * dt, RHS3);
          Pade2ex(dt, u, A, B, B2, du, RHS, RHS2, RHS3);
          break;
        case -220: {
          gamma = 0.5 * dt;
          if (abs(dt - dt_old) > 1e-13) {
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule: gamma = " << gamma << endl;
            S(B, 1);
          }
          Vector Au(u);
          Au = A * u;
          Au *= gamma;
          Au += M * u;
          du = u;
          Au -= B * du;
          du += S * Au;
          // KPre += S.Iter();
          du -= u;
          ++KSum;
        }
          break;
        case -21: {
          gamma = 0.5 * dt;
          if (abs(dt - dt_old) > 1e-13) {
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule: gamma = " << gamma << endl;
            S(B, 1);
          }
          assemble.RHS(t - 0.5 * dt, RHS);
          assemble.MassMatrix(M);
          assemble.FluxMatrix(A);
          Vector Au(u);
          Au = A * u;
          Au *= gamma;
          Au += RHS;
          Au += M * u;
          IM(M);
          B = M;
          gamma = 0.5 * dt;
          assemble.SetGamma(gamma);
          B += (-gamma) * A;
          if (abs(dt - dt_old) > 1e-13)
            mout << "Implicit Midpoint Rule: gamma = " << gamma << endl;
          S(B, 1);
          du = u;
          Au -= B * du;
          du += S * Au;
          // mout << OUT(u.norm()) << OUT(Au.norm()) << OUT(du.norm()) << endl;
          // KPre += S.Iter();
          du -= u;
          ++KSum;
          // Pade2(dt,u,A,B,du,RHS);
        }
          break;
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du) << endl;
          //break;
        case -22: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            gamma = 0.5 * dt;
            assemble.RHS(t - 1.5 * dt, u, RHS);
            Vector Au(u);
            Au = A * u_old;
            Au *= gamma;
            Au += RHS;
            Mu = M * u_old;
            Au += Mu;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            S(B, 1);
            u = S * Au;
            // KPre += S.Iter();
            Mu = M * u;
            mout << "repeat step " << step
                 << "\t t = " << t - dt
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);
          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du *= (1 / dt);
          Au -= B * du;
          du += S * Au;
          // KPre += S.Iter();
          du *= dt;
          // Mu = M * u;
          //mout << OUT(u*Mu) << OUT(u.norm()) << " pv_max " << assemble.PV_Max(u) << endl;
          // Vector Au(u);
          // Au = A * u;
          // Au *= gamma;
          // Au += RHS;
          // Au += M * u;
          // du = u;
          // Au -= B * du;
          // du += S * Au;
          // mout << OUT(u.norm()) << OUT(Au.norm()) << OUT(du.norm()) << endl;
          // du -= u;
          // Pade2(dt,u,A,B,du,RHS);
          // KPre += S.Iter();
          ++KSum;
        }
          break;
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du) << endl;
          //break;
        case -227: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            // mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "    Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, M, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            gamma = dt_old;
            assemble.RHS(t - dt - 0.5 * dt_old, u, RHS);
            RHS *= dt_old;
            Mu = M * u_old;
            Mu += RHS;
            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            S(B, 1);
            u = S * Mu;
            // KPre += S.Iter();
            Mu = M * u;
            mout << "    repeat step " << step
                 << "\t t = " << t - dt
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
            IM(M);
            B = M;
            gamma = 0.5 * dt;
            B += (-gamma) * A;
            mout << "    Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
            if (assemble.Repeat() > 0) u = 0;
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);

          //Vector rr(RHS);
          //rr *= 10000;
          //mout << "rr" << rr.norm() << endl << rr;

          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du *= (1 / dt);
          Au -= B * du;
          du += S * Au;
          // KPre += S.Iter();
          du *= dt;

          //Vector uu(RHS);
          //uu *= 10000;
          //mout << "uu" << uu.norm() << endl << uu;


          // Mu = M * u;
          //mout << OUT(u*Mu) << OUT(u.norm()) << " pv_max " << assemble.PV_Max(u) << endl;
          // Vector Au(u);
          // Au = A * u;
          // Au *= gamma;
          // Au += RHS;
          // Au += M * u;
          // du = u;
          // Au -= B * du;
          // du += S * Au;
          // mout << OUT(u.norm()) << OUT(Au.norm()) << OUT(du.norm()) << endl;
          // du -= u;
          // Pade2(dt,u,A,B,du,RHS);
          ++KSum;
        }
          break;
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du) << endl;
          //break;
        case -2270: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            gamma = dt;
            assemble.RHS(t - dt, u, RHS);
            RHS *= dt;
            assemble.SetMat3();
            assemble.MassMatrix(M);
            Mu = M * u_old;
            Mu += RHS;
            assemble.SetMat1();
            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            S(B, 1);
            u = S * Mu;
            // KPre += S.Iter();
            Mu = M * u;
            mout << "repeat step " << step
                 << "\t t = " << t - dt
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
            IM(M);
            B = M;
            gamma = 0.5 * dt;
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);
          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du *= (1 / dt);
          Au -= B * du;
          du += S * Au;
          // KPre += S.Iter();
          du *= dt;
          // Mu = M * u;
          //mout << OUT(u*Mu) << OUT(u.norm()) << " pv_max " << assemble.PV_Max(u) << endl;
          // Vector Au(u);
          // Au = A * u;
          // Au *= gamma;
          // Au += RHS;
          // Au += M * u;
          // du = u;
          // Au -= B * du;
          // du += S * Au;
          // mout << OUT(u.norm()) << OUT(Au.norm()) << OUT(du.norm()) << endl;
          // du -= u;
          // Pade2(dt,u,A,B,du,RHS);
          // KPre += S.Iter();
          ++KSum;
        }
          break;
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du) << endl;
          //break;
        case -2271: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            assemble.SetMat3();
            assemble.EnergyCorrection(u);
            assemble.SetMat1();
            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            Mu = M * u;
            mout << "energy correction in step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu)
                 // << OUT(u.norm())
                 << endl;
            IM(M);
            B = M;
            gamma = 0.5 * dt;
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
            du = 0;
          } else {
            assemble.RHS(t - 0.5 * dt, u, RHS);
            Vector Au(u);
            Au = A * u;
            // if (step == 734) pout << OUTX(Au);
            Au += RHS;
            du *= (1 / dt);
            Au -= B * du;
            du += S * Au;
            // KPre += S.Iter();
            du *= dt;
            // Mu = M * u;
            //mout << OUT(u*Mu) << OUT(u.norm()) << " pv_max " << assemble.PV_Max(u) << endl;
            // Vector Au(u);
            // Au = A * u;
            // Au *= gamma;
            // Au += RHS;
            // Au += M * u;
            // du = u;
            // Au -= B * du;
            // du += S * Au;
            // mout << OUT(u.norm()) << OUT(Au.norm()) << OUT(du.norm()) << endl;
            // du -= u;
            // Pade2(dt,u,A,B,du,RHS);
            // KPre += S.Iter();
            ++KSum;
          }
        }
          break;
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du) << endl;
          //break;
        case -2272: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            assemble.SetMat3();
            assemble.MassMatrix(M);
            IM(M);
            u = IM * Mu;
            assemble.SetMat1();
            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            Mu = M * u;
            mout << "energy projection in step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu)
                 // << OUT(u.norm())
                 << endl;
            IM(M);
            B = M;
            gamma = 0.5 * dt;
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
            du = 0;
          } else {
            assemble.RHS(t - 0.5 * dt, u, RHS);
            Vector Au(u);
            Au = A * u;
            Au += RHS;
            du *= (1 / dt);
            Au -= B * du;
            du += S * Au;
            // KPre += S.Iter();
            du *= dt;
            // Mu = M * u;
            //mout << OUT(u*Mu) << OUT(u.norm()) << " pv_max " << assemble.PV_Max(u) << endl;
            // Vector Au(u);
            // Au = A * u;
            // Au *= gamma;
            // Au += RHS;
            // Au += M * u;
            // du = u;
            // Au -= B * du;
            // du += S * Au;
            // mout << OUT(u.norm()) << OUT(Au.norm()) << OUT(du.norm()) << endl;
            // du -= u;
            // Pade2(dt,u,A,B,du,RHS);
            // KPre += S.Iter();
            ++KSum;
          }
        }
          break;
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du) << endl;
          //break;
        case -2273: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            assemble.RHS(t - 0.5 * dt, u, RHS);
            RHS += Mu;
            assemble.SetMat3();
            assemble.MassMatrix(M);
            B = M;
            gamma = dt;
            B += (-gamma) * A;
            mout << "Implicit Euler with update: gamma = " << gamma << endl;
            S(B, 1);
            du = S * RHS;
            du -= u;
            // KPre += S.Iter();
            ++KSum;
            assemble.SetMat1();
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
            Mu = M * u;
            mout << "energy update in step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu)
                 // << OUT(u.norm())
                 << endl;
          } else {
            assemble.RHS(t - 0.5 * dt, u, RHS);
            Vector Au(u);
            Au = A * u;
            Au += RHS;
            du *= (1 / dt);
            Au -= B * du;
            du += S * Au;
            // KPre += S.Iter();
            du *= dt;
            // KPre += S.Iter();
            ++KSum;
          }
          break;
        }
        case -2274: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            assemble.RHS(t - 0.5 * dt, u, RHS);
            Vector Au(u);
            Au = A * u;
            Au += RHS;
            Au *= dt;
            Au += Mu;
            assemble.SetMat3();
            assemble.MassMatrix(M);
            mout << "Explicit Euler with update: gamma = " << gamma << endl;
            IM(M);
            du = IM * Au;
            du -= u;
            // KPre += S.Iter();
            ++KSum;
            assemble.SetMat1();
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
            Mu = M * u;
            mout << "energy update in step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu)
                 // << OUT(u.norm())
                 << endl;
          } else {
            assemble.RHS(t - 0.5 * dt, u, RHS);
            Vector Au(u);
            Au = A * u;
            Au += RHS;
            du *= (1 / dt);
            Au -= B * du;
            du += S * Au;
            // KPre += S.Iter();
            du *= dt;
            // KPre += S.Iter();
            ++KSum;
          }
          break;
        }
        case -228: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          mout << "Step " << step
               << "\t t = " << t
               << "\t Energy " << assemble.Energy(u)
               << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            gamma = dt;
            assemble.RHS(t - dt, u, RHS);
            RHS *= dt;
            Mu = M * u_old;
            Mu += RHS;
            assemble.MassMatrix2(M);
            assemble.FluxMatrix(A);
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            S(B, 1);
            u = S * Mu;
            // KPre += S.Iter();
            assemble.MassMatrix(M);
            Mu = M * u;
            mout << "repeat step " << step
                 << "\t t = " << t - dt
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
            IM(M);
            B = M;
            gamma = 0.5 * dt;
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);
          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du *= (1 / dt);
          Au -= B * du;
          du += S * Au;
          // KPre += S.Iter();
          du *= dt;
          // Mu = M * u;
          //mout << OUT(u*Mu) << OUT(u.norm()) << " pv_max " << assemble.PV_Max(u) << endl;
          // Vector Au(u);
          // Au = A * u;
          // Au *= gamma;
          // Au += RHS;
          // Au += M * u;
          // du = u;
          // Au -= B * du;
          // du += S * Au;
          // mout << OUT(u.norm()) << OUT(Au.norm()) << OUT(du.norm()) << endl;
          // du -= u;
          // Pade2(dt,u,A,B,du,RHS);
          // KPre += S.Iter();
          ++KSum;
        }
          break;
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du) << endl;
          //break;
        case -23: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          mout << "step " << step
               << "\t t = " << t
               << "\t Energy " << assemble.Energy(u)
               << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            Mu = M * u_old;
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            u_old = IM * Mu;
            gamma = 0.5 * dt;
            assemble.RHS(t - 1.5 * dt, u, RHS);
            Vector Au(u);
            Au = A * u_old;
            Au *= gamma;
            Au += RHS;
            Mu = M * u_old;
            Au += Mu;
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            S(B, 1);
            u = S * Au;
            // KPre += S.Iter();
            Mu = M * u;
            mout << "repeat step " << step
                 << "\t t = " << t - dt
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);
          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du *= (1 / dt);
          Au -= B * du;
          du += S * Au;
          // KPre += S.Iter();
          du *= dt;
          // Mu = M * u;
          //mout << OUT(u*Mu) << OUT(u.norm()) << " pv_max " << assemble.PV_Max(u) << endl;
          // Vector Au(u);
          // Au = A * u;
          // Au *= gamma;
          // Au += RHS;
          // Au += M * u;
          // du = u;
          // Au -= B * du;
          // du += S * Au;
          // mout << OUT(u.norm()) << OUT(Au.norm()) << OUT(du.norm()) << endl;
          // du -= u;
          // Pade2(dt,u,A,B,du,RHS);
          // KPre += S.Iter();
          ++KSum;
        }
          break;
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du) << endl;
          //break;
        case -24: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            Mu = M * u_old;
            assemble.SetMat3();
            assemble.MassMatrix(M);
            IM(M);
            u_old = IM * Mu;
            assemble.SetMat1();
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            gamma = 0.5 * dt;
            assemble.RHS(t - 1.5 * dt, u, RHS);
            Vector Au(u);
            Au = A * u_old;
            Au *= gamma;
            Au += RHS;
            Mu = M * u_old;
            Au += Mu;
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            S(B, 1);
            u = S * Au;
            // KPre += S.Iter();
            Mu = M * u;
            mout << "repeat step " << step
                 << "\t t = " << t - dt
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);
          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du *= (1 / dt);
          Au -= B * du;
          du += S * Au;
          // KPre += S.Iter();
          du *= dt;
          ++KSum;
        }
          break;
        case -25: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            for (int j = 0; j < repeat_step; ++j) {
              Mu = M * u_old;
              double s = (j + 1.0) / repeat_step;
              assemble.SetMat(s);
              assemble.MassMatrix(M);
              IM(M);
              u_old = IM * Mu;
            }
            assemble.SetMat1();
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            gamma = 0.5 * dt;
            assemble.RHS(t - 1.5 * dt, u, RHS);
            Vector Au(u);
            Au = A * u_old;
            Au *= gamma;
            Au += RHS;
            Mu = M * u_old;
            Au += Mu;
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            S(B, 1);
            u = S * Au;
            // KPre += S.Iter();
            Mu = M * u;
            mout << "repeat step " << step
                 << "\t t = " << t - dt
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);
          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du *= (1 / dt);
          Au -= B * du;
          du += S * Au;
          // KPre += S.Iter();
          du *= dt;
          ++KSum;
        }
          break;
        case -26: {
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;
            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
          if (assemble.reassemble()) {
            t -= dt;
            u = u_old;
            for (int j = 0; j < repeat_step; ++j) {
              t += dt / repeat_step;
              Mu = M * u;
              double s = (j + 1.0) / repeat_step;
              assemble.SetMat(s);
              assemble.MassMatrix(M);
              IM(M);
              u = IM * Mu;
              gamma = 0.5 * dt / repeat_step;
              assemble.RHS(t - 0.5 * dt / repeat_step, u, RHS);
              Vector Au(u);
              Au = A * u;
              Au *= gamma;
              Au += RHS;
              Mu = M * u;
              Au += Mu;
              B = M;
              assemble.SetGamma(gamma);
              B += (-gamma) * A;
              S(B, 1);
              u = S * Au;
              // KPre += S.Iter();
              Mu = M * u;
              mout << "repeat step " << step
                   << "\t t = " << t
                   << "\t Energy " << assemble.Energy(u)
                   << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;
            }
            assemble.SetMat1();
            assemble.MassMatrix(M);
            IM(M);
            assemble.FluxMatrix(A);
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            S(B, 1);
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);
          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du *= (1 / dt);
          Au -= B * du;
          du += S * Au;
          // KPre += S.Iter();
          du *= dt;
          ++KSum;
        }
          break;
        case -22000: {
          gamma = 0.5 * dt;
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {

            mout << OUT(t) << OUT(dt) << OUT(dt_old) << endl;

            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          // assemble.RHS(t-0.5*dt,RHS);
          // Vector Au(u);
          // Vector Mu(u);
          // Mu = M * u;
          // mout << OUT(Mu*u) << endl;
          // Au = A * u;
          // Au *= gamma;
          // Au += RHS;
          // Au += M * u;

          Mu = M * u;
          if (0)
            mout << "step " << step
                 << "\t t = " << t
                 << "\t Energy " << assemble.Energy(u)
                 << " 0.5*u*M*u " << 0.5 * (u * Mu)
                 << " " << OUT(u.norm())
                 << endl;

          if (assemble.reassemble()) {
            double uMu;
            if (EnergyCorrection) {
              Mu = M * u;
              uMu = u * Mu;
              if (0)
                mout << "    "
                     << OUT(u * Mu) << OUT(u.norm())
                     << " f_max " << f_max
                     << " pv_max " << assemble.PV_Max(u) << endl;
            }
            mout << " reass " << OUT(t) << OUT(dt) << OUT(dt_old) << endl;

            assemble.MassMatrix(M);
            IM(M);
            if (EnergyCorrection) {
              //u = IM * Mu;
              assemble.EnergyCorrection(u);
              du = 0;
              Mu = M * u;
              double uMu2 = Mu * u;
              mout << " Energy dissipation from "
                   << 0.5 * uMu << " to " << 0.5 * uMu2
                   << " pv_max " << assemble.PV_Max(u)
                   << " " << OUT(u.norm())
                   << endl;
            }
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma
                 << " " << OUT(u.norm())
                 << endl;
            B += (-gamma) * A;
            S(B, 1);
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);
          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du *= (1 / dt);
          Au -= B * du;
          du += S * Au;
          //du = S * Au;
          du *= dt;
          // KPre += S.Iter();
          ++KSum;
          break;
        }
        case -22002: {
          gamma = 0.5 * dt;
          if ((abs(dt - dt_old) > 1e-13) && (!assemble.reassemble())) {
            assemble.MassMatrix(M);
            assemble.FluxMatrix(A);
            B = M;
            assemble.SetGamma(gamma);
            B += (-gamma) * A;
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            S(B, 1);
          }
          // assemble.RHS(t-0.5*dt,RHS);
          // Vector Au(u);
          // Vector Mu(u);
          // Mu = M * u;
          // mout << OUT(Mu*u) << endl;
          // Au = A * u;
          // Au *= gamma;
          // Au += RHS;
          // Au += M * u;

          Mu = M * u;
          mout << "step " << step
               << "\t t = " << t
               << "\t Energy " << assemble.Energy(u)
               << " 0.5*u*M*u " << 0.5 * (u * Mu) << endl;

          if (assemble.reassemble()) {
            double uMu;
            if (EnergyCorrection) {
              Mu = M * u;
              uMu = u * Mu;
              if (0)
                mout << "    "
                     << OUT(u * Mu) << OUT(u.norm())
                     << " f_max " << f_max
                     << " pv_max " << assemble.PV_Max(u) << endl;
            }
            mout << " reass " << OUT(t) << OUT(dt) << OUT(dt_old) << endl;

            assemble.MassMatrix2(M);
            IM(M);
            if (EnergyCorrection) {
              u = IM * Mu;
              Mu = M * u;
              double uMu2 = Mu * u;
              mout << " Energy dissipation from "
                   << 0.5 * uMu << " to " << 0.5 * uMu2
                   << " pv_max " << assemble.PV_Max(u) << endl;
            }
            assemble.FluxMatrix(A);
            B = M;
            gamma = 0.5 * dt;
            assemble.SetGamma(gamma);
            mout << "Implicit Midpoint Rule with update: gamma = " << gamma << endl;
            B += (-gamma) * A;
            S(B, 1);
          }
          assemble.RHS(t - 0.5 * dt, u, RHS);
          Vector Au(u);
          Au = A * u;
          Au += RHS;
          du = S * Au;
          // KPre += S.Iter();
          du *= dt;
          ++KSum;
          break;
        }
        case -222:
          assemble.MassMatrix(M);
          IM(M);
          assemble.FluxMatrix(A);
          B = M;
          gamma = 0.5 * dt;
          assemble.SetGamma(gamma);
          B += (-gamma) * A;
          if (abs(dt - dt_old) > 1e-13)
            mout << "Implicit Midpoint Rule: gamma = " << gamma << endl;
          S(B, 1);
          assemble.RHS(t - 0.5 * dt, RHS);
          Pade2(dt, u, A, B, du, RHS);
          break;
          //Vector Au = A * u;
          //mout << OUTX(u) << OUTX(Au) << OUTX(RHS) << OUTX(du) << endl;
          //break;
        default: Exit("not implemented");
      }
      u_old = u;
      u += du;
      //   mout << OUTX(u);
      ++SSum;
      if (u.norm() > 1e10) {
//                u = 0;
        mout << "divergence at t = " << t << endl;
        return;
      }
      assemble.FinishStep(u, t, dt, ++step, plot_tStep);
      dt_old = dt;
    }
    TComp = Date() - CStart;
  }

  int S_Sum() const { return SSum; }

  int M_Sum() const { return MSum; }

  int K_Sum() const { return KSum; }

  int K_Pre() const { return KPre; }

  Time T_Comp() const { return TComp; }

  Time T_Pre() const { return TPre; }
};

#endif
