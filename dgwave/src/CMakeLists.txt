set(dgwave
        DGElasticity.cpp
        DGProblem.cpp
        )

add_library(DGWAVE STATIC ${dgwave})
target_link_libraries(DGWAVE MPP_LIBRARIES)
