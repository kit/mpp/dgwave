#ifndef _SPACETIMETOOLS_H_
#define _SPACETIMETOOLS_H_

#include "m++.hpp"
#include "RVector.hpp"
#include "RMatrix.hpp"


static void PrintValues(const char *name, const RVector &V) {
  mout << name << V << endl;
  int N = V.size();
  if (N < 2) return;
  RVector Difference(0.0, N - 1);
  for (int i = 0; i < N - 1; ++i)
    Difference[i] = V[i] - V[i + 1];
  mout << "Difference" << endl;
  mout << "                     " << Difference;
  if (N < 3) return;
  RVector Rate(0.0, N - 2);
  for (int i = 0; i < N - 2; ++i) {
    if (abs(Difference[i]) > 1e-9) {
      Rate[i] = log2(Difference[i] / Difference[i + 1]);
    }
    mout << "                     " << Rate << endl;
  }
}

static void PrintValuesCR(const char *name, const RVector &V) {
  int scales = 1;
  int level = 1;
  int plevel = 1;
  mout << "--------------------------------";
  mout << name;
  mout << "--------------------------------" << endl;
  mout << "###########################" << endl;
  Config::Get("level", level);
  Config::Get("plevel", plevel);
  Config::Get("Scales", scales);
  if (scales == 1) return;
  int L = level - plevel + 1;
  for (int l = 0; l < L; ++l) {
    mout << "SpaceLevel " << l << " Errors:" << endl;
    for (int i = 0; i < scales; ++i) {
      mout << V[l * scales + i] << "     ";
    }
    mout << endl;
    mout << "Error in Time: " << endl;
    for (int i = 0; i < scales - 1; ++i) {
      mout << V[l * scales + i] - V[l * scales + i + 1] << "  ";
    }
    mout << endl;
  }
  mout << "###########################" << endl;
}

#endif
